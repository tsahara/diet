#include "scm.h"

#include <stdlib.h>

sval_t
g__2a_(sval_t a, sval_t b)
{
	return fixnum(a) * b;
}

sval_t
g__3c_(sval_t a, sval_t b)
{
	return (fixnum(a) < fixnum(b)) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g__3c__3d_(sval_t a, sval_t b)
{
	return (fixnum(a) <= fixnum(b)) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g__3d_(sval_t a, sval_t b)
{
	return (fixnum(a) == fixnum(b)) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g__3e_(sval_t a, sval_t b)
{
	return (fixnum(a) > fixnum(b)) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g__3e__3d_(sval_t a, sval_t b)
{
	return (fixnum(a) >= fixnum(b)) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_abs(sval_t n)
{
	return fixnum(n) >= 0 ? n : -n;
}

sval_t
g_complex_3f_(sval_t n)
{
	return fixnum_p(n) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_exact_3f_(sval_t n)
{
	return fixnum_p(n) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_exact_integer(sval_t n)
{
	return fixnum_p(n) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_finite_3f_(sval_t n)
{
	return fixnum_p(n) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_inexact_3f_(sval_t n)
{
	return (g_exact_3f_(n) == SVAL_FALSE) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_infinite_3f_(sval_t n)
{
	return SVAL_FALSE;
}

sval_t
g_integer_3f_(sval_t n)
{
	return fixnum_p(n) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_nan_3f_(sval_t n)
{
	return SVAL_FALSE;
}

sval_t
g_number_3f_(sval_t n)
{
	return fixnum_p(n) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_plus(sval_t va, sval_t vb)
{
	signed_sval_t a = va, b = vb;
	signed_sval_t sum = a + b;

	if (((sum ^ a) & (sum ^ b)) >= 0)
		return sum;
	else {
		raise_cstr("integer overflow/underflow");
		return SVAL_UNSPECIFIED;
	}
}

sval_t
g_real_3f_(sval_t n)
{
	return fixnum_p(n) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_rational_3f_(sval_t n)
{
	return fixnum_p(n) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_zero_3f_(sval_t n)
{
	return fixnum(n) == 0 ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_positive_3f_(sval_t n)
{
	return fixnum(n) > 0 ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_negative_3f_(sval_t n)
{
	return fixnum(n) < 0 ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_odd_3f_(sval_t n)
{
	return (fixnum(n) & 1) == 1 ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_even_3f_(sval_t n)
{
	return (fixnum(n) & 1) == 0 ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_max_c(sval_t l)
{
	int i, n;

	n = fixnum(pair_car(l));
	l = pair_cdr(l);
	while (pair_p(l)) {
		i = fixnum(pair_car(l));
		if (i > n)
			n = i;
		l = pair_cdr(l);
	}
	return sval_fixnum(n);
}

sval_t
g_min(sval_t a, sval_t b)
{
	return (fixnum(a) < fixnum(b)) ? a : b;
}

sval_t
g_modulo(sval_t a, sval_t b)
{
	int a0, a1, b0, b1, q;

	// XXX
	a0 = fixnum(a);
	b0 = fixnum(b);
	a1 = abs(a0);
	b1 = abs(b0);
	q = a1 / b1;       /* q >= 0 */
	if (a0 * b0 >= 0)
		return sval_fixnum(a0 - b0 * q);
	else
		return sval_fixnum(a0 + b0 * (q + 1));
}

sval_t
g_quotient(sval_t a, sval_t b)
{
	int a0, a1, b0, b1, q;

	// XXX
	a0 = fixnum(a);
	b0 = fixnum(b);
	a1 = abs(a0);
	b1 = abs(b0);
	q = a1 / b1;       /* q >= 0 */
	if (a0 * b0 >= 0)
		return sval_fixnum(q);
	else
		return sval_fixnum(-q);
}

sval_t
g_remainder(sval_t a, sval_t b)
{
	int a0, a1, b0, b1, q;

	// XXX
	a0 = fixnum(a);
	b0 = fixnum(b);
	a1 = abs(a0);
	b1 = abs(b0);
	q = a1 / b1;
	if (a0 * b0 >= 0)
		return sval_fixnum(a0 - b0 * q);
	else
		return sval_fixnum(a0 - b0 * (-q));
}
