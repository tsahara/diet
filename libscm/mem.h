#include <stddef.h>

void *cell_alloc(sval_t *, sval_t);
void gc_mark(sval_t);
void gc_mark_cell_bitmap(sval_t);
void gc_mark_stack(void *, size_t);
void *mem_alloc(size_t, sval_t *, sval_t);
void mem_init(void);
void mem_last(void);
void tmproot_add(sval_t);
size_t tmproot_prepare(void);
void tmproot_reset(size_t);
