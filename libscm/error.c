#include <stdio.h>
#include <stdlib.h>

#include "scm.h"


enum error_class {
	ARGUMENT_ERROR,
};

struct sobj_error {
	struct sobj_header h;
	sval_t eclass;
	sval_t vline;
};

static sval_t exception_handler = SVAL_NIL;


sval_t
c_error_p(sval_t v)
{
	return error_p(v) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
c_error_abort(void)
{
	printf("default exception handler\n");
	abort();
	return SVAL_NIL;
}

sval_t
c_make_argument_error(sval_t vline)
{
	struct sobj_error *err;

	err = mem_alloc(sizeof(struct sobj_error), NULL, 0);
	err->h.word = sizeof(struct sobj_error);
	err->eclass = ARGUMENT_ERROR;
	err->vline   = vline;
	return (sval_t)err | SVAL_TAG_ERROR;
}

sval_t
c_raise2(sval_t ve)
{
	extern void ucall(sval_t, sval_t);
	sval_t p;

	p = g_cons(ve, SVAL_NIL);
	ucall(exception_handler, p);
	return SVAL_FALSE;
}

sval_t
c_set_exception_handler(sval_t vp)
{
	exception_handler = vp;
	return SVAL_NIL;
}

void
mark_exception_handler(void)
{
	gc_mark(exception_handler);
}

void
raise_cstr(const char *cp)
{
	g_raise(make_string_cstr(cp));
}

sval_t
g_raise(sval_t obj)
{
	s_scheme_2d_default_2d_exception_2d_handler(obj);
	return SVAL_FALSE;
}

sval_t
s_scheme_2d_default_2d_exception_2d_handler(sval_t e)
{
	printf("default exception handler\n");
	abort();
}
