#include <stdlib.h>

#include "scm.h"

struct sobj_box {
	sval_t val;
	sval_t dummy;
};


void
box_gc_mark(sval_t v)
{
	struct sobj_box *b = sval_to_ptr(v);

	gc_mark_cell_bitmap(v);
	gc_mark(b->val);
}

sval_t
s_box(sval_t val)
{
	struct sobj_box *b;

	b = cell_alloc(&val, 1);
	b->val = val;
	b->dummy = 0xb2b2b2b2b2b2b2b2ULL;
	return (sval_t)b | SVAL_TAG_BOX;
}
