#include <ctype.h>
#include <inttypes.h>
#include <stdio.h>
#include "scm.h"

void
char_write(sval_t v_ch)
{
	uint32_t ucs = cval_char(v_ch);
	const char *repr = NULL;
	char buf[10];

	switch (ucs) {
	case 0x00:	repr = "#\null";	break;
	case 0x07:	repr = "#\\alarm";	break;
	case 0x08:	repr = "#\\backspace";	break;
	case 0x09:	repr = "#\\tab";	break;
	case 0x0a:	repr = "#\\newline";	break;
	case 0x0d:	repr = "#\\return";	break;
	case 0x1b:	repr = "#\\escape";	break;
	case 0x20:	repr = "#\\space";	break;
	case 0x7f:	repr = "#\\delete";	break;
	default:
		if (ucs < 0x80 && isprint((int)ucs)) {
			buf[0] = '#';
			buf[1] = '\\';
			buf[2] = (uint8_t)ucs;
			buf[3] = '\0';
		} else if (ucs <= 999999)
			snprintf(buf, sizeof(buf), "#\\x%x", (unsigned)ucs);
		else  // invalid char
			snprintf(buf, sizeof(buf), "#\\x%x", 0xffffff);
		repr = buf;
		break;
	}
	printf("%s", repr);
}

sval_t
g_char_3f_(sval_t a)
{
	return char_p(a) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_char_integer(sval_t v_ch)
{
	uint32_t ucs = cval_char(v_ch);
	return sval_fixnum((int)ucs);
}

sval_t
g_integer_char(sval_t v_num)
{
	uint32_t ucs = cval_fixnum(v_num);
	if (ucs > 0xffffffffUL)
		raise_cstr("too large number to convert to char");
	return sval_char(ucs);
}
