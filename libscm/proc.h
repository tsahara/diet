sval_t make_proc(void *, sval_t, sval_t);
void proc_gc_mark(sval_t);

static inline int
proc_p(sval_t v)
{
	return (v & 0xf) == SVAL_TAG_PROC;
}
