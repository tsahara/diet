#include "scm.h"

struct sobj_gfun {
	struct sobj_header h;
	sval_t vclass;
	void *code;
	sval_t procs[16];
};


sval_t
g_generic_function_add_b(sval_t v_gf, sval_t v_proc)
{
	struct sobj_gfun *gf = sval_to_ptr(v_gf);
	int i;

	for (i = 0; i < 16; i++)
		if (gf->procs[i] == SVAL_NIL)
			break;
	if (i == 16)
		raise_cstr("too many procs for a generic function");
	gf->procs[i] = v_proc;
	return SVAL_UNSPECIFIED;
}

sval_t
g_make_generic_function(sval_t vl)
{
	extern void gcall(sval_t, sval_t);
	struct sobj_gfun *gf;
	int i;

	// n = list_length(vl);
	// if (n >= 16) ...
	gf = mem_alloc(sizeof(struct sobj_gfun), &vl, 1);
	gf->h.word = sizeof(struct sobj_gfun);
	gf->vclass = SVAL_NIL;
	gf->code = gcall;
	for (i = 0; i < 16; i++) {
		if (pair_p(vl)) {
			gf->procs[i] = pair_car(vl);
			vl = pair_cdr(vl);
		} else
			gf->procs[i] = SVAL_NIL;
	}
	return (sval_t)gf | SVAL_TAG_GPROC;
}

void
gfun_gc_mark(sval_t v)
{
	struct sobj_gfun *gf = sval_to_ptr(v);
	int i;

	if (sobj_h_mark(&gf->h))
		return;
	sobj_h_set_mark(&gf->h, 1);

	gc_mark(gf->vclass);
	for (i = 0; i < 16; i++) {
		if (gf->procs[i] == SVAL_NIL)
			break;
		gc_mark(gf->procs[i]);
	}
}
