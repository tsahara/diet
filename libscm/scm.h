#include <stdint.h>

typedef uintptr_t sval_t;
typedef intptr_t  signed_sval_t;

/*
 * 64bit sval_t
 *
 * xx....xx xxxx0000 : Fixnum  (upper 60 bits => signed integer)
 * xx....xx xxxx0001 : List (Pair)
 * xx....xx xxxx0010 : (notused)
 * xx....xx xxxx0011 : Symbol
 * xx....xx xxxx0100 : String
 * xx....xx xxxx0101 : Procedure
 * xx....xx xxxx0110 : Box
 * xx....xx 00000111 : Character (upper 32bits = Unicode code point)
 * xx....xx xxxx1000 : Vector
 * xx....xx xxxx1001 : Bytevector
 * xx....xx xxxx1010 : Error
 * xx....xx xxxx1011 : (notused)
 * xx....xx xxxx1100 : Large Object
 * xx....xx xxxx1101 : Generic Function (or Large Object?)
 * 00....00 00001110 : '()
 * xx....xx xxxx1111 : Unique Objects (#f, #t, ...)
 */

#define SVAL_TAG_FIXNUM		0x00
#define SVAL_TAG_PAIR		0x01
//#define SVAL_TAG_		0x02
#define SVAL_TAG_SYMBOL		0x03
#define SVAL_TAG_STR		0x04
#define SVAL_TAG_PROC		0x05
#define SVAL_TAG_BOX		0x06
#define SVAL_TAG_CHAR		0x07
#define SVAL_TAG_VECTOR		0x08
#define SVAL_TAG_BYTEVECTOR	0x09
#define SVAL_TAG_ERROR		0x0a
//#define SVAL_TAG_		0x0b
#define SVAL_TAG_LARGE		0x0c
#define SVAL_TAG_GPROC		0x0d
#define SVAL_NIL		0x0e
#define SVAL_FALSE		0x0f
#define SVAL_TRUE		0x1f
#define SVAL_UNSPECIFIED	0x2f
#define SVAL_EOF_OBJECT		0x3f


static inline void *
sval_to_ptr(sval_t v)
{
	return (void *)(v & ~0xf);
}

static inline sval_t
ptr_to_sval(void *ptr, sval_t v)
{
	return (sval_t)ptr | v;
}

static inline int
large_object_p(sval_t v)
{
	return (v & 0xf) == SVAL_TAG_LARGE;
}

struct sobj_header {
	sval_t word;
	/* markbit:  1; */
	/* large:    1; */
	/* unused:  14; */
	/* len:     48; */
};

#define SOBJ_HEADER_MARK_BIT  ((sval_t)1 << 63)
#define SOBJ_HEADER_LARGE     ((sval_t)1 << 62)


static inline sval_t
sobj_h_mark(const struct sobj_header *h)
{
	return h->word & SOBJ_HEADER_MARK_BIT;
}

static inline void
sobj_h_set_mark(struct sobj_header *h, int on)
{
	if (on)
		h->word |= SOBJ_HEADER_MARK_BIT;
	else
		h->word &= ~SOBJ_HEADER_MARK_BIT;
}

static inline sval_t
sobj_h_large_p(const struct sobj_header *h)
{
	return h->word & SOBJ_HEADER_LARGE;
}

static inline sval_t
sobj_h_len(const struct sobj_header *h)
{
	return h->word & ((1ULL << 48) - 1);
}

static inline sval_t
sobj_h_allocated_size(const struct sobj_header *h)
{
	return  (sobj_h_len(h) + 0x0f) & ~0x0f;
}


#include "boolean.h"
#include "box.h"
#include "bytevector.h"
#include "char.h"
#include "error.h"
#include "fixnum.h"
#include "gfun.h"
#include "largeobj.h"
#include "list.h"
#include "mem.h"
#include "misc.h"
#include "port.h"
#include "proc.h"
#include "sval.h"
#include "string.h"
#include "symbol.h"
#include "unspecified.h"
#include "vector.h"
