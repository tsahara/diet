sval_t g__3c_(sval_t, sval_t);  /* < */
sval_t g__3c__3d_(sval_t, sval_t);  /* <= */
sval_t g__3d_(sval_t, sval_t);  /* = */
sval_t g__3e_(sval_t, sval_t);  /* > */
sval_t g__3e__3d_(sval_t, sval_t);  /* >= */
sval_t g_zero_3f_(sval_t);  /* zero? */
sval_t g_positive_3f_(sval_t);  /* positive? */
sval_t g_negative_3f_(sval_t);  /* negative? */
sval_t g_odd_3f_(sval_t);  /* odd? */
sval_t g_even_3f_(sval_t);  /* even? */

static inline int
cval_fixnum(sval_t a)
{
	return (int)a / 0x10;
}

static inline int
fixnum(sval_t a)
{
	return cval_fixnum(a);
}

static inline int
fixnum_p(sval_t v)
{
	return (v & 0xf) == 0x0;
}

static inline sval_t
sval_fixnum(int i)
{
	return (sval_t)i * 0x10;
}
