(define-library (scheme file)
  (export open-input-file open-output-file)
  (begin
    (define-cproc (open-input-file filename) "g_open_input_file")
    (define-cproc (open-output-file filename) "g_open_output_file")
    ))
