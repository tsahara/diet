#include <stdio.h>

#include "scm.h"

sval_t
g_write_string_1(sval_t vstr)
{
	const uint8_t *cp;
	sval_t len;

	sval_string_ref(vstr, &cp, &len);
	fwrite(cp, 1, len, stdout);
	return SVAL_UNSPECIFIED;
}
