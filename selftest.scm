(use gauche.test)

(load "./scheme.scm")

(define (main args)
  (test-start "selftest")

  (test* "+" #f     (peg '((s (+ "a"))) 's "" 'test))
  (test* "+" '("a") (peg '((s (+ "a"))) 's "a" 'test))
  (test* "+" #f     (peg '((s (+ "a"))) 's "b" 'test))

  (test* "!" #f     (peg '((s ("a" (! "b")))) 's "ab" 'test))
  (test* "!" '("a" () "c")  (peg '((s ("a" (! "b") "c"))) 's "ac" 'test))

  (exit (test-end)))
