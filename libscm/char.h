void char_write(sval_t);

static inline int
char_p(sval_t v)
{
	return (v & 0xf) == SVAL_TAG_CHAR;
}

static inline uint32_t
cval_char(sval_t v)
{
	return (uint32_t)(v >> 32);
}

static inline sval_t
sval_char(uint32_t ch)
{
	return ((sval_t)ch << 32) + SVAL_TAG_CHAR;
}
