#include <stddef.h>

uint8_t *bytevector_bytes(sval_t);
void bytevector_copy_b(sval_t, sval_t, sval_t, sval_t, sval_t);
sval_t bytevector_equal(sval_t, sval_t);
sval_t bytevector_length(sval_t);
void bytevector_write(sval_t);
sval_t make_bytevector(size_t, sval_t *, sval_t);
void sval_bytevector_ref(sval_t, uint8_t **, sval_t *);

static inline int
bytevector_p(sval_t v)
{
	return (v & 0xf) == SVAL_TAG_BYTEVECTOR;
}
