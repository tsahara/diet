#include "scm.h"

sval_t
g_boolean_3f_(sval_t a)
{
	return boolean_p(a) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_boolean_eq_p(sval_t va, sval_t vb, sval_t vl)
{
	if (va != SVAL_TRUE && va != SVAL_FALSE)
		return SVAL_FALSE;
	if (va != vb)
		return SVAL_FALSE;
	while (pair_p(vl)) {
		sval_t vc = pair_car(vl);
		if (va != vc)
			return SVAL_FALSE;
		vl = pair_cdr(vl);
	}
	if (null_p(vl))
		return SVAL_TRUE;
	else
		return SVAL_FALSE;
}
