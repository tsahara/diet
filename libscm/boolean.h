static inline int
boolean_p(sval_t v)
{
	return (v == SVAL_TRUE || v == SVAL_FALSE);
}

static inline sval_t
sval_boolean(int b)
{
        return b ? SVAL_TRUE : SVAL_FALSE;
}

