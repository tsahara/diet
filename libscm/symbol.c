#include <ctype.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include "scm.h"

struct sobj_symbol {
	struct sobj_header h;
	uint8_t name[];
};


static struct sobj_symbol *sobj_symbol(sval_t);
static sval_t symbol_length(const struct sobj_symbol *);
static int symbol_is_simple(const struct sobj_symbol *);


static struct sobj_symbol *
sobj_symbol(sval_t v)
{
        return (struct sobj_symbol *)(v & ~0xf);
}

static int
symbol_is_simple(const struct sobj_symbol *sym)
{
	sval_t len = symbol_length(sym);
	int ch;
	const char special_initial[] = "!$%&*/:<=>?^_~";
	const char special_subsequent[] = "+-.@";

	// <initial>
	ch = sym->name[0];
	if (!isalpha(ch) && strchr(special_initial, ch) == NULL)
		return 0;

	// <subsequent>*
	for (sval_t i = 1; i < len; i++) {
		ch = sym->name[i];
		if (!isalnum(ch) &&
		    strchr(special_initial, ch) == NULL &&
		    strchr(special_subsequent, ch) == NULL) {
			return 0;
		}
	}

	return 1;
}

static sval_t
symbol_length(const struct sobj_symbol *sym)
{
        return sobj_h_len(&sym->h) - sizeof(struct sobj_symbol);
}

void
symbol_write(sval_t v)
{
	struct sobj_symbol *sym = sobj_symbol(v);
	sval_t len = symbol_length(sym);

	if (symbol_is_simple(sym))
		printf("%.*s", (int)len, sym->name);
	else {
		fputc('|', stdout);
		for (sval_t i = 0; i < len; i++) {
			int ch = sym->name[i];
			if (ch == '\\' || ch == '|')
				fputc('\\', stdout);
			fputc(ch, stdout);
		}
		fputc('|', stdout);
	}
}

sval_t
g_symbol_3f_(sval_t a)
{
	return symbol_p(a) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_symbol_eq(sval_t va, sval_t vb)
{
	struct sobj_symbol *sa = sobj_symbol(va);
	struct sobj_symbol *sb = sobj_symbol(vb);
	size_t len;

	len = symbol_length(sa);
	if (symbol_length(sb) == len && memcmp(sa->name, sb->name, len) == 0)
		return SVAL_TRUE;
	else
		return SVAL_FALSE;
}

sval_t
g_symbol_to_string(sval_t v_sym)
{
	struct sobj_symbol *sym = sobj_symbol(v_sym);

	return make_string(symbol_length(sym), sym->name);
}
