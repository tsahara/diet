struct sobj_pair {
	sval_t car;
	sval_t cdr;
};

sval_t list_length(sval_t);
void null_write(sval_t);
sval_t pair_equal(sval_t, sval_t);
void pair_write(sval_t);

sval_t g_cons(sval_t, sval_t);
sval_t g_null_3f_(sval_t);
sval_t g_set_2d_car_21_(sval_t, sval_t);
sval_t g_set_2d_cdr_21_(sval_t, sval_t);


static inline int
null_p(sval_t v)
{
	return (v == SVAL_NIL);
}

static inline int
pair_p(sval_t v)
{
	return (v & 0xf) == 0x1;
}

static inline sval_t
pair_car(sval_t vp)
{
	struct sobj_pair *p = sval_to_ptr(vp);
	return p->car;
}

static inline sval_t
pair_cdr(sval_t vp)
{
	struct sobj_pair *p = sval_to_ptr(vp);
	return p->cdr;
}
