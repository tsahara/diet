sval_t make_string(sval_t, uint8_t *);
sval_t make_string_cstr(const char *);
size_t sval_string_copy_cstr(sval_t, char *, size_t);
void sval_string_ref(sval_t, const uint8_t **, sval_t *);
sval_t sval_string_to_bytevector(sval_t);
void string_write(sval_t);

sval_t g_string_3d__3f_(sval_t, sval_t);


static inline int
string_p(sval_t v)
{
	return (v & 0xf) == SVAL_TAG_STR;
}
