(define-library (diet base)
  (import (scheme base)
          (scheme write))
  (export print print-sval sleep)
  (begin
    ;; libscm/init.c
    (define (print x)
      (display x)
      (newline))

    (define-cproc (print-sval obj) "g_print_sval")
    (define-cproc (sleep sec) "g_sleep")
    ))
