(define-library (test library export)
  (export foo bar)
  (begin
    (define foo 1)
    (define (bar) 2)))
