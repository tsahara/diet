(define-library (diet test)
  (import (scheme base) (diet error))
  (export assert-false assert-eq? assert-eqv? assert-equal? assert-exception)
  (begin
    (define-syntax assert-false
      (syntax-rules ()
        ((_ e) (assert-true (not e)))))

    (define-syntax assert-eq?
      (syntax-rules ()
        ((_ a b) (assert-true (eq? a b)))))

    (define-syntax assert-eqv?
      (syntax-rules ()
        ((_ a b) (assert-true (eqv? a b)))))

    (define-syntax assert-equal?
      (syntax-rules ()
        ((_ a b) (assert-true (equal? a b)))))

    (define (assert-exception thunk)
      (let ((e (call/cc (lambda (c)
                          (set-exception-handler! c)
                          (thunk)
                          #f))))
        (assert-true (error? e))))
    ))
