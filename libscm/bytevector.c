#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "scm.h"

struct sobj_bytevector {
	struct sobj_header h;
	uint8_t bytes[];
};


sval_t
make_bytevector(size_t len, sval_t *liveobjects, sval_t nobjs)
{
	struct sobj_bytevector *bv;
	size_t objlen;

	objlen = sizeof(struct sobj_bytevector) + len;
	bv = mem_alloc(objlen, liveobjects, nobjs);
	bv->h.word = objlen;
	return (sval_t)bv + SVAL_TAG_BYTEVECTOR;
}

static struct sobj_bytevector *
sobj_bytevector(sval_t v)
{
	return (struct sobj_bytevector *)(v - SVAL_TAG_BYTEVECTOR);
}

static sval_t
sobj_bytevector_length(const struct sobj_bytevector *bv)
{
	return sobj_h_len(&bv->h) - sizeof(struct sobj_bytevector);
}

void
sval_bytevector_ref(sval_t v, uint8_t **cpp, sval_t *lenp)
{
	struct sobj_bytevector *bv = sobj_bytevector(v);
	*cpp = bv->bytes;
	*lenp = sobj_bytevector_length(bv);
}

void
bytevector_copy_b(sval_t v_to, sval_t c_at, sval_t v_from, sval_t c_start,
		  sval_t c_end)
{
	struct sobj_bytevector *bv_src = sobj_bytevector(v_from);
	struct sobj_bytevector *bv_dst = sobj_bytevector(v_to);
	memmove(bv_dst->bytes + c_at, bv_src->bytes + c_start, c_end - c_start);
}

sval_t
bytevector_literal(sval_t len, const uint8_t *bytes)
{
	struct sobj_bytevector *bv;
	sval_t v;

	v = make_bytevector((size_t)len, NULL, 0);
	bv = sobj_bytevector(v);
	memcpy(bv->bytes, bytes, len);
	return v;
}

uint8_t *
bytevector_bytes(sval_t v)
{
	struct sobj_bytevector *bv = sobj_bytevector(v);
	return bv->bytes;
}

sval_t
g_bytevector_copy_b(sval_t to, sval_t at, sval_t from, sval_t start,
		    sval_t end)
{
	sval_t c_at    = cval_fixnum(at);
	sval_t c_start = cval_fixnum(start);
	sval_t c_end   = cval_fixnum(end);

	if (!bytevector_p(to))
		raise_cstr("bytevector-copy!: not a bytevector: to");
	if (!bytevector_p(from))
		raise_cstr("bytevector-copy!: not a bytevector: from");

	bytevector_copy_b(to, c_at, from, c_start, c_end);
	return SVAL_UNSPECIFIED;
}

sval_t
bytevector_equal(sval_t v0, sval_t v1)
{
	const struct sobj_bytevector *bv0, *bv1;
	sval_t i, len;

	bv0 = sobj_bytevector(v0);
	bv1 = sobj_bytevector(v1);
	len = sobj_bytevector_length(bv0);
	if (len != sobj_bytevector_length(bv1))
		return SVAL_FALSE;
	for (i = 0; i < len; i++) {
		if (bv0->bytes[i] != bv1->bytes[i])
			return SVAL_FALSE;
	}
	return SVAL_TRUE;
}

sval_t
bytevector_length(sval_t v)
{
	const struct sobj_bytevector *bv = sobj_bytevector(v);
	return sobj_bytevector_length(bv);
}

void
bytevector_write(sval_t v)
{
	struct sobj_bytevector *bv = sobj_bytevector(v);
	sval_t len;
	int i;

	printf("#u8(");
	len = sobj_bytevector_length(bv);
	if (len > 0) {
		printf("%u", bv->bytes[0]);
		for (i = 1; i < len; i++)
			printf(" %u", bv->bytes[i]);
	}
	printf(")");
}

sval_t
g_bytevector_2d_length(sval_t v)
{
	struct sobj_bytevector *bv = sobj_bytevector(v);
	return sval_fixnum(sobj_bytevector_length(bv));
}

sval_t
g_bytevector_2d_u8_2d_ref(sval_t v, sval_t k)
{
	struct sobj_bytevector *bv = sobj_bytevector(v);
	return sval_fixnum(bv->bytes[fixnum(k)]);
}

sval_t
g_bytevector_u8_set_b(sval_t v_bytevector, sval_t v_k, sval_t v_byte)
{
	struct sobj_bytevector *bv = sobj_bytevector(v_bytevector);
	sval_t idx = cval_fixnum(v_k);

	if (idx < 0 || idx >= sobj_bytevector_length(bv))
		raise_cstr("bytevector-u8-set!: k is out of range");
	bv->bytes[idx] = cval_fixnum(v_byte);
	return SVAL_UNSPECIFIED;
}

sval_t
g_bytevector_3f_(sval_t v)
{
	return (bytevector_p(v)) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_make_bytevector1(sval_t v_k)
{
	return make_bytevector(cval_fixnum(v_k), NULL, 0);
}

sval_t
g_make_bytevector2(sval_t v_k, sval_t v_byte)
{
	struct sobj_bytevector *bv;
	sval_t v_bv;
	size_t len;

	len = cval_fixnum(v_k);
	v_bv = make_bytevector(len, NULL, 0);
	bv = sobj_bytevector(v_bv);
	memset(bv->bytes, cval_fixnum(v_byte), len);
	return v_bv;
}
