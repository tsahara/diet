void raise_cstr(const char *);

sval_t g_raise(sval_t);
void mark_exception_handler(void);
sval_t s_scheme_2d_default_2d_exception_2d_handler(sval_t);

static inline int
error_p(sval_t v)
{
	return (v & 0xf) == SVAL_TAG_ERROR;
}
