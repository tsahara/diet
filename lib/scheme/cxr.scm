(define-library (scheme cxr)
  (import (scheme base))
  (export caaar caadr cadar caddr
          cdaar cdadr cddar cdddr
          caaaar caaadr caadar caaddr cadaar cadadr caddar cadddr
          cdaaar cdaadr cdadar cdaddr cddaar cddadr cdddar cddddr)
  (begin
    (define (caaar l) (car (car (car l))))
    (define (caadr l) (car (car (cdr l))))
    (define (cadar l) (car (cdr (car l))))
    (define (caddr l) (car (cdr (cdr l))))
    (define (cdaar l) (cdr (car (car l))))
    (define (cdadr l) (cdr (car (cdr l))))
    (define (cddar l) (cdr (cdr (car l))))
    (define (cdddr l) (cdr (cdr (cdr l))))
    (define (caaaar l) (car (car (car (car l)))))
    (define (caaadr l) (car (car (car (cdr l)))))
    (define (caadar l) (car (car (cdr (car l)))))
    (define (caaddr l) (car (car (cdr (cdr l)))))
    (define (cadaar l) (car (cdr (car (car l)))))
    (define (cadadr l) (car (cdr (car (cdr l)))))
    (define (caddar l) (car (cdr (cdr (car l)))))
    (define (cadddr l) (car (cdr (cdr (cdr l)))))
    (define (cdaaar l) (cdr (car (car (car l)))))
    (define (cdaadr l) (cdr (car (car (cdr l)))))
    (define (cdadar l) (cdr (car (cdr (car l)))))
    (define (cdaddr l) (cdr (car (cdr (cdr l)))))
    (define (cddaar l) (cdr (cdr (car (car l)))))
    (define (cddadr l) (cdr (cdr (car (cdr l)))))
    (define (cdddar l) (cdr (cdr (cdr (car l)))))
    (define (cddddr l) (cdr (cdr (cdr (cdr l)))))
    ))

;(let1 adlist (cartesian-product '(("a" "d") ("a" "d") ("a" "d") ("a" "d")))
;  (display (map (^a (string-join (append '("c") a '("r")) "")) adlist))
;  (newline)
;
;  (for-each (lambda (a)
;              (let ((name (string-join (append '("c") a '("r")) ""))
;                    )
;                (format #t "(define (~a l) (~a (~a (~a (~a l)))))~%"
;                        name
;                        #"c~(car a)r"
;                        #"c~(cadr a)r"
;                        #"c~(caddr a)r"
;                        #"c~(cadddr a)r")))
;            adlist))
