#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "scm.h"

struct sobj_string {
	struct sobj_header h;
	uint8_t bytes[];
};


static struct sobj_string *
sobj_string(sval_t v)
{
	return (struct sobj_string *)(v - SVAL_TAG_STR);
}

inline static sval_t
string_length(const struct sobj_string *str)
{
	return sobj_h_len(&str->h) - sizeof(struct sobj_string);
}


size_t
sval_string_copy_cstr(sval_t v, char *buf, size_t bufsize)
{
	struct sobj_string *str = sobj_string(v);
	size_t copylen;

	copylen = string_length(str);
	if (copylen >= bufsize)
		copylen = bufsize - 1;
	memcpy(buf, str->bytes, copylen);
	buf[copylen] = '\0';
	return copylen + 1;
}

static sval_t
sobj_string_length(sval_t v)
{
	return string_length(sobj_string(v));
}

void
sval_string_ref(sval_t v, const uint8_t **cpp, sval_t *lenp)
{
	struct sobj_string *str = sobj_string(v);
	*cpp = str->bytes;
	*lenp = string_length(str);
}

sval_t
make_string(sval_t len, uint8_t *cp)
{
	struct sobj_string *str;
	size_t objlen;

	objlen = sizeof(struct sobj_string) + len;
	str = mem_alloc(objlen, NULL, 0);
	str->h.word = objlen;
	if (cp != NULL)
		memcpy(str->bytes, cp, len);
	return (sval_t)str | SVAL_TAG_STR;
}

sval_t
make_string2(sval_t len)
{
	return make_string(len, NULL);
}

sval_t
make_string_cstr(const char *cp)
{
	return make_string(strlen(cp), (uint8_t *)cp);
}

sval_t
g_make_string_1(sval_t k)
{
	return make_string2(fixnum(k));
}

sval_t
g_make_string_2(sval_t k, sval_t ch)
{
	struct sobj_string *str;
	uint8_t c_ch;
	sval_t i, c_len, s;

	c_len = fixnum(k);
	c_ch = cval_char(ch);
	s = make_string2(c_len);
	str = sobj_string(s);
	for (i = 0; i < c_len; i++)
		str->bytes[i] = c_ch;
	return s;
}

sval_t
g_string_copy3(sval_t v_string, sval_t v_start, sval_t v_end)
{
	struct sobj_string *str = sobj_string(v_string);
	sval_t c_end, c_start;

	c_start = cval_fixnum(v_start);
	c_end   = cval_fixnum(v_end);
	if (c_start > c_end)
		raise_cstr("start > end");
	if (c_start < 0)
		c_start = 0;
	if (c_end > string_length(str))
		c_end = string_length(str);
	return make_string(c_end - c_start, str->bytes + c_start);
}

sval_t
g_string_copy_b(sval_t v_to, sval_t v_at, sval_t v_from, sval_t v_start,
		sval_t v_end)
{
	struct sobj_string *str_to   = sobj_string(v_to);
	struct sobj_string *str_from = sobj_string(v_from);
	sval_t c_at, c_copylen, c_end, c_start;

	c_at = cval_fixnum(v_at);
	if (c_at < 0)
		raise_cstr("string-copy!: \"at\" < 0");
	if (c_at > string_length(str_to))
		raise_cstr("string-copy!: \"at\" > (string-length to)");

	c_start = cval_fixnum(v_start);
	c_end   = cval_fixnum(v_end);
	if (c_start > c_end)
		raise_cstr("string-copy!: start > end");
	c_copylen = c_end - c_start;
	if (c_copylen == 0)
		return SVAL_UNSPECIFIED;
	if (string_length(str_to) - c_at < c_copylen)
		raise_cstr("string-copy!: overrun at the end");
	memmove(str_to->bytes + c_at, str_from->bytes + c_start, c_copylen);
	return SVAL_UNSPECIFIED;
}

sval_t
g_string_set_b(sval_t v_string, sval_t v_k, sval_t v_char)
{
	struct sobj_string *str = sobj_string(v_string);
	sval_t c_idx;

	c_idx = cval_fixnum(v_k);
	if (c_idx < 0 || c_idx >= string_length(str))
		raise_cstr("k is not a valid index");
	str->bytes[c_idx] = (uint8_t)cval_char(v_char);
	return SVAL_UNSPECIFIED;
}

sval_t
g_substring(sval_t ss, sval_t sa, sval_t sb)
{
	struct sobj_string *str = sobj_string(ss);
	sval_t slen;
	int ia, ib;

	slen = sobj_string_length(ss);
	ia = fixnum(sa);
	ib = fixnum(sb);

	if (ia >= slen)
		raise_cstr("start too big");
	if (ib > slen)
		raise_cstr("end too big");

	return make_string(ib - ia, str->bytes + ia);
}

sval_t
g_string_3d__3f_(sval_t a, sval_t b)
{
	sval_t alen, blen;

	alen = sobj_string_length(a);
	blen = sobj_string_length(b);
	if (alen == blen &&
	    memcmp(sobj_string(a)->bytes, sobj_string(b)->bytes, alen) == 0)
		return SVAL_TRUE;
	else
		return SVAL_FALSE;
}

sval_t
g_string_2d_append_c(sval_t l0)
{
	sval_t l, n, result, str, len;
	uint8_t *cp;

	len = 0;
	for (l = l0; pair_p(l); l = pair_cdr(l))
		len += sobj_string_length(pair_car(l));

	if (len == 0)
		return make_string(0, NULL);

	result = make_string2(len);
	cp = sobj_string(result)->bytes;
	for (l = l0; pair_p(l); l = pair_cdr(l)) {
		str = pair_car(l);
		n = sobj_string_length(str);
		if (n > 0) {
			memcpy(cp, sobj_string(str)->bytes, n);
			cp += n;
		}
	}
        return result;
}

sval_t
g_string_2d_length(sval_t v)
{
	return sval_fixnum(sobj_string_length(v));
}

sval_t
g_string_2d_ref(sval_t s, sval_t k)
{
	struct sobj_string *str = sobj_string(s);
	sval_t i;

	i = fixnum(k);
	if (i < 0 || i >= string_length(str))
		raise_cstr("string-ref range error");
	return sval_char(str->bytes[i]);
}

sval_t
g_string_3f_(sval_t v)
{
	return string_p(v) ? SVAL_TRUE : SVAL_FALSE;
}

void
string_write(sval_t v)
{
	struct sobj_string *s = sobj_string(v);
	sval_t len = string_length(s);
	int ch;

	fputc('"', stdout);
	for (sval_t i = 0; i < len; i++) {
		ch = s->bytes[i];
		if (ch == '\\' || ch == '"')
			fputc('\\', stdout);
		fputc(ch, stdout);
	}
	fputc('"', stdout);
}

sval_t
sval_string_to_bytevector(sval_t v_str)
{
	struct sobj_string *s = sobj_string(v_str);
	sval_t v_bv, n;
	uint8_t *cp;

	n = string_length(s);
	v_bv = make_bytevector(n, &v_str, 1);
	cp = bytevector_bytes(v_bv);
	memcpy(cp, s->bytes, n);
	return v_bv;
}
