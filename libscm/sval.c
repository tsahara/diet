#include <stdio.h>

#include "scm.h"

const char *
sval_tag_name(sval_t v)
{
	switch (v & 0x0f) {
	case 0x0: return "fixnum";
	case 0x1: return "pair";
	case 0x2: return "notused:2";
	case 0x3: return "symbol";
	case 0x4: return "string";
	case 0x5: return "procedure";
	case 0x6: return "box:6";
	case 0x7: return "chaacter";
	case 0x8: return "vector";
	case 0x9: return "bytevector";
	case 0xa: return "error";
	case 0xb: return "notused:b";
	case 0xc: return "large";
	case 0xd: return "gfun";
	default:
		switch (v) {
		case SVAL_NIL:         return "nil";
		case SVAL_FALSE:       return "false";
		case SVAL_TRUE:        return "true";
		case SVAL_UNSPECIFIED: return "unspecified";
		case SVAL_EOF_OBJECT:  return "eof-object";
		default:
			return "unknown";
		}
	}
}

sval_t
g_print_sval(sval_t v)
{
	fprintf(stderr, "sval %012lx, tag=%x, ptr=%012lx\n",
		(unsigned long)v,
		(unsigned)(v & 0xf), (unsigned long)sval_to_ptr(v));
	return SVAL_UNSPECIFIED;
}
