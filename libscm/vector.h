sval_t vector_equal(sval_t, sval_t);
void vector_gc_mark(sval_t);
void vector_write(sval_t);

static inline int
vector_p(sval_t v)
{
	return (v & 0xf) == SVAL_TAG_VECTOR;
}
