(import (diet test))
(import (scheme base)
        (scheme case-lambda)
        (scheme cxr))

(assert-true #t)
(assert-false #f)

(assert-eq? 1 1)

;; quote (R7RS p.12)
(assert-true (eq? #t '#t))
(assert-true (eq? #f '#f))
(assert-true (eq? 145932 '145932))
(assert-true (eq? #\a '#\a))
(assert-true (pair? '(1 2)))
(assert-true (string=? "abc" '"abc"))
(assert-true (vector? '#(0 1 2)))
(assert-true (bytevector? '#u8(0 1 2)))

;; lambda (R7RS p.13)
(assert-true ((lambda () #t)))
(assert-true ((lambda (x) x) #t))
(assert-true ((lambda (x y) y) #f #t))
(assert-eq? 1 ((lambda x (car x)) 1 2))
(assert-eq? 2 ((lambda (x . y) (car y)) 1 2 3))

;; if (R7RS p.13)
(define ifvar 0)
(assert-eq? 0 ifvar)

(if #t (set! ifvar 1))
(assert-eq? 1 ifvar)

(if #f (set! ifvar 2))    ;; issue#1
(assert-eq? 1 ifvar)

;; set! (R7RS p.14)
(assert-true ((lambda (a) (set! a #t) a) #f))

(define (f1 a)
  (define (f2)
    (set! a 2))
  (f2)
  a)
(assert-eq? 2 (f1 1))

(define (f3 a)
  (define (f4)
    (define (f5)
      (set! a 2))
    (f5))
  (f4)
  a)
(assert-eq? 2 (f3 1))

;; cond (R7RS p.14)
(assert-eq? 'a (cond ('a) (else #f)))
(assert-true (cond ((> 1 0) #t)))

(define (issue15-1) 1234)    ;; issue#15
(define (issue15-2) (cond (else (issue15-1))))
(assert-eq? 1234 (issue15-2))

;; and (R7RS p.15)
(assert-true (and))

;; when (R7RS p.15)
(define var1 0)
(when #t (set! var1 1))
(assert-eq? 1 var1)
(when #f (set! var1 2))
(assert-eq? 1 var1)

;; unless (R7RS p.15)
(unless #t (set! var1 3))
(assert-eq? 1 var1)
(unless #f (set! var1 4))
(assert-eq? 4 var1)

;; let (R7RS p.16)
(assert-true (let ((x 2) (y 3)) (= (* x y) 6)))
(assert-true (let ((x 1))
               (let ((x 2) (y x))
                 (= (+ x y) 3))))
(assert-true (let () #t))

(let ((issue30 "issue30"))    ;; issue#30
  ((lambda () (set! issue30 1)))
  (assert-eqv? 1 issue30))

;; let* (R7RS p.16)
(assert-true (let* ((x 1) (y x)) (= (+ x y) 2)))

;; letrec (R7RS p.16)
(assert-eq? 0 (letrec ((a (lambda (n) (if (> n 0) (b (- n 1)) n)))
                       (b (lambda (n) (a (- n 2)))))
                (a 3)))

; letrec* (R7RS p.16)
(assert-eq? 0 (letrec* ((a (lambda (n) (if (> n 0) (c (- n 1)) n)))
                        (b (lambda (n) (a (- n 2))))
                        (c b))
                (a 3)))

;; named-let (R7RS p.18)
(assert-true (let loop ((x #t))  ; -> jump
               (if x
                   (loop #f)
                   #t)))

(define (let1 x) 1)
(assert-true (let loop ((x 0))  ; -> letrec
               (if (= x 0)
                   (let1 loop)
                   #t)))

(assert-true (let loop ((x 0))  ; -> letrec
               (if (= x 0)
                   (loop loop)
                   #t)))

(assert-eq? 1 (let loop ((x 0))  ; -> let
                (+ x 1)))

;; let-values (R7RS p.17)
(let ((a 1) (b 2) (c 3))
  (let-values (((a b)   (values b a))
               ((c . d) (values a b c))
               (e       (values a b)))
    (assert-eq? 2 a)
    (assert-eq? 1 b)
    (assert-eq? 1 c)
    (assert-true (pair? d))
    (assert-eq? 2 (car d))
    (assert-eq? 3 (cadr d))
    (assert-true (pair? e))
    (assert-eq? 1 (car e))
    (assert-eq? 2 (cadr e))))

;; let*-values (R7RS p.17)
(let ((a 1) (b 2) (c 3))
  (let*-values (((a b)   (values b a))
                ((c . d) (values a b c))
                (e       (values a b)))
    (assert-eq? 2 a)
    (assert-eq? 1 b)
    (assert-eq? 2 c)
    (assert-true (pair? d))
    (assert-eq? 1 (car d))
    (assert-eq? 3 (cadr d))
    (assert-true (pair? e))
    (assert-eq? 2 (car e))
    (assert-eq? 1 (cadr e))))

;; make-parameter (R7RS p.19)
(let ((p1 (make-parameter 1))
      (p2 (make-parameter 1 (lambda (n) (* n 2)))))
  (assert-eqv? 1 (p1))
  (assert-eqv? 2 (p2)))

(let ((p1 (make-parameter 1))
      (p2 (make-parameter 2 (lambda (n) (* n 2)))))
  (p1 3)
  (p2 4)
  (assert-eqv? 3 (p1))
  (assert-eqv? 8 (p2)))

;; parameterize (R7RS p.20)
(let ((p1 (make-parameter 1)))
  (parameterize ((p1 2))
    (assert-eqv? 2 (p1)))
  (assert-eqv? 1 (p1)))

;; case-lambda (R7RS p.21)
(let ((gf (case-lambda ((a) a)
                       ((a b) (+ a b))
                       ((a . b) (+ a (* (car b) (cadr b))))
                       (a (length a)))))
  (assert-eq? 1 (gf 1))
  (assert-eq? 3 (gf 1 2))
  (assert-eq? 7 (gf 1 2 3))
  (assert-eq? 0 (gf)))

(define (gf2)
  (define f (case-lambda ((a) a) ((a b) (+ a b))))
  (lambda ()
    (f 1 2)))
(assert-eq? 3 ((gf2)))

(define gf3 (case-lambda ((a) a)      ; generic function in a global variable
                         ((a b) (+ a b))))
(assert-eq? 1 (gf3 1))
(assert-eq? 3 (gf3 1 2))

;; define (R7RS p.25)
(define def1 123)
(assert-eq? 123 def1)

(define (def2 a) (+ a 1))
(assert-eq? 2 (def2 1))

(define (def3 . x) (car x))
(assert-eq? 3 (def3 3 4 5))

(define (def3 . x) (car x))
(assert-eq? 3 (def3 3 4 5))

(define def-overwrite 1)
(define def-overwrite 2)
(assert-eq? 2 def-overwrite)

;; internal definitions (R7RS p.26)
(define (idef1)
  (define idef1-x 1)
  (define idef1-y 2)
  (+ idef1-x idef1-y))
(assert-eq? 3 (idef1))

(define (idef2)  ;; forward reference
  (define idef2-x (lambda () idef2-y))
  (define idef2-y 4)
  (idef2-x))
(assert-eq? 4 (idef2))

(define (idef3)  ;; internal definition as a free variable
  (define (idef3-x x)
    (define (idef3-y)
      (define (idef3-z) (idef3-x 6))
      (idef3-z))
    (if (= x 5)
        (idef3-y)
        7))
  (idef3-x 5))
(assert-eq? 7 (idef3))

;; define-values (R7RS p.26)
(define-values (mv1 mv2) (values 1 2))
(assert-eq? 1 mv1)
(assert-eq? 2 mv2)

(define-values (mv3 . mv4) (values 3 4 5))
(assert-eq? 3 mv3)
(assert-eq? 4 (car mv4))
(assert-eq? 5 (cadr mv4))

(define-values mv5 (values 6 7))
(assert-eq? 6 (car mv5))
(assert-eq? 7 (cadr mv5))

(define (internal-define-values-1)
  (define (a) b)
  (define-values (b) (values 8))
  (a))
(assert-eq? 8 (internal-define-values-1))

;; eqv? (R7RS p.30)
(assert-true  (eqv? #t #t))
(assert-true  (eqv? #f #f))
(assert-true  (eqv? 'a 'a))
(assert-false (eqv? 'a 'b))
(assert-true  (eqv? 2 2))
;(assert-true  (eqv? 100000000 100000000))
(assert-true  (eqv? #\a #\a))
(assert-true  (eqv? '() '()))
(let ((p '(1 . 2))
      (v #(1))
      (b #u8(1 2 3))
      ; record
      (s "abc"))
  (assert-true (eqv? p p))
  (assert-true (eqv? v v))
  (assert-true (eqv? b b))
  (assert-true (eqv? s s)))
(let ((proc (lambda (x) x)))
  (assert-true (eqv? proc proc)))
(assert-false (eqv? #t #f))
(assert-false (eqv? 'a 'b))
;(assert-false (eqv? 2 2.0))
;(assert-false (eqv? 0.0 +nan.0))
(assert-false (eqv? 1 2))
(assert-false (eqv? #\a #\b))
(assert-false (eqv? '() '(1)))
(assert-false (eqv? (cons 1 2) (cons 1 2)))
(assert-false (eqv? (lambda () 1) (lambda () 2)))
;(assert-false (let ((p (lambda (x) x))) (eqv? p p)))
(assert-false (eqv? #f 'nil))

;; eq? (R7RS p.31)
(assert-true  (eq? 'a 'a))
(assert-false (eq? (list 'a) (list 'b)))
(assert-true  (eq? '() '()))
(assert-true  (eq? car car))

;; equal? (R7RS p.32)
(assert-true  (equal? #t #t))
(assert-true  (equal? #f #f))
(assert-true  (equal? 'a 'a))
(assert-true  (equal? 123 123))
(assert-true  (equal? #\a #\a))
(assert-true  (equal? '() '()))
(assert-true  (equal? '(a b) '(a b)))
(assert-true  (equal? '(a . b) '(a . b)))     ; issue#24
(assert-true  (equal? #(1 2 3) #(1 2 3)))
(assert-true  (equal? "abc" "abc"))
(assert-true  (equal? #u8(1 2 3) #u8(1 2 3)))

;; number? (R7RS p.35)
(assert-true  (number? 1))
(assert-false (number? 'a))

;; complex? (R7RS p.35)
(assert-true  (complex? 1))

;; real? (R7RS p.35)
(assert-true  (real? 1))

;; rational? (R7RS p.35)
(assert-true  (rational? 1))

;; integer? (R7RS p.35)
(assert-true  (integer? 1))

;; exact? (R7RS p.35)
(assert-true  (exact? 1))

;; inexact? (R7RS p.35)
(assert-false (inexact? 1))

;; exact-integer? (R7RS p.35)
(assert-true  (exact-integer? 1))

;; finite? (R7RS p.35)
(assert-true  (finite? 1))

;; infinite? (R7RS p.35)
(assert-false (infinite? 1))

;; nan? (R7RS p.35)
(assert-false (nan? 1))

;; = (R7RS p.36)
(assert-true (= 1 1))
(assert-true (not (= 1 2)))

;; < (R7RS p.36)
(assert-true (< 1 2))

;; > (R7RS p.36)
(assert-true (> 2 1))

;; <= (R7RS p.36)
(assert-true (<= 1 1))
(assert-true (<= 1 2))
(assert-true (not (<= 2 1)))

;; >= (R7RS p.36)
(assert-true (>= 2 1))
(assert-true (>= 1 1))
(assert-true (not (>= 1 2)))

;; zero? (R7RS p.36)
(assert-true (zero? 0))
(assert-true (not (zero? 1)))

;; positive? (R7RS p.36)
(assert-true (positive? 1))
(assert-true (not (positive? 0)))
(assert-true (not (positive? -1)))

;; negative? (R7RS p.36)
(assert-true (not (negative? 1)))
(assert-true (not (negative? 0)))
(assert-true (negative? -1))

;; odd? (R7RS p.36)
(assert-true (odd? 1))
(assert-true (not (odd? 2)))
(assert-true (odd? -1))
(assert-true (not (odd? -2)))

;; even? (R7RS p.36)
(assert-true (even? 2))
(assert-true (not (even? 1)))
(assert-true (even? -2))
(assert-true (not (even? -1)))

;; max (R7RS p.36)
(assert-eq? 2 (max 1 2))
(assert-eq? 1 (max 1 1))
(assert-eq? 9 (max 3 1 4 1 5 9 2))

;; min (R7RS p.36)
(assert-eqv? 1 (min 1 2))
(assert-eqv? 5 (min 9 8 7 6 5))

;; + (R7RS p.36)
(assert-eq? (+) 0)
(assert-eq? (+ 3) 3)
(assert-eq? (+ 3 4) 7)
(assert-eqv? 0 (apply + '()))
(assert-eqv? 3 (apply + '(3)))
(assert-eqv? 7 (apply + '(3 4)))

;; - (R7RS p.36)
(assert-eq? -123 (- 123))
(assert-eq? 1 (- 3 2))
(assert-eq? -17 (- 3 1 4 1 5 9))

;; * (R7RS p.36)
;(assert-true (eq? (*) 1))
;(assert-true (eq? (* 4) 4))
(assert-eq? (* 3 4) 12)

;; abs (R7RS p.36)
(assert-eq? 7 (abs 7))
(assert-eq? 7 (abs -7))

;; quotient (R7RS p.37)
(assert-eq? 2  (quotient  5  2))
(assert-eq? -2 (quotient -5  2))
(assert-eq? -2 (quotient  5 -2))
(assert-eq? 2  (quotient -5 -2))

;; remainder (R7RS p.37)
(assert-eq?  1 (remainder  5  2))
(assert-eq? -1 (remainder -5  2))
(assert-eq?  1 (remainder  5 -2))
(assert-eq? -1 (remainder -5 -2))

;; modulo (R7RS p.37)
(assert-eq?  1 (modulo  5  2))
(assert-eq?  1 (modulo -5  2))
(assert-eq? -1 (modulo  5 -2))
(assert-eq? -1 (modulo -5 -2))

;; not (R7RS p.40)
(assert-true (not #f))
(assert-true (eq? #f (not #t)))
(assert-true (eq? #f (not '())))

;; boolean? (R7RS p.40)
(assert-true (boolean? #t))
(assert-true (boolean? #f))
(assert-true (not (boolean? '())))

;; boolean=? (R7RS p.40)
(assert-true (boolean=? #t #t))
(assert-true (boolean=? #f #f))
(assert-false (boolean=? #t #t 1))
(assert-false (boolean=? #t #f #t))

;; pair? (R7RS p.41)
(assert-true (pair? '(a . b)))
(assert-true (pair? '(a b c)))
(assert-true (not (pair? '())))

;; cons (R7RS p.41)
(assert-eq? 1 (car (cons 1 2)))
(assert-eq? 2 (cdr (cons 1 2)))

;; car (R7RS p.41)
(assert-eq? 1 (car '(1 . 2)))

;; cdr (R7RS p.41)
(assert-eq? 2 (cdr '(1 . 2)))

;; set-car! (R7RS p.41)
(assert-eq? 3 (car ((lambda (p) (set-car! p 3) p) '(1 . 2))))

;; set-cdr! (R7RS p.42)
(assert-eq? 4 (cdr ((lambda (p) (set-cdr! p 4) p) '(1 . 2))))

;; cxxr (R7RS p.42)
(let ((l '((1 . 2) . (3 . 4))))
  (assert-eq? 1 (caar l))
  (assert-eq? 3 (cadr l))
  (assert-eq? 2 (cdar l))
  (assert-eq? 4 (cddr l)))  ; ordered by proc name

;; cxxxr (R7RS p.42)
(let ((l '(((1 . 2) . (3 . 4)) . ((5 . 6) . (7 . 8)))))
  (assert-eq? 1 (caaar l))
  (assert-eq? 5 (caadr l))
  (assert-eq? 3 (cadar l))
  (assert-eq? 7 (caddr l))
  (assert-eq? 2 (cdaar l))
  (assert-eq? 6 (cdadr l))
  (assert-eq? 4 (cddar l))
  (assert-eq? 8 (cdddr l)))

;; cxxxxr...

;; null? (R7RS p.42)
(assert-true (null? '()))
(assert-true (not (null? #f)))

;; list? (R7RS p.42)
(assert-true (list '(a b c)))
(assert-true (list '()))
(assert-false (list? '(a . b)))

;; make-list (R7RS p.42)
(assert-eq? 3 (length (make-list 3)))
(assert-eq? 4 (length (make-list 4 'a)))
(assert-eq? 'a (car (make-list 4 'a)))

;; list (R7RS p.42)
(assert-eq? 1 (car (list 1)))
(assert-eq? 2 (car (cdr (list 1 2))))
(assert-eq? '() (list))

;; length (R7RS p.42)
(assert-eq? 0 (length '()))
(assert-eq? 3 (length '(1 2 3)))

;; append (R7RS p.42)
(assert-equal? '() (append))
(assert-equal? 1 (append 1))
(assert-equal? '(a b c d) (append '(a) '(b c d)))
(assert-equal? '(a b c . d) (append '(a b) '(c .d)))
(assert-equal? 'a (append '() 'a))

;; reverse (R7RS p.42)
(assert-equal? '(c b a) (reverse '(a b c)))

;; list-tail (R7RS p.42)
(let ((l '(1 2 3)))
  (assert-eq? l (list-tail l 0))
  (assert-eq? (cdr l) (list-tail l 1))
  (assert-eq? '() (list-tail l 3)))

;; list-ref (R7RS p.42)
(assert-eq? 'c (list-ref '(a b c d) 2))

;; list-set! (R7RS p.43)
(let ((l (list 'a 'X 'c)))
  (list-set! l 1 'b)
  (assert-equal? '(a b c) l))

;; memq (R7RS p.43)
(let ((l '(a b c)))
  (assert-eq? (cdr l) (memq 'b l))
  (assert-false (memq 'x l)))

;; memv (R7RS p.43)
(let ((l '(a b c)))
  (assert-eq? (cdr l) (memv 'b l))    ; Note: eq? = eqv? currently
  (assert-false (memv 'x l)))

;; member (R7RS p.43)
(let ((l '(1 2 3)))
  (assert-eq? (cdr l) (member 2 l))
  (assert-false (member 'x l))
  (assert-eq? (cddr l) (member 7 l (lambda (a b) (= (+ a b) 10)))))

;; assq (R7RS p.43)
(let ((e '((a 1) (b 2) (c 3))))
  (assert-eq? (cadr e) (assq 'b e))
  (assert-false (assq 'x e)))

;; assv (R7RS p.43)
(let ((e '((a 1) (b 2) (c 3))))
  (assert-eq? (cadr e) (assv 'b e))    ; Note: eq? = eqv? currently
  (assert-false (assv 'x e)))

;; assoc (R7RS p.43)
(let ((e '((1 a) (2 b) (3 c))))
  (assert-eq? (cadr e) (assoc 2 e))
  (assert-false (assoc 9 e))
  (assert-eq? (caddr e) (assoc 7 e (lambda (a b) (= (+ a b) 10)))))

;; list-copy (R7RS p.43)
(assert-equal? '(1 2) (list-copy '(1 2)))
(assert-equal? '(3 . 4) (list-copy '(3 . 4)))
(assert-eq? 1 (list-copy 1))

;; symbol? (R7RS p.44)
(assert-true (symbol? 'foo))
(assert-true (not (symbol? '())))

;; symbol=? (R7RS p.44)
(assert-true (symbol=? 'foo 'foo))
(assert-false (symbol=? 'foo 'bar))

;; issue#8
(define (sym1) 'symbol1)
(assert-true (symbol=? 'symbol1 (sym1)))

;; symbol->string (R7RS p.44)
(assert-true (equal? "flying-fish" (symbol->string 'flying-fish)))

;; char? (R7RS p.44)
(assert-true (char? #\a))
(assert-true (not (char? #f)))

;; char->integer (R7RS p.45)
(assert-eq? 48 (char->integer #\0))

;; integer->char (R7RS p.45)
(assert-eq? #\0 (integer->char 48))

;; string? (R7RS p.46)
(assert-true (string? "abc"))
(assert-true (not (string? 'abc)))

;; make-string (R7RS p.46)
(assert-eq? 5 (string-length (make-string 5)))
(assert-eq? 5 (string-length (make-string 5 #\a)))
(assert-eq? #\a (string-ref (make-string 5 #\a) 1))

;; string-length (R7RS p.46)
(assert-true (= 3 (string-length "abc")))

;; string-ref (R7RS p.46)
(assert-true (eq? #\b (string-ref "abc" 1)))

;; string-set! (R7RS p.46)
(let ((abc (string-copy "abc"))
      (def (string-copy "def")))
  (string-set! abc 0 #\x)
  (string-set! def 2 #\x)
  (assert-equal? abc "xbc")
  (assert-equal? def "dex"))

;; string=? (R7RS p.46)
(assert-true (string=? "abc" "abc"))
(assert-false (string=? "abc" "cba"))
(assert-false (string=? "abc" "abcde"))
(assert-true (string=? "abc" "abc" "abc"))
(assert-false (string=? "abc" "abc" "xxx"))

;; substring (R7RS p.47)
(assert-true (string=? "bcd" (substring "abcde" 1 4)))

;; string-append (R7RS p.47)
(assert-true (string=? "" (string-append)))
(assert-true (string=? "abcdefghi" (string-append "abc" "def" "ghi")))

;; string->list (R7RS p.47)
(assert-equal? '(#\a #\b #\c) (string->list "abc"))
(assert-equal? '(#\b #\c)     (string->list "abc" 1))
(assert-equal? '(#\b)         (string->list "abc" 1 2))
(assert-equal? '()            (string->list "abc" 2 1))

;; list->string (R7RS p.47)
(assert-equal? "" (list->string '()))
(assert-equal? "abc" (list->string '(#\a #\b #\c)))

;; string-copy (R7RS p.47)
(assert-equal? "abc" (string-copy "abc"))
(assert-equal? "bc" (string-copy "abc" 1))
(assert-equal? "b" (string-copy "abc" 1 2))

;; string-copy! (R7RS p.47)
(let ((s (string-copy "abcde")))
  (string-copy! s 2 "xyz")
  (assert-equal? "abxyz" s))
(let ((s (string-copy "abcde")))
  (string-copy! s 1 "xyz" 1)
  (assert-equal? "ayzde" s))
(let ((s (string-copy "abcde")))
  (string-copy! s 2 s 0 3)
  (assert-equal? "ababc" s))

;; string-fill! (R7RS p.48)
(let ((s1 (string-copy "abcde"))
      (s2 (string-copy "abcde"))
      (s3 (string-copy "abcde")))
  (string-fill! s1 #\x)
  (string-fill! s2 #\x 2)
  (string-fill! s3 #\x 1 4)
  (assert-equal? "xxxxx" s1)
  (assert-equal? "abxxx" s2)
  (assert-equal? "axxxe" s3))

;; vector? (R7RS p.48)
(assert-true (vector? #(1)))
(assert-true (not (vector? 123)))

;; make-vector (R7RS p.48)
(assert-eq? 3 (vector-length (make-vector 3)))
(assert-eq? 5 (vector-length (make-vector 5 #t)))
(assert-eq? 'a (vector-ref (make-vector 3 'a) 1))

;; vector (R7RS p.48)
(assert-equal? #() (vector))
(assert-equal? #(1 2 3) (vector 1 2 3))

;; vector-length (R7RS p.48)
(assert-eq? 1 (vector-length #(1)))

;; vector-ref (R7RS p.48)
(assert-eq? 1 (vector-ref #(1) 0))

;; vector-set! (R7RS p.48)
(let ((v (make-vector 3 0)))
  (vector-set! v 0 7)
  (vector-set! v 1 8)
  (vector-set! v 2 9)
  (assert-equal? #(7 8 9) v))

;; vector->list (R7RS p.48)
(assert-equal? '(1 2 3) (vector->list #(1 2 3)))
(assert-equal? '(2 3)   (vector->list #(1 2 3) 1))
(assert-equal? '(2)     (vector->list #(1 2 3) 1 2))

;; list->vector (R7RS p.48)
(assert-equal? #(1 2 3) (list->vector '(1 2 3)))

;; vector->string (R7RS p.48)
(assert-equal? "abc" (vector->string #(#\a #\b #\c)))
(assert-equal? "bc"  (vector->string #(#\a #\b #\c) 1))
(assert-equal? "b"   (vector->string #(#\a #\b #\c) 1 2))

;; string->vector (R7RS p.48)
(assert-equal? #(#\a #\b #\c) (string->vector "abc"))
(assert-equal? #(#\b #\c)     (string->vector "abc" 1))
(assert-equal? #(#\b)         (string->vector "abc" 1 2))

;; vector-copy (R7RS p.49)
(assert-equal? #(0 1 2) (vector-copy #(0 1 2)))
(assert-equal? #(1 2)   (vector-copy #(0 1 2) 1))
(assert-equal? #(1)     (vector-copy #(0 1 2) 1 2))

;; vector-copy! (R7RS p.49)
(let ((a (list->vector '(0 1 2 3 4)))
      (b (list->vector '(0 1 2 3 4)))
      (c (list->vector '(0 1 2 3 4))))
  (vector-copy! a 2 #(7 8 9))
  (assert-equal? #(0 1 7 8 9) a)

  (vector-copy! b 1 #(6 7 8 9) 1)
  (assert-equal? #(0 7 8 9 4) b)

  (vector-copy! c 0 #(5 6 7 8 9) 1 4)
  (assert-equal? #(6 7 8 3 4) c))

;; vector-append (R7RS p.49)
(assert-equal? #() (vector-append))
(assert-equal? #(1) (vector-append #(1)))
(assert-equal? #(1 2 3) (vector-append #(1) #(2 3)))

;; vector-fill! (R7RS p.49)
(let ((a (make-vector 3 0))
      (b (make-vector 3 0))
      (c (make-vector 3 0)))
  (vector-fill! a 1)
  (assert-equal? #(1 1 1) a)

  (vector-fill! b 2 1)
  (assert-equal? #(0 2 2) b)

  (vector-fill! c 3 1 2)
  (assert-equal? #(0 3 0) c))

;; bytevector? (R7RS p.49)
(assert-true (bytevector? #u8(1)))
(assert-true (not (bytevector? #t)))

;; make-bytevector (R7RS p.49)
(assert-true (bytevector? (make-bytevector 1)))
(assert-eq? 5 (bytevector-length (make-bytevector 5)))
(assert-eq? 12 (bytevector-u8-ref (make-bytevector 3 12) 2))

;; bytevector (R7RS p.49)
(let ((bv (bytevector 1 0 255)))
  (assert-true (bytevector? bv))
  (assert-equal? #u8(1 0 255) bv))

;; bytevector-length (R7RS p.50)
(assert-eq? 3 (bytevector-length #u8(1 2 3)))

;; bytevector-u8-ref (R7RS p.50)
(assert-eq? 2 (bytevector-u8-ref #u8(1 2 3) 1))

;; bytevector-u8-set! (R7RS p.50)
(let ((a (make-bytevector 3 0)))
  (bytevector-u8-set! a 0 1)
  (bytevector-u8-set! a 1 2)
  (bytevector-u8-set! a 2 3)
  (assert-equal? #u8(1 2 3) a))

;; bytevector-copy (R7RS p.50)
(assert-equal? #u8(1 2 3) (bytevector-copy #u8(1 2 3)))
(assert-equal? #u8(2 3)   (bytevector-copy #u8(1 2 3) 1))
(assert-equal? #u8(2)     (bytevector-copy #u8(1 2 3) 1 2))

;; bytevector-copy! (R7RS p.50)
(let ((a (bytevector 1 2 3))
      (b (bytevector 1 2 3 4))
      (c (bytevector 1 2 3 4 5)))
  (bytevector-copy! a 0 #u8(4 5))
  (assert-equal? #u8(4 5 3) a)

  (bytevector-copy! b 1 #u8(5 6 7) 1)
  (assert-equal? #u8(1 6 7 4) b)

  (bytevector-copy! c 1 #u8(4 5 6 7 8) 1 4)
  (assert-equal? #u8(1 5 6 7 5) c)
  )

;; bytevector-append (R7RS p.50)
(assert-equal? #u8() (bytevector-append))
(assert-equal? #u8(1) (bytevector-append #u8(1)))
(assert-equal? #u8(1 2 3) (bytevector-append #u8(1) #u8(2 3)))

;; procedure? (R7RS p.50)
(define (procp1) 1)
(define-generic (procp2) 1)
(assert-true (procedure? procp1))
(assert-true (procedure? procp2))

;; apply (R7RS p.50)
(assert-eq? 9 (apply (lambda (a) (+ a 8)) '(1)))
(assert-eq? 3 (apply (lambda (a b) (+ a b)) '(1 2)))
(assert-eq? 6 (apply (lambda (a b c) (+ a b c)) 1 2 '(3)))

;; map (R7RS p.51)
(assert-equal? '() (map number? '()))
(assert-equal? '(2 4 6) (map (lambda (n) (* n 2)) '(1 2 3)))

;; for-each (R7RS p.51)
(let ((b 0))
  (for-each (lambda (n) (set! b (+ b n))) '(1 2 3 4 5))
  (assert-eqv? 15 b))
(let ((axe #t))
  (for-each (lambda () (set! axe #f)) '())  ;; r7rs-unspecified
  (assert-true axe))

;; string-for-each (R7RS p.52)
(let ((i 0))
  (string-for-each (lambda (ch) (set! i (+ i (char->integer ch))))
                   "abc")
  (assert-eqv? (+ 97 98 99) i))

;; vector-for-each (R7RS p.52)
(let ((i 0))
  (vector-for-each (lambda (n) (set! i (+ i n))) #(1 2 3))
  (assert-eqv? 6 i))

;; call/cc (R7RS p.52)
(assert-eq? 1 (call/cc (lambda (k) (k 1) 2)))

(define (callcc1 a b)
  (if (call/cc (lambda (k)
                  (set! b k)
                  #t))
      (begin
        (set! a 2)
        (b #f))
      a))
(assert-eq? 2 (callcc1 1 #f))    ; ref. #12

;; values & call-with-values (R7RS p.53)
(assert-eq? 3 (call-with-values (lambda () (values))
                                (lambda () 3)))
(assert-eq? 5 (call-with-values (lambda () (values 4 5))
                                (lambda (a b) b)))

;; dynamic-wind (R7RS p.53)
(let ((result '()))
  (assert-eqv? 20 (dynamic-wind
                      (lambda ()
                        (set! result (cons 1 result))
                        10)
                      (lambda ()
                        (set! result (cons 2 result))
                        20)
                      (lambda ()
                        (set! result (cons 3 result))
                        30)))
  (assert-equal? '(3 2 1) result))

(let ((result '())
      (cont   #f))
  (dynamic-wind
      (lambda () (set! result (cons 1 result)))
      (lambda ()
        (if (call/cc (lambda (c)
                       (set! cont c)
                       #t))
            (set! result (cons 2 result))
            (begin
              (set! result (cons 4 result))
              (set! cont #f))))
      (lambda () (set! result (cons 3 result))))
  (if cont (cont #f))
  (assert-equal? '(3 4 1 3 2 1) result))

;; current-input-port (R7RS p.56)
;; - depends on read-char
(let ((strport1 (open-input-string "abc"))
      (strport2 (open-input-string "xyz")))
  (parameterize ((current-input-port strport1))
    (assert-eq? #\a (read-char))
    (parameterize ((current-input-port strport2))
      (assert-eq? #\x (read-char)))
    (assert-eq? #\b (read-char))))

;; current-output-port (R7RS p.56)
;; - depends on write-char
(let ((strport1 (open-output-string))
      (strport2 (open-output-string)))
  (parameterize ((current-output-port strport1))
    (write-char #\a)
    (assert-equal? "a" (get-output-string strport1))
    (parameterize ((current-output-port strport2))
      (write-char #\x)
      (assert-equal? "x" (get-output-string strport2)))
    (write-char #\b)
    (assert-equal? "ab" (get-output-string strport1))))

;; current-error-port (R7RS p.56)
;; - depends on write-char
(let ((strport1 (open-output-string))
      (strport2 (open-output-string)))
  (parameterize ((current-error-port strport1))
    (write-char #\a (current-error-port))
    (assert-equal? "a" (get-output-string strport1))
    (parameterize ((current-error-port strport2))
      (write-char #\x (current-error-port))
      (assert-equal? "x" (get-output-string strport2))
      (assert-equal? "a" (get-output-string strport1)))))

;; open-input-bytevector (R7RS p.57)
(let ((port (open-input-bytevector #u8(97 98))))
  (assert-true (input-port? port))
  (assert-eq? 97 (read-u8 port))
  (assert-eq? 98 (read-u8 port))
  (assert-true (eof-object? (read-u8 port)))
  (close-port port))

;; open-output-bytevector (R7RS p.57)
(let ((port (open-output-bytevector)))
  (assert-true (output-port? port))
  (close-port port))

;; get-output-bytevector (R7RS p.57)
(let ((port (open-output-bytevector)))
  (write-u8 97 port)
  (write-u8 98 port)
  (write-u8 99 port)
  (assert-equal? #u8(97 98 99) (get-output-bytevector port))
  (close-port port))

;; read-char (R7RS p.57)
(let ((port (open-input-string "ab")))
  (parameterize ((current-input-port port))
    (assert-eq? #\a (read-char))
    (assert-eq? #\b (read-char port))
    (assert-true (eof-object? (read-char))))
  (close-port port))

;; peek-char (R7RS p.57)
(let ((port (open-input-string "a")))
  (parameterize ((current-input-port port))
    (assert-eq? #\a (peek-char))
    (assert-eq? #\a (peek-char port))
    (read-char)
    (assert-true (eof-object? (peek-char))))
  (close-port port))

;; read-u8 (R7RS p.58)
(let ((port (open-input-bytevector #u8(97 98))))
  (parameterize ((current-input-port port))
    (assert-eqv? 97 (read-u8))
    (assert-eqv? 98 (read-u8 port))
    (assert-true (eof-object? (read-u8))))
  (close-port port))

;; peek-u8 (R7RS p.58)
(let ((port (open-input-bytevector #u8(97))))
  (parameterize ((current-input-port port))
    (assert-eqv? 97 (peek-u8))
    (assert-eqv? 97 (peek-u8 port))
    (read-u8)
    (assert-true (eof-object? (peek-u8))))
  (close-port port))

;; read-bytevector (R7RS p.58)
(let ((port (open-input-bytevector #u8(97 98 99))))
  (parameterize ((current-input-port port))
    (assert-equal? #u8() (read-bytevector 0 port))
    (assert-equal? #u8(97) (read-bytevector 1))
    (assert-equal? #u8(98 99) (read-bytevector 2 port))
    (assert-true (eof-object? (read-bytevector 1 port))))
  (close-port port))

;; read-bytevector! (R7RS p.58)
(let ((port1 (open-input-bytevector #u8(1)))
      (port2 (open-input-bytevector #u8(2)))
      (port3 (open-input-bytevector #u8(3 4 5 6)))
      (port4 (open-input-bytevector #u8(7 8 9)))
      (buf   (bytevector 10 20 30)))
  (parameterize ((current-input-port port1))
    (assert-eqv? 1 (read-bytevector! buf))
    (assert-equal? #u8(1 20 30) buf)
    (assert-true (eof-object? (read-bytevector! buf)))

    (assert-eqv? 1 (read-bytevector! buf port2))
    (assert-equal? #u8(2 20 30) buf)

    (assert-eqv? 2 (read-bytevector! buf port3 1))
    (assert-equal? #u8(2 3 4) buf)

    (assert-eqv? 1 (read-bytevector! buf port4 1 2))
    (assert-equal? #u8(2 7 4) buf))
  (close-port port1)
  (close-port port2))

;; write-char (R7RS p.59)
(let ((strport1 (open-output-string))
      (strport2 (open-output-string)))
  (parameterize ((current-output-port strport1))
    (write-char #\a)
    (assert-equal? "a" (get-output-string strport1))
    (write-char #\x strport2)
    (assert-equal? "x" (get-output-string strport2)))
    (write-char #\b strport1)
    (assert-equal? "ab" (get-output-string strport1)))

;; write-string (R7RS p.59)
(let ((strport1 (open-output-string))
      (strport2 (open-output-string)))
  (parameterize ((current-output-port strport1))
    (write-string "1")
    (assert-equal? "1" (get-output-string strport1))
    (write-string "a" strport2)
    (assert-equal? "a" (get-output-string strport2))
    (write-string "ab" strport2 1)
    (assert-equal? "ab" (get-output-string strport2))
    (write-string "abcde" strport2 2 4)
    (assert-equal? "abcd" (get-output-string strport2))
    ))

;; write-u8 (R7RS p.59)
(let ((bvport1 (open-output-bytevector))
      (bvport2 (open-output-bytevector)))
  (parameterize ((current-output-port bvport1))
    (write-u8 97)
    (assert-equal? #u8(97) (get-output-bytevector bvport1))
    (write-u8 120 bvport2)
    (assert-equal? #u8(120) (get-output-bytevector bvport2))
    (write-u8 98 bvport1)
    (assert-equal? #u8(97 98) (get-output-bytevector bvport1)))
  (close-port bvport1)
  (close-port bvport2))

;; write-bytevector (R7RS p.59)
(let ((port (open-output-bytevector)))
  (write-bytevector #u8(1) port)
  (write-bytevector #u8(2 3) port)
  (write-bytevector #u8(3 4 5) port 1)
  (write-bytevector #u8(5 6 7 8) port 1 3)
  (assert-equal? #u8(1 2 3 4 5 6 7) (get-output-bytevector port))
  (close-port port))

;; multiple-values
(call-with-values (lambda () (if #t (values 1 2) (values 3 4)))
  (lambda (a b)
    (assert-eqv? 1 a)
    (assert-eqv? 2 b)))
(call-with-values (lambda () (if #f (values 1 2) (values 3 4)))
  (lambda (a b)
    (assert-eqv? 3 a)
    (assert-eqv? 4 b)))

;; global variable
(define gv1 123)
(assert-eq? 123 gv1)
(set! gv1 234)
(assert-eq? 234 gv1)

(define v1 123)
(define (scope1 v1) v1)
(assert-eq? 234 (scope1 234))

(define gv2 0)
(define (gv2-update)
  (let ((set-gv2 (lambda (n) (set! gv2 n))))
    (set! gv2 1)
    (set-gv2 2)
    gv2))
(assert-eq? 2 (gv2-update))    ; issue#27

;; procedure call
(assert-true ((lambda () #t)))

(assert-eq? 1 ((lambda (a . x) 1) 2))
(assert-eq? 3 ((lambda (a . x) 3) 4 5))
(assert-eq? 6 ((lambda x 6)))
(assert-eq? 7 ((lambda x 7) 8))

(define (argcheck0) #t)
(assert-exception (lambda () (argcheck0 1)))
(assert-exception (lambda () ((lambda (a) #t) 1 2)))

;; recursion
(define (rec1 a b) ;; recursion of global procedure
  (if (= a 0)
      b
      (rec1 (- a 1) (+ a b))))
(assert-eq? 6 (rec1 3 0))

(define (rec2) ;; recursion of non-global procedure
  (define (rec2a a b)
    (if (= a 0)
        b
        (rec2a (- a 1) (+ a b))))
  (rec2a 4 0))
(assert-eq? 10 (rec2))

(define (rec3 . a)  ;; vararg recursive procedure
  (if (pair? a)
      (if (= (car a) 1)
          (rec3 2)
	  2)
      3))
(assert-eq? 2 (rec3 1))

(define (pp1 arg1)  ;; calling parent procedure
  (define (pp2 arg2)
    (define (pp3 arg3)
      (if (= arg3 1)
          (pp2 2)
          3))
    (if (= arg2 1)
        (pp3 1)
        2))
  (pp2 1))
(assert-eq? 2 (pp1 1))

;; arity
(define (arity1 a) 1)
(assert-eq? 1 (arity arity1))

;; untyped procedure call
(define (untyped-proc-1 x y)   (+ x y))
(define (untyped-proc-2 x . y) (+ x (car y)))
(define (untyped-proc-3 . x)   (+ (car x) (cadr x)))

(define (tailcall-untyped f)
  (f 1 2))
(define (normalcall-untyped f)
  (* 3 (f 1 2)))

(assert-eq? 3 (tailcall-untyped untyped-proc-1))
(assert-eq? 3 (tailcall-untyped untyped-proc-2))
(assert-eq? 3 (tailcall-untyped untyped-proc-3))
(assert-eq? 9 (normalcall-untyped untyped-proc-1))
(assert-eq? 9 (normalcall-untyped untyped-proc-2))
(assert-eq? 9 (normalcall-untyped untyped-proc-3))

(define untyped-global-proc (begin (lambda (n) n)))
(assert-eq? 1 (untyped-global-proc 1))

;; higher-order function
(define (hof1 x) (lambda (y) (+ x y)))
(assert-eq? 3 ((hof1 1) 2))

(define (hof2 x) (lambda y (+ x (car y))))
(assert-eq? 7 ((hof2 3) 4))

;; port library
(let ((strport1 (open-output-string)))
  (write-u8 97 strport1)
  (write-u8 98 strport1)
  (write-u8 99 strport1)
  (assert-true (string=? "abc" (get-output-string strport1))))

;; output-port-open?
(let ((osp (open-output-string))
      (isp (open-input-string "")))
  (assert-true  (output-port-open? osp))
  (close-output-port osp)
  (assert-false (output-port-open? osp))
  (assert-false (output-port-open? #t))
  (assert-false (output-port-open? isp))
  (close-input-port isp))

;; eof-object & eof-object?
(assert-true  (eof-object? (eof-object)))
(assert-false (eof-object? #t))

;; read-bytevector! (p.58)
(let ((bv1  (make-bytevector 1 0))
      (bv5  (make-bytevector 5 0))
      (port (open-input-string "abc")))
  (assert-equal? 1 (read-bytevector! bv1 port))
  (assert-equal? #u8(97) bv1)
  (assert-equal? 2 (read-bytevector! bv5 port))
  (assert-equal? #u8(98 99 0 0 0) bv5)
  (assert-true (eof-object? (read-bytevector! bv1 port))))

;;;
;;; (scheme list)
;;;
(assert-true (every number? '()))
(assert-false (every number? '(1 a)))
(assert-eqv? 4 (every (lambda (x) (+ x 1)) '(1 2 3)))

;;;
;;; Non-standard Procedure and Syntax
;;;

(define-generic (dg1 a)   (+ a 1))
(define-generic (dg1 a b) (+ a b 1))
(define-generic (dg1 a b . rest) (+ a b (length rest)))
(assert-equal? 3 (dg1 2))
(assert-equal? 8 (dg1 3 4))
(assert-equal? 6 (dg1 1 2 3 4 5))
(assert-equal? 4 (apply dg1 '(3)))
(assert-equal? 10 (apply dg1 '(4 5)))

(gc)
(exit-test)
