void unspecified_write(sval_t);

static inline int
unspecified_p(sval_t v)
{
	return (v == SVAL_UNSPECIFIED);
}
