#include "scm.h"


struct sobj_large_common {
	struct sobj_header h;
	const struct sobj_large_header *l;
};


void
free_large_object(void *ptr)
{
	struct sobj_large_common *large = ptr;
	(*large->l->free_object)(ptr);
}

void
large_gc_mark(sval_t v)
{
	void *ptr = sval_to_ptr(v);
	struct sobj_large_common *large = ptr;
	(*large->l->mark_object)(ptr);
}
