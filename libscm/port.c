#include <fcntl.h>
#include <unistd.h>

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "scm.h"


struct sobj_port {
	struct sobj_header h;
	const struct sobj_large_header *l;
	sval_t v_buff;
	sval_t write_offset;
	sval_t read_offset;
	int fd;
	unsigned is_input: 1,
		 is_output: 1,
	         is_input_open: 1,
		 is_output_open: 1,
		 is_string: 1,
		 dont_close_fd: 1;
};


static void free_port(void *);
static void port_gc_mark(void *);
const struct sobj_large_header lh_port  = { free_port, port_gc_mark };

static void flush_write_buffer(struct sobj_port *);
sval_t g_close_port(sval_t);
sval_t g_input_port_p(sval_t);
sval_t g_output_port_p(sval_t);
sval_t g_port_p(sval_t);
sval_t g_peek_u8(sval_t v_port);
sval_t g_read_u8(sval_t v_port);
static struct sobj_port *make_file_object(sval_t *, size_t, bool);


static void
flush_write_buffer(struct sobj_port *port)
{
	ssize_t k;
	sval_t len;
	const uint8_t *cp;

	if (port->fd == -1) {
		// extend buffer
		sval_t oldlen = bytevector_length(port->v_buff);
		sval_t newlen = oldlen * 2;
		sval_t v_newbuf = make_bytevector(newlen, NULL, 0);
		uint8_t *newcp = bytevector_bytes(v_newbuf);
		cp = bytevector_bytes(port->v_buff);
		memcpy(newcp, cp, port->write_offset);
		port->v_buff = v_newbuf;
		return;
	}

	cp = bytevector_bytes(port->v_buff);
	len = port->write_offset;
	while (len > 0) {
		k = write(port->fd, cp, (size_t)len);
		if (k <= 0) {
			if (k == -1) {
				// signal an error?
			}
			break;
		}
		cp += k;
		len -= k;
	}
	port->write_offset = 0;
}

void
free_port(void *ptr)
{
	sval_t v_port = ptr_to_sval(ptr, SVAL_TAG_LARGE);
	g_close_port(v_port);
}

void
port_gc_mark(void *ptr)
{
	struct sobj_port *port = ptr;

	if (sobj_h_mark(&port->h))
                return;
        sobj_h_set_mark(&port->h, 1);

	gc_mark(port->v_buff);
}

sval_t
g_close_input_port(sval_t v)
{
	struct sobj_port *port = sval_to_ptr(v);

	if (!port->is_input_open)
		return SVAL_NIL;

	if (port->fd != -1 && !port->dont_close_fd) {
		close(port->fd);
		port->fd = -1;
	}
	port->is_input_open = 0;
	return SVAL_NIL;
}

sval_t
g_close_output_port(sval_t v)
{
	struct sobj_port *port = sval_to_ptr(v);

	if (!port->is_output_open)
		return SVAL_NIL;

	if (port->fd != -1) {
		flush_write_buffer(port);
		if (!port->dont_close_fd) {
			close(port->fd);
			port->fd = -1;
		}
	}
	port->is_output_open = 0;
	return SVAL_NIL;
}

sval_t
g_close_port(sval_t v)
{
	g_close_input_port(v);
	g_close_output_port(v);
	return SVAL_NIL;
}

sval_t
g_eof_object(void)
{
	return SVAL_EOF_OBJECT;
}

sval_t
g_flush_output_port_2(sval_t v_port)
{
	struct sobj_port *port = sval_to_ptr(v_port);

	if (port->is_output_open && port->write_offset > 0)
		flush_write_buffer(port);
	return SVAL_UNSPECIFIED;
}

sval_t
g_eof_object_p(sval_t v_obj)
{
	return sval_boolean(v_obj == SVAL_EOF_OBJECT);
}

sval_t
g_get_output_bytevector(sval_t v_port)
{
	struct sobj_port *port = sval_to_ptr(v_port);
	//return port->v_buff;
	sval_t v_bytev = make_bytevector(port->write_offset, &v_port, 1);
	bytevector_copy_b(v_bytev, 0, port->v_buff, 0, port->write_offset);
	return v_bytev;
}

sval_t
g_get_output_string(sval_t v)
{
	struct sobj_port *port = sval_to_ptr(v);

	return make_string(port->write_offset - port->read_offset,
			   bytevector_bytes(port->v_buff));
}

sval_t
g_input_port_open_p(sval_t v)
{
	struct sobj_port *port;

	if (g_input_port_p(v) == SVAL_FALSE)
		return SVAL_FALSE;
	port = sval_to_ptr(v);
	return sval_boolean(port->is_input_open);
}

sval_t
g_input_port_p(sval_t v)
{
	struct sobj_port *port;

	if (g_port_p(v) == SVAL_FALSE)
		return SVAL_FALSE;
	port = sval_to_ptr(v);
	return sval_boolean(port->is_input);
}

sval_t
g_output_port_open_p(sval_t v)
{
	struct sobj_port *port;

	if (g_output_port_p(v) == SVAL_FALSE)
		return SVAL_FALSE;
	port = sval_to_ptr(v);
	return sval_boolean(port->is_output_open);
}

sval_t
g_output_port_p(sval_t v)
{
	struct sobj_port *port;

	if (g_port_p(v) == SVAL_FALSE)
		return SVAL_FALSE;
	port = sval_to_ptr(v);
	return sval_boolean(port->is_output);
}

sval_t
g_open_input_bytevector(sval_t v_bytevec)
{
	struct sobj_port *port;
	sval_t vals[2];

	vals[0] = v_bytevec;
	vals[1] = 0;
	port = make_file_object(vals, 2, false);
	port->is_input = 1;
	port->is_input_open = 1;
	port->v_buff = v_bytevec;
	port->write_offset = bytevector_length(v_bytevec);

	return (sval_t)port | SVAL_TAG_LARGE;
}

sval_t
g_open_input_string(sval_t v_str)
{
	struct sobj_port *port;
	sval_t slen, vals[2];
	const uint8_t *strp;
	uint8_t *bvp;

	vals[0] = v_str;
	vals[1] = 0;
	port = make_file_object(vals, 2, true);
	port->is_input = 1;
	port->is_input_open = 1;
	port->is_string = 1;

	sval_string_ref(v_str, &strp, &slen);
	bvp = bytevector_bytes(port->v_buff);
	memcpy(bvp, strp, slen);  // XXX: len(b_buff) >= slen ???
	port->write_offset = slen;

	return (sval_t)port | SVAL_TAG_LARGE;
}

sval_t
g_open_input_fd(sval_t v_num)
{
	int fd = cval_fixnum(v_num);
	struct sobj_port *port = make_file_object(NULL, 0, true);
	port->is_input = 1;
	port->is_input_open = 1;
	port->dont_close_fd = 1;
	port->fd = fd;
	return (sval_t)port | SVAL_TAG_LARGE;
}

sval_t
g_open_input_file(sval_t v_str)
{
	struct sobj_port *port;
	sval_t len;
	const uint8_t *cp;
	char buf[100];

	port = make_file_object(NULL, 0, true);
	port->is_input = 1;
	port->is_input_open = 1;

	sval_string_ref(v_str, &cp, &len);
	memcpy(buf, cp, len);
	buf[len] = '\0';  // XXX

	port->fd = open(buf, O_RDONLY);
	if (port->fd == -1) {
		// XXX: signal an error
		return SVAL_NIL;
	}

	return (sval_t)port | SVAL_TAG_LARGE;
}

sval_t
g_open_output_fd(sval_t v_num)
{
	struct sobj_port *port;
	sval_t v_buff;

	int fd = cval_fixnum(v_num);
	v_buff = make_bytevector(8192, NULL, 0);

	port = mem_alloc(sizeof(struct sobj_port), &v_buff, 1);
	port->h.word = sizeof(struct sobj_port) | SOBJ_HEADER_LARGE;
	port->l = &lh_port;
	port->v_buff = v_buff;
	port->write_offset = port->read_offset = 0;
	port->fd = fd;
	port->is_output      = 1;
	port->is_output_open = 1;
	port->dont_close_fd  = 1;
	port->is_input = port->is_input_open = port->is_string = 0;
	return (sval_t)port | SVAL_TAG_LARGE;
}

sval_t
g_open_output_file(sval_t v_str)
{
	struct sobj_port *port;
	sval_t len, v_buff;
	uint8_t buf[100];
	const uint8_t *cp;

	v_buff = make_bytevector(8192, &v_str, 1);

	port = mem_alloc(sizeof(struct sobj_port), &v_buff, 1);
	port->h.word = sizeof(struct sobj_port) | SOBJ_HEADER_LARGE;
	port->l = &lh_port;
	port->v_buff = v_buff;
	port->write_offset = port->read_offset = 0;
	port->fd = -1;
	port->is_output      = 1;
	port->is_output_open = 1;
	port->is_input = port->is_input_open = port->is_string = 0;

	sval_string_ref(v_str, &cp, &len);
	memcpy(buf, cp, len);
	buf[len] = '\0';  // XXX

	port->fd = open((const char *)buf, O_WRONLY|O_CREAT|O_TRUNC, 0666);
	if (port->fd == -1) {
		// XXX: signal an error
		return SVAL_NIL;
	}

	return (sval_t)port | SVAL_TAG_LARGE;
}

sval_t
g_open_output_bytevector(void)
{
	struct sobj_port *port;
	sval_t v_buff;

	v_buff = make_bytevector(8192, NULL, 0);

	port = mem_alloc(sizeof(struct sobj_port), &v_buff, 1);
	port->h.word = sizeof(struct sobj_port) | SOBJ_HEADER_LARGE;
	port->l = &lh_port;
	port->v_buff = v_buff;
	port->write_offset = port->read_offset = 0;
	port->fd = -1;
	port->is_output      = 1;
	port->is_output_open = 1;
	port->is_input = port->is_input_open = 0;
	return (sval_t)port | SVAL_TAG_LARGE;
}

sval_t
g_open_output_string(void)
{
	struct sobj_port *port;
	sval_t v_buff;

	v_buff = make_bytevector(8192, NULL, 0);

	port = mem_alloc(sizeof(struct sobj_port), &v_buff, 1);
	port->h.word = sizeof(struct sobj_port) | SOBJ_HEADER_LARGE;
	port->l = &lh_port;
	port->v_buff = v_buff;
	port->write_offset = port->read_offset = 0;
	port->fd = -1;
	port->is_output      = 1;
	port->is_output_open = 1;
	port->is_string      = 1;
	port->is_input = port->is_input_open = 0;
	return (sval_t)port | SVAL_TAG_LARGE;
}

sval_t
g_peek_char(sval_t v_port)
{
	sval_t v = g_peek_u8(v_port);
	if (fixnum_p(v))
		return sval_char(cval_fixnum(v));
	else
		return v;  // should be an eof-object.
}

sval_t
g_peek_u8(sval_t v_port)
{
	struct sobj_port *port = sval_to_ptr(v_port);
	ssize_t k;
	sval_t len;
	uint8_t *cp;

	sval_bytevector_ref(port->v_buff, &cp, &len);

	if (port->read_offset >= port->write_offset) {
		if (port->fd == -1)
			return SVAL_EOF_OBJECT;
		k = read(port->fd, cp, len);
		if (k == 0)
			return SVAL_EOF_OBJECT;
		else if (k == -1) {
			// XXX: raise an error
		}
		port->read_offset = 0;
		port->write_offset = k;
	}

	return sval_fixnum(cp[port->read_offset]);
}

sval_t
g_port_p(sval_t v)
{
	struct sobj_port *port;

	if (!large_object_p(v))
		return SVAL_FALSE;
	port = sval_to_ptr(v);
	return sval_boolean(port->l == &lh_port);
}

sval_t
g_read_bytevector(sval_t v_k, sval_t v_port)
{
	struct sobj_port *port = sval_to_ptr(v_port);
	ssize_t n;
	sval_t k, bufsize, datalen, retlen, v_ret;
	uint8_t *bufp, *retp;

	k = cval_fixnum(v_k);
	if (k == 0)
		return make_bytevector(0, NULL, 0);

	v_ret = make_bytevector(k, NULL, 0);
	retp = bytevector_bytes(v_ret);
	retlen = 0;

	sval_bytevector_ref(port->v_buff, &bufp, &bufsize);

	for (;;) {
		datalen = port->write_offset - port->read_offset;
		if (k <= retlen + datalen) {  // enough bytes in buffer
			sval_t copylen = k - retlen;
			memcpy(&retp[retlen], &bufp[port->read_offset],
			       copylen);
			port->read_offset += copylen;
			if (port->read_offset == port->write_offset)
				port->read_offset = port->write_offset = 0;
			break;
		}

		if (datalen > 0) {
			memcpy(&retp[retlen], &bufp[port->read_offset],
			       datalen);
			retlen += datalen;
			port->read_offset = port->write_offset = 0;
		}

		if (port->fd == -1)
			n = 0;
		else
			n = read(port->fd, bufp, bufsize);
		if (n == 0) {
			if (retlen == 0)
				return SVAL_EOF_OBJECT;
			else {
				sval_t v_ret2;
				uint8_t *retp2;

				// we need bytevector_shrink?
				v_ret2 = make_bytevector(retlen, &v_ret, 1);
				retp2 = bytevector_bytes(v_ret2);
				memcpy(retp2, retp, retlen);
				return v_ret2;
			}
		} else if (n == -1) {
			raise_cstr("read error");
		}
		port->write_offset += n;
	}
	return v_ret;
}

sval_t
g_read_char(sval_t v_port)
{
	sval_t v = g_read_u8(v_port);
	if (fixnum_p(v))
		return sval_char(cval_fixnum(v));
	else
		return v;  // should be an eof-object.
}

sval_t
g_read_u8(sval_t v_port)
{
	struct sobj_port *port = sval_to_ptr(v_port);
	sval_t v_u8;

	v_u8 = g_peek_u8(v_port);
	port->read_offset++;
	return v_u8;
}

sval_t
g_write(sval_t v)
{
	if (fixnum_p(v))
		printf("%d", fixnum(v));
	else if (box_p(v))
		printf("#<box:%p>", sval_to_ptr(v));
	else if (bytevector_p(v))
		bytevector_write(v);
	else if (char_p(v))
		char_write(v);
	else if (null_p(v))
		null_write(v);
	else if (pair_p(v))
		pair_write(v);
	else if (proc_p(v)) {
		printf("#<proc:%p>", sval_to_ptr(v));
	} else if (string_p(v))
		string_write(v);
	else if (symbol_p(v))
		symbol_write(v);
	else if (vector_p(v))
		vector_write(v);
	else if (v == SVAL_TRUE)
		printf("#t");
	else if (v == SVAL_FALSE)
		printf("#f");
	else if (v == SVAL_UNSPECIFIED)
		unspecified_write(v);
	else if (v == SVAL_EOF_OBJECT)
		printf("#eof");
	else
		printf("???");
	return SVAL_FALSE;
}

sval_t
g_write_bytevector_4(sval_t v_bv, sval_t v_port, sval_t v_start, sval_t v_end)
{
	struct sobj_port *port = sval_to_ptr(v_port);
	sval_t buflen, datalen, dataoffs;
	uint8_t *bufp, *datap;

	datap = bytevector_bytes(v_bv);
	sval_bytevector_ref(port->v_buff, &bufp, &buflen);
	dataoffs = cval_fixnum(v_start);
	datalen = cval_fixnum(v_end) - cval_fixnum(v_start);
	while (port->write_offset + datalen >= buflen) {
		sval_t cplen = buflen - port->write_offset;
		memcpy(bufp + port->write_offset, datap + dataoffs, cplen);
		port->write_offset += cplen;
		dataoffs += cplen;
		datalen -= cplen;
		flush_write_buffer(port);
		sval_bytevector_ref(port->v_buff, &bufp, &buflen);
	}
	if (datalen > 0) {
		memcpy(bufp + port->write_offset, datap + dataoffs, datalen);
		port->write_offset += datalen;
		if (port->write_offset == buflen)
			flush_write_buffer(port);
	}
	return SVAL_UNSPECIFIED;
}

sval_t
g_write_u8(sval_t v_byte, sval_t v_port)
{
	struct sobj_port *port = sval_to_ptr(v_port);
	uint8_t u8;
	uint8_t *cp;

	u8 = cval_fixnum(v_byte);
	if (port->write_offset >= bytevector_length(port->v_buff))
		flush_write_buffer(port);
	cp = bytevector_bytes(port->v_buff);
	cp[port->write_offset++] = u8;
	return SVAL_UNSPECIFIED;
}

static struct sobj_port *
make_file_object(sval_t *vals, size_t num, bool alloc_buffer)
{
	struct sobj_port *port;
	sval_t v_buff;

	if (alloc_buffer) {
		v_buff = make_bytevector(8192, vals, num - 1);
		if (vals == NULL)
			port = mem_alloc(sizeof(struct sobj_port), &v_buff, 1);
		else {
			vals[num - 1] = v_buff;
			port = mem_alloc(sizeof(struct sobj_port), vals, num);
		}
	} else {
		if (vals == NULL)
			port = mem_alloc(sizeof(struct sobj_port), NULL, 0);
		else
			port = mem_alloc(sizeof(struct sobj_port), vals, num);
	}

	memset(port, 0, sizeof(*port));
	port->h.word = sizeof(struct sobj_port) | SOBJ_HEADER_LARGE;
	port->l = &lh_port;
	port->v_buff = (alloc_buffer) ? v_buff : SVAL_NIL;
	port->fd = -1;
	return port;
}
