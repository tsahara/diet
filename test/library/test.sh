#!/bin/sh -e

cd `dirname $0`/../..
d=test/library

err() {
	echo Failed: "$@"
	exit 1
}

SCHEME="gosh scheme.scm"

echo library name with hyphen
$SCHEME lib $d/define-library-0-l.scm >/dev/null
$SCHEME build $d/define-library-0-p.scm

echo begin in define-library
$SCHEME lib $d/begin-0-l.scm >/dev/null
$SCHEME build $d/begin-0-p.scm
[ `./a.out` = 1 ] || err basic import

echo export in define-library
$SCHEME lib $d/export-0-l.scm >/dev/null
$SCHEME build $d/export-0-p.scm || err build export
./a.out

echo automatic import
$SCHEME build $d/autoimport.scm || err build automatic import
[ `./a.out` = 123 ] || err run automatic import: `./a.out`
exit 0
