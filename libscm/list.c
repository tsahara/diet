#include <stdio.h>
#include <stdlib.h>

#include "scm.h"

static struct sobj_pair *sobj_pair(sval_t);


static struct sobj_pair *
sobj_pair(sval_t v)
{
	return (struct sobj_pair *)(v & ~0xfULL);
}

sval_t
list_length(sval_t v)
{
	sval_t n;

	for (n = 0; pair_p(v); n++)
		v = pair_cdr(v);
	return n;
}

void
null_write(sval_t v)
{
	printf("()");
}

sval_t
pair_equal(sval_t a, sval_t b)
{

	do {
		struct sobj_pair *pa = sobj_pair(a);
		struct sobj_pair *pb = sobj_pair(b);

		if (g_equal(pa->car, pb->car) == SVAL_FALSE)
			return SVAL_FALSE;
		a = pa->cdr;
		b = pb->cdr;
	} while (pair_p(a) && pair_p(b));
	return g_equal(a, b);
}

void
pair_write(sval_t v)
{
	struct sobj_pair *p = sobj_pair(v);

	printf("(");
	g_write(p->car);

	while (pair_p(p->cdr)) {
		p = sobj_pair(p->cdr);
		printf(" ");
		g_write(p->car);
	}

	if (!null_p(p->cdr)) {
		printf(" . ");
		g_write(p->cdr);
	}
	printf(")");
}

sval_t
g_apply(sval_t vproc, sval_t vlist)
{
	extern sval_t ucall(sval_t, sval_t);
	extern sval_t gcall(sval_t, sval_t);

	if (proc_p(vproc))
		return ucall(vproc, vlist);
	else if (gfun_p(vproc))
		return gcall(vproc, vlist);
	else {
		raise_cstr("apply not proc");
		return SVAL_UNSPECIFIED;
	}
}

sval_t
g_car_c(sval_t v)
{
	return pair_car(v);
}

sval_t
g_cdr(sval_t v)
{
	return pair_cdr(v);
}

sval_t
g_cons(sval_t a, sval_t b)
{
	struct sobj_pair *p;
	sval_t objs[2];

	objs[0] = a;
	objs[1] = b;
	p = cell_alloc(objs, 2);
	p->car = a;
	p->cdr = b;
	return (sval_t)p | 0x01;
}

sval_t
g_length(sval_t l)
{
	int n;

	for (n = 0; pair_p(l); n++)
		l = pair_cdr(l);
	return sval_fixnum(n);
}

sval_t
g_list_c(sval_t l)
{
	return l;
}

sval_t
g_list_3f_(sval_t l)
{
	// XXX: loop detection
	while (1) {
		if (null_p(l))
			return SVAL_TRUE;
		if (! pair_p(l))
			return SVAL_FALSE;
		l = pair_cdr(l);
	}
}

sval_t
g_null_3f_(sval_t v)
{
	return null_p(v) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_pair_3f_(sval_t v)
{
	return pair_p(v) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_set_2d_car_21_(sval_t p, sval_t v)
{
	sobj_pair(p)->car = v;
	return v;
}

sval_t
g_set_2d_cdr_21_(sval_t p, sval_t v)
{
	sobj_pair(p)->cdr = v;
	return v;
}
