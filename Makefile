all: libscm/libscm.a build3

build3::
	gosh scheme.scm build3

libscm/libscm.a::
	(cd libscm && make)

test: all
	gosh scheme.scm run -t test.scm
libtest: all
	./test/library/test.sh
citest: test libtest

clean:
	-rm -rf build
	(cd libscm && make clean)
