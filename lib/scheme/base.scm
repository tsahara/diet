(define-library (scheme base)
  (export caar cadr cdar cddr
          call/cc dynamic-wind make-parameter parameterize
          boolean? boolean=?
          bytevector bytevector-append bytevector-copy
          bytevector-copy! bytevector-length
          bytevector-u8-ref bytevector-u8-set! bytevector?
          make-bytevector
          char? char->integer integer->char
          raise
          * + < <= = > >= abs complex? exact? exact-integer? finite? inexact?
          infinite? integer? nan? number? real? rational? zero? positive?
          negative? odd? even? max min modulo quotient remainder
          eq? equal? eqv? exit-test sys-abort
          assq assv assoc
          make-list append apply car cdr cons
          for-each length list list-copy list-ref
          list-set! list-tail list? map memq memv member
          null? pair? reverse set-car! set-cdr!
          gc
          close-input-port close-output-port close-port
          current-error-port current-input-port current-output-port
          eof-object eof-object?
          flush-output-port get-output-bytevector get-output-string
          input-port-open? input-port?
          newline open-input-bytevector open-input-string
          open-output-bytevector open-output-string
          output-port-open? output-port? peek-char peek-u8
          port? read-bytevector read-bytevector! read-char read-u8
          write-bytevector write-char write-u8
          arity procedure?
          list->string make-string substring
          string=? string->list string-append
          string-copy string-copy! string-fill! string-for-each string-length
          string-ref string-set! string?
          symbol? symbol=? symbol->string
          list->vector make-vector string->vector vector vector? vector-append
          vector-copy vector-copy! vector-fill! vector-for-each vector->list
          vector->string vector-length vector-ref vector-set!
          when unless write-string
          every)
  (begin
    (define (caar l) (car (car l)))
    (define (cadr l) (car (cdr l)))
    (define (cdar l) (cdr (car l)))
    (define (cddr l) (cdr (cdr l)))

    (define *dynamic-wind-stack* '())

    (define (dynamic-wind before thunk after)
      (before)
      (set! *dynamic-wind-stack* (cons (cons before after)
                                       *dynamic-wind-stack*))
      (let ((rv (thunk)))
        (if (null? *dynamic-wind-stack*)
            (raise "*dynamic-wind-stack* is empty!"))
        (set! *dynamic-wind-stack* (cdr *dynamic-wind-stack*))
        (after)
        rv))

    (define (call/cc f)
      (define (cut-common-head alist blist)
        (if (and (pair? alist)
                 (pair? blist)
                 (eq? (car alist) (car blist)))
            (cut-common-head (cdr alist) (cdr blist))
            (cons alist blist)))
      (let ((saved-wind *dynamic-wind-stack*))
        (let ((ret (call/cc0 f)))
          (if (eq? *dynamic-wind-stack* saved-wind)
              ret
              (let* ((p (cut-common-head (reverse *dynamic-wind-stack*)
                                         (reverse saved-wind)))
                     (rev-cur-stack  (car p))
                     (rev-cont-stack (cdr p)))
                ;; invoke after's of current-stack
                (let loop ((st (reverse rev-cur-stack)))
                  (if (pair? st)
                      (begin
                        ((cdar st))
                        (loop (cdr st)))))
                ;; invoke before's of wind-stack of the continuation
                (let loop ((st rev-cont-stack))
                  (if (pair? st)
                      (begin
                        ((caar st))
                        (loop (cdr st)))))
                (set! *dynamic-wind-stack* saved-wind)
                ret)))))

    (define-generic (make-parameter init)
      (let ((stack (list init)))
        (case-lambda (()    (car stack))
                     ((val) (set-car! stack val)))))

    (define-generic (make-parameter init converter)
      (let ((stack (list (converter init))))
        (case-lambda (()    (car stack))
                     ((val) (set-car! stack (converter val))))))

    (define-syntax parameterize
      (syntax-rules ()
        ((_ ((param1 value1)) . body)
         (let* ((pobj1  param1)
                (saved1 (pobj1)))
           (dynamic-wind
               (lambda () (pobj1 value1))
               (lambda () . body)
               (lambda () (pobj1 saved1)))))))

    ;; libscm/boolean.c
    (define-cproc (boolean? n) "g_boolean_3f_")
    (define-cproc (boolean=? a b . l) "g_boolean_eq_p")

    ;; libscm/bytevector.c
    (define (bytevector . byte-list)
      (let ((bv (make-bytevector (length byte-list))))
        (let loop ((l byte-list)
                   (i 0))
          (if (pair? l)
              (begin
                (bytevector-u8-set! bv i (car l))
                (loop (cdr l) (+ i 1)))
              bv))))

    (define (bytevector-append . bytevector-list)
      (let* ((len-list (map bytevector-length bytevector-list))
             (new      (make-bytevector (apply + len-list))))
        (let loop ((bl  bytevector-list)
                   (ll  len-list)
                   (idx 0))
          (if (pair? ll)
              (begin
                (bytevector-copy! new idx (car bl) 0 (car ll))
                (loop (cdr bl) (cdr ll) (+ idx (car ll))))
              new))))

    (define-generic (bytevector-copy bytevector)
      (bytevector-copy bytevector 0 (bytevector-length bytevector)))

    (define-generic (bytevector-copy bytevector start)
      (bytevector-copy bytevector start (bytevector-length bytevector)))

    (define-generic (bytevector-copy bytevector start end)
      (let ((new (make-bytevector (- end start))))
        (bytevector-copy! new 0 bytevector start end)
        new))

    (define-generic (bytevector-copy! to at from)
      (bytevector-copy! to at from 0 (bytevector-length from)))

    (define-generic (bytevector-copy! to at from start)
      (bytevector-copy! to at from start (bytevector-length from)))

    (define-generic-cproc (bytevector-copy! to at from start end)
      "g_bytevector_copy_b")

    (define bytevector-length (lambda-cproc (bv) "g_bytevector_2d_length"))
    (define bytevector-u8-ref (lambda-cproc (bv k) "g_bytevector_2d_u8_2d_ref"))
    (define-cproc (bytevector-u8-set! bv k byte) "g_bytevector_u8_set_b")
    (define bytevector? (lambda-cproc (bv) "g_bytevector_3f_"))
    (define-generic-cproc (make-bytevector k) "g_make_bytevector1")
    (define-generic-cproc (make-bytevector k byte) "g_make_bytevector2")

    ;; libscm/char.c
    (define-cproc (char? n) "g_char_3f_")
    (define-cproc (char->integer c) "g_char_integer")
    (define-cproc (integer->char n) "g_integer_char")

    ;; libscm/error.c
    (define-cproc (raise v) "g_raise")

    ;; libscm/fixnum.c
    (define-cproc (* a b) "g__2a_")

    (define-generic-cproc (+ a b) "g_plus")
    (define-generic (+ . num-list)
      (let loop ((sum 0)
                 (l   num-list))
        (if (pair? l)
            (loop (+ sum (car l)) (cdr l))
            sum)))

    (define-cproc (< a b) "g__3c_")
    (define-cproc (<= a b) "g__3c__3d_")
    (define-cproc (= a b) "g__3d_")
    (define-cproc (> a b) "g__3e_")
    (define-cproc (>= a b) "g__3e__3d_")
    (define-cproc (abs n) "g_abs")
    (define-cproc (complex? n) "g_complex_3f_")
    (define-cproc (exact? n) "g_exact_3f_")
    (define-cproc (exact-integer? n) "g_exact_integer")
    (define-cproc (finite? n) "g_finite_3f_")
    (define-cproc (inexact? n) "g_inexact_3f_")
    (define-cproc (infinite? n) "g_infinite_3f_")
    (define-cproc (integer? n) "g_integer_3f_")
    (define-cproc (nan? n) "g_nan_3f_")
    (define-cproc (number? n) "g_number_3f_")
    (define-cproc (real? n) "g_real_3f_")
    (define-cproc (rational? n) "g_rational_3f_")
    (define-cproc (zero? n) "g_zero_3f_")
    (define-cproc (positive? n) "g_positive_3f_")
    (define-cproc (negative? n) "g_negative_3f_")
    (define-cproc (odd? n) "g_odd_3f_")
    (define-cproc (even? n) "g_even_3f_")
    (define-cproc (max . l) "g_max_c")

    (define-generic-cproc (min a b) "g_min")
    (define-generic (min a b . rest)
      (let loop ((n (min a b))
                 (l rest))
        (if (pair? l)
            (loop (min n (car l)) (cdr l))
            n)))

    (define-cproc (modulo a b) "g_modulo")
    (define-cproc (quotient a b) "g_quotient")
    (define-cproc (remainder a b) "g_remainder")

    ;; libscm/init.c
    (define-cproc (eq? a b) "g_eq_p")
    (define-cproc (equal? a b) "g_equal")
    (define-cproc (eqv? a b) "g_eqv")
    (define-cproc (exit-test) "g_exit_2d_test")
    (define-cproc (sys-abort) "g_sys_abort")

    ;; libscm/list.c
    (define (append . arglist)
      (define (duplicate l)
        (let ((head (cons (car l) #f)))
          (let loop ((src  (cdr l))
                     (tail head))
            (if (pair? src)
                (begin
                  (set-cdr! tail (cons (car src) #f))
                  (loop (cdr src) (cdr tail)))
                (cons head tail)))))
      (let skip ()
        (if (and (pair? arglist)
                 (null? (car arglist)))
            (begin
              (set! arglist (cdr arglist))
              (skip))))
      (cond ((null? arglist) '())
            ((null? (cdr arglist)) (car arglist))
            (else
             (let* ((p    (duplicate (car arglist)))
                    (head (car p)))
               (let loop ((tail (cdr p))
                          (args (cdr arglist)))
                 (if (pair? (cdr args))
                     (if (null? (car args))
                         (loop tail (cdr args))
                         (let ((p (duplicate (car args))))
                           (set-cdr! tail (car p))
                           (loop (cdr p) (cdr args))))
                     (begin
                       (set-cdr! tail (car args))
                       head)))))))

    (define (assq obj alist) (assoc obj alist eq?))

    (define (assv obj alist) (assoc obj alist eqv?))

    (define-generic (assoc obj alist) (assoc obj alist equal?))

    (define-generic (assoc obj alist compare)
      (let loop ((l alist))
        (if (pair? l)
            (let ((p (car l)))
              (if (compare (car p) obj)
                  p
                  (loop (cdr l))))
            #f)))

    (define-generic (make-list k) (make-list k #t))
    (define-generic (make-list k fill)
      (let loop ((i k)
                 (l '()))
        (if (> i 0)
            (loop (- i 1) (cons fill l))
            l)))

    (define-generic-cproc (apply proc args) "g_apply")
    (define-generic (apply proc arg1 . args)
      (let ((head (cons arg1 '())))
        (let loop ((tail head)
                   (rest args))
          (if (pair? (cdr rest))
              (let ((p (cons (car rest) '())))
                (set-cdr! tail p)
                (loop p (cdr rest)))
              (begin
                (set-cdr! tail (car rest))
                (apply proc head))))))

    (define-cproc (car v) "g_car_c")
    (define-cproc (cdr v) "g_cdr")
    (define-cproc (cons a b) "g_cons")

    (define (for-each proc list)
      (let loop ((l list))
        (if (pair? l)
            (begin
              (proc (car l))
              (loop (cdr l))))))

    (define-cproc (length l) "g_length")
    (define-cproc (list . l) "g_list_c")
    (define-cproc (list? l) "g_list_3f_")

    (define (list-copy l)
      (if (not (pair? l))
          l
          (let ((head (cons (car l) '())))
            (let loop ((l    (cdr l))
                       (tail head))
              (if (pair? l)
                  (begin
                    (set-cdr! tail (cons (car l) '()))
                    (loop (cdr l) (cdr tail)))
                  (begin
                    (set-cdr! tail l)
                    head))))))

    (define (list-ref l k)
      (car (list-tail l k)))

    (define (list-set! l k obj)
      (set-car! (list-tail l k) obj))

    (define (list-tail l k)
      (if (zero? k)
          l
          (if (pair? l)
              (list-tail (cdr l) (- k 1))
              (raise "list-tail: k < len(l)"))))

    (define (map proc list)
      (if (null? list)
          '()
          (let ((head (cons (proc (car list)) '())))
            (let loop ((l    (cdr list))
                       (tail head))
              (if (pair? l)
                  (let ((p (cons (proc (car l)) '())))
                    (set-cdr! tail p)
                    (loop (cdr l) p))
                  head)))))

    (define (memq obj list) (member obj list eq?))

    (define (memv obj list) (member obj list eqv?))

    (define-generic (member obj list) (member obj list equal?))

    (define-generic (member obj list compare)
      (let loop ((l list))
        (if (pair? l)
            (if (compare (car l) obj)
                l
                (loop (cdr l)))
            #f)))

    (define (reverse l)
      (let loop ((l l)
                 (r '()))
        (if (pair? l)
            (loop (cdr l) (cons (car l) r))
            r)))

    (define-cproc (null? l) "g_null_3f_")
    (define-cproc (pair? l) "g_pair_3f_")
    (define-cproc (set-car! p v) "g_set_2d_car_21_")
    (define-cproc (set-cdr! p v) "g_set_2d_cdr_21_")

    ;; libscm/mem.c
    (define-cproc (gc) "g_gc")

    ;; libscm/port.c
    (define-cproc (open-input-fd fd) "g_open_input_fd")
    (define-cproc (open-output-fd fd) "g_open_output_fd")

    (define *standard-input-port* (open-input-fd 0))
    (define *standard-output-port* (open-output-fd 1))
    (define *standard-error-port* (open-output-fd 2))

    (define-cproc (close-input-port port) "g_close_input_port")
    (define-cproc (close-output-port port) "g_close_output_port")
    (define-cproc (close-port port) "g_close_port")

    (define current-input-port (make-parameter *standard-input-port*))
    (define current-output-port (make-parameter *standard-output-port*))
    (define current-error-port (make-parameter *standard-error-port*))

    (define-cproc (eof-object) "g_eof_object")
    (define-cproc (eof-object? obj) "g_eof_object_p")

    (define-generic (flush-output-port)
      (flush-output-port (current-output-port)))

    (define-generic-cproc (flush-output-port port) "g_flush_output_port_2")

    (define-cproc (get-output-bytevector port) "g_get_output_bytevector")
    (define-cproc (get-output-string port) "g_get_output_string")
    (define-cproc (input-port-open? port) "g_input_port_open_p")
    (define-cproc (input-port? port) "g_input_port_p")

    (define-generic (newline)
      (newline (current-output-port)))

    (define-generic (newline port)
      (write-char #\newline port))

    (define-cproc (open-input-bytevector bytevector) "g_open_input_bytevector")
    (define-cproc (open-input-string str) "g_open_input_string")
    (define-cproc (open-output-bytevector) "g_open_output_bytevector")
    (define-cproc (open-output-string) "g_open_output_string")
    (define-cproc (output-port-open? port) "g_output_port_open_p")
    (define-cproc (output-port? port) "g_output_port_p")

    (define-generic (peek-char) (peek-char (current-input-port)))
    (define-generic-cproc (peek-char port) "g_peek_char")

    (define-generic (peek-u8) (peek-u8 (current-input-port)))
    (define-generic-cproc (peek-u8 port) "g_peek_u8")

    (define-cproc (port? port) "g_port_p")

    (define-generic (read-bytevector k)
      (read-bytevector k (current-input-port)))
    (define-generic-cproc (read-bytevector k port) "g_read_bytevector")

    (define-generic (read-bytevector! bv)
      (read-bytevector! bv (current-input-port)))

    (define-generic (read-bytevector! bv port)
      (read-bytevector! bv port 0 ))

    (define-generic (read-bytevector! bytevector)
      (read-bytevector! bytevector (current-input-port)
                        0 (bytevector-length bytevector)))

    (define-generic (read-bytevector! bytevector port)
      (read-bytevector! bytevector port 0 (bytevector-length bytevector)))

    (define-generic (read-bytevector! bytevector port start)
      (read-bytevector! bytevector port start (bytevector-length bytevector)))

    (define-generic (read-bytevector! bytevector port start end)
      (set! end (min end (bytevector-length bytevector)))
      (let ((newbytes (read-bytevector (- end start) port)))
        (if (eof-object? newbytes)
            newbytes
            (begin
              (bytevector-copy! bytevector start newbytes)
              (bytevector-length newbytes)))))

    (define-generic (read-char) (read-char (current-input-port)))
    (define-generic-cproc (read-char port) "g_read_char")

    (define-generic (read-u8) (read-u8 (current-input-port)))
    (define-generic-cproc (read-u8 port) "g_read_u8")

    (define-generic (write-bytevector bv)
      (write-bytevector bv (current-output-port) 0 (bytevector-length bv)))

    (define-generic (write-bytevector bv port)
      (write-bytevector bv port 0 (bytevector-length bv)))

    (define-generic (write-bytevector bv port start)
      (write-bytevector bv port start (bytevector-length bv)))

    (define-generic-cproc (write-bytevector bv port start end)
      "g_write_bytevector_4")

    (define-generic (write-char char)
      (write-char char (current-output-port)))

    (define-generic (write-char char port)
      (write-u8 (char->integer char) port))

    (define-generic (write-string string)
      (write-string string (current-output-port) 0 (string-length string)))

    (define-generic (write-string string port)
      (write-string string port 0 (string-length string)))

    (define-generic (write-string string port start)
      (write-string string port start (string-length string)))

    (define-generic (write-string string port start end)
      (let loop ((i start))
        (if (< i end)
            (begin
              (write-char (string-ref string i) port)
              (loop (+ i 1))))))

    (define-generic (write-u8 byte) (write-u8 byte (current-output-port)))
    (define-generic-cproc (write-u8 byte port) "g_write_u8")

    ;; libscm/proc.c
    (define-cproc (arity proc) "g_arity")
    (define-cproc (procedure? obj) "g_procedure_p")

    ;; libscm/string.c
    (define (list->string char-list)
      (let* ((len (length char-list))
             (str (make-string len)))
        (let loop ((l   char-list)
                   (idx 0))
          (if (< idx len)
              (begin
                (string-set! str idx (car l))
                (loop (cdr l) (+ idx 1)))
              str))))

    (define-generic-cproc (make-string k) "g_make_string_1")
    (define-generic-cproc (make-string k char) "g_make_string_2")
    (define-cproc (substring string start end) "g_substring")

    (define-generic-cproc (string=? a b) "g_string_3d__3f_")
    (define-generic (string=? a b . rest)
      (and (string=? a b)
           (let loop ((l rest))
             (or (null? l)
                 (and (string=? a (car l))
                      (loop (cdr l)))))))

    (define-generic (string-fill! string fill)
      (string-fill! string fill 0 (string-length string)))

    (define-generic (string-fill! string fill start)
      (string-fill! string fill start (string-length string)))

    (define-generic (string-fill! string fill start end)
      (let loop ((i start))
        (if (< i end)
            (begin
              (string-set! string i fill)
              (loop (+ i 1))))))

    (define (string-for-each proc string1)
      (let ((len (string-length string1)))
        (let loop ((idx 0))
          (if (< idx len)
              (begin
                (proc (string-ref string1 idx))
                (loop (+ idx 1)))))))

    (define-generic (string->list string)
      (string->list string 0 (string-length string)))

    (define-generic (string->list string start)
      (string->list string start (string-length string)))

    (define-generic (string->list string start end)
      (if (< start 0)
          (set! start 0))
      (if (> end (string-length string))
          (set! end (string-length string)))
      (if (>= start end)
          '()
          (let ((head (cons (string-ref string start) '())))
            (let loop ((i    (+ start 1))
                       (tail head))
              (if (= i end)
                  head
                  (begin
                    (set-cdr! tail (cons (string-ref string i) '()))
                    (loop (+ i 1) (cdr tail))))))))

    (define-generic (string->vector string)
      (string->vector string 0 (string-length string)))

    (define-generic (string->vector string start)
      (string->vector string start (string-length string)))

    (define-generic (string->vector string start end)
      (let ((strlen (string-length string)))
        (if (< start 0)
            (set! start 0))
        (if (> end strlen)
            (set! end strlen))
        (let* ((len (- end start))
               (vec (make-vector len)))
          (let loop ((idx 0))
            (if (< idx len)
                (begin
                  (vector-set! vec idx (string-ref string (+ start idx)))
                  (loop (+ idx 1)))
                vec)))))

    (define-cproc (string-append . strlist) "g_string_2d_append_c")

    (define-generic (string-copy string)
      (string-copy string 0 (string-length string)))

    (define-generic (string-copy string start)
      (string-copy string start (string-length string)))

    (define-generic-cproc (string-copy string start end) "g_string_copy3")

    (define-generic (string-copy! to at from)
      (string-copy! to at from 0 (string-length from)))

    (define-generic (string-copy! to at from start)
      (string-copy! to at from start (string-length from)))

    (define-generic-cproc (string-copy! to at from start end)
      "g_string_copy_b")

    (define-cproc (string-length s) "g_string_2d_length")
    (define-cproc (string-ref s k) "g_string_2d_ref")
    (define-cproc (string-set! string k char) "g_string_set_b")
    (define-cproc (string? s) "g_string_3f_")

    ;; libscm/symbol.c
    (define-cproc (symbol? n) "g_symbol_3f_")
    (define-cproc (symbol=? a b) "g_symbol_eq")
    (define-cproc (symbol->string sym) "g_symbol_to_string")

    ;; libscm/vector.c
    (define (list->vector list)
      (let ((vec (make-vector (length list))))
        (let loop ((l list)
                   (i 0))
          (if (pair? l)
              (begin
                (vector-set! vec i (car l))
                (loop (cdr l) (+ i 1)))
              vec))))

    (define-generic-cproc (make-vector k) "g_make_vector_1")
    (define-generic-cproc (make-vector k fill) "g_make_vector_2")

    (define (vector . obj-list) (list->vector obj-list))

    (define-cproc (vector? a) "g_vector_3f_")

    (define (vector-append . vector-list)
      (let* ((len-list (map vector-length vector-list))
             (new      (make-vector (apply + len-list))))
        (let loop ((vl  vector-list)
                   (ll  len-list)
                   (idx 0))
          (if (pair? ll)
              (begin
                (vector-copy! new idx (car vl) 0 (car ll))
                (loop (cdr vl) (cdr ll) (+ idx (car ll))))
              new))))

    (define-generic (vector-copy vector)
      (vector-copy vector 0 (vector-length vector)))

    (define-generic (vector-copy vector start)
      (vector-copy vector start (vector-length vector)))

    (define-generic (vector-copy vector start end)
      (let ((new (make-vector (- end start))))
        (vector-copy! new 0 vector start end)
        new))

    (define-generic (vector-copy! to at from)
      (vector-copy! to at from 0 (vector-length from)))

    (define-generic (vector-copy! to at from start)
      (vector-copy! to at from start (vector-length from)))

    (define-generic (vector-copy! to at from start end)
      (if (< (- (vector-length to) at) (- end start))
          (raise "vector-copy!: too short from"))
      (let loop ((i start)
                 (j at))
        (if (< i end)
            (begin
              (vector-set! to j (vector-ref from i))
              (loop (+ i 1) (+ j 1))))))

    (define-generic (vector-fill! vector fill)
      (vector-fill! vector fill 0 (vector-length vector)))

    (define-generic (vector-fill! vector fill start)
      (vector-fill! vector fill start (vector-length vector)))

    (define-generic (vector-fill! vector fill start end)
      (let loop ((idx start))
        (if (< idx end)
            (begin
              (vector-set! vector idx fill)
              (loop (+ idx 1))))))

    (define (vector-for-each proc vector1)
      (let ((len (vector-length vector1)))
        (let loop ((idx 0))
          (if (< idx len)
              (begin
                (proc (vector-ref vector1 idx))
                (loop (+ idx 1)))))))

    (define-generic (vector->list vector)
      (vector->list vector 0 (vector-length vector)))

    (define-generic (vector->list vector start)
      (vector->list vector start (vector-length vector)))

    (define-generic (vector->list vector start end)
      (let loop ((i    (- end 1))
                 (last '()))
        (if (>= i start)
            (loop (- i 1) (cons (vector-ref vector i) last))
            last)))

    (define-generic (vector->string vector)
      (vector->string vector 0 (vector-length vector)))

    (define-generic (vector->string vector start)
      (vector->string vector start (vector-length vector)))

    (define-generic (vector->string vector start end)
      (let* ((len (- end start))
             (str (make-string len)))
        (let loop ((idx  0))
          (if (< idx len)
              (begin
                (string-set! str idx (vector-ref vector (+ start idx)))
                (loop (+ idx 1)))
              str))))

    (define-cproc (vector-length vec) "g_vector_2d_length")
    (define-cproc (vector-ref vec i) "g_vector_ref")
    (define-cproc (vector-set! vec i obj) "g_vector_set_b")

    ;; (scheme list)
    (define (every proc clist)
      (if (null? clist)
          #t
          (let loop ((l clist))
            (if (null? (cdr l))
                (proc (car l))
                (and (proc (car l))
                     (loop (cdr l)))))))

    (define-syntax when
      (syntax-rules ()
        ((_ condition . exp-list)
         (if condition
             (begin . exp-list)))))

    (define-syntax unless
      (syntax-rules ()
        ((_ condition . exp-list)
         (if (not condition)
             (begin . exp-list)))))
    ))
