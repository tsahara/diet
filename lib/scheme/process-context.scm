(define-library (scheme process-context)
  (export exit)
  (begin
    (define-generic-cproc (exit) "c_exit")
    (define-generic-cproc (exit n) "c_exit_n")
    ))
