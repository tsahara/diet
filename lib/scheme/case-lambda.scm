(define-library (scheme case-lambda)
  (export case-lambda)
  (begin
    ;; Note: "case-lambda" is implemented in the compiler.
    (define case-lambda #f)
    ))
