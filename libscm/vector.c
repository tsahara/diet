#include <stdio.h>
#include <stdlib.h>

#include "scm.h"

struct sobj_vector {
	struct sobj_header h;
	sval_t member[];
};


sval_t g_make_vector_2(sval_t, sval_t);


static struct sobj_vector *
sobj_vector(sval_t v)
{
        return (struct sobj_vector *)(v - SVAL_TAG_VECTOR);
}

static sval_t
sobj_vector_length(const struct sobj_vector *vec)
{
	return (sobj_h_len(&vec->h) - sizeof(vec->h)) / sizeof(sval_t);
}

sval_t
vector_equal(sval_t a, sval_t b)
{
	struct sobj_vector *va = sval_to_ptr(a);
	struct sobj_vector *vb = sval_to_ptr(b);
	sval_t i, len;

	len = sobj_vector_length(va);
	if (len != sobj_vector_length(vb))
		return SVAL_FALSE;
	for (i = 0; i < len; i++) {
		if (g_equal(va->member[i], vb->member[i]) != SVAL_TRUE)
			return SVAL_FALSE;
	}
	return SVAL_TRUE;
}

void
vector_gc_mark(sval_t v)
{
	struct sobj_vector *vec = sval_to_ptr(v);
	sval_t i, n;

	if (sobj_h_mark(&vec->h))
		return;
	sobj_h_set_mark(&vec->h, 1);

	n = sobj_vector_length(vec);
	for (i = 0; i < n; i++)
		gc_mark(vec->member[i]);
}

void
vector_write(sval_t v)
{
	struct sobj_vector *vec = sobj_vector(v);
	sval_t i, len;

	len = sobj_vector_length(vec);
	printf("#(");
	if (len > 0) {
		g_write(vec->member[0]);
		for (i = 1; i < len; i++) {
			printf(" ");
			g_write(vec->member[i]);
		}
	}
	printf(")");
}

sval_t
g_make_vector_1(sval_t vk)
{
	return g_make_vector_2(vk, SVAL_FALSE);
}

sval_t
g_make_vector_2(sval_t vk, sval_t vfill)
{
	struct sobj_vector *vec;
	size_t sz;
	sval_t i, len;

	len = fixnum(vk);
	sz = sizeof(struct sobj_vector) + len * sizeof(sval_t);
	vec = mem_alloc(sz, &vk, 1);
	vec->h.word = sz;
	for (i = 0; i < len; i++)
		vec->member[i] = vfill;
	return (sval_t)vec + SVAL_TAG_VECTOR;
}

sval_t
g_vector_3f_(sval_t a)
{
        return (vector_p(a)) ? SVAL_TRUE : SVAL_FALSE;
}

sval_t
g_vector1(sval_t a)
{
	struct sobj_vector *vec;
        size_t sz;

	sz = sizeof(struct sobj_vector) + sizeof(sval_t);
	vec = mem_alloc(sz, &a, 1);
	vec->h.word = sz;
	vec->member[0] = a;
	return (sval_t)vec + SVAL_TAG_VECTOR;
}

sval_t
g_vector2(sval_t a, sval_t b)
{
	struct sobj_vector *vec;
	sval_t objs[2];
        size_t sz;

	sz = sizeof(struct sobj_vector) + sizeof(sval_t) * 2;
	objs[0] = a;
	objs[1] = b;
	vec = mem_alloc(sz, objs, 2);
	vec->h.word = sz;
	vec->member[0] = a;
	vec->member[1] = b;
	return (sval_t)vec + SVAL_TAG_VECTOR;
}

sval_t
g_vector_2d_length(sval_t v)
{
	struct sobj_vector *vec = sobj_vector(v);

	return sval_fixnum(sobj_vector_length(vec));
}

sval_t
g_vector_ref(sval_t vv, sval_t vi)
{
	struct sobj_vector *vec;
	int i;

	vec = sobj_vector(vv);
	i = fixnum(vi);
	return vec->member[i];
}

sval_t
g_vector_set_b(sval_t v_vec, sval_t v_idx, sval_t v_obj)
{
	struct sobj_vector *vec;
	int i;

	vec = sobj_vector(v_vec);
	i = cval_fixnum(v_idx);
	if (i >= 0 && i < sobj_vector_length(vec))
		vec->member[i] = v_obj;
	else
		raise_cstr("vector-set!: out of range");
	return SVAL_UNSPECIFIED;
}
