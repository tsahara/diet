void symbol_write(sval_t);

static inline int
symbol_p(sval_t v)
{
	return (v & 0xf) == 0x3;
}
