struct sobj_large_header {
	void (*free_object)(void *);
	void (*mark_object)(void *);
};


void free_large_object(void *);
void large_gc_mark(sval_t);
