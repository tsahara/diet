(define-library (scheme write)
  (import (scheme base))
  (export display write)
  (begin
    ;; libscm/init.c
    (define-cproc (write obj) "g_write")

    (define (display obj)
      (cond ((string? obj) (write-string obj))
            ((symbol? obj) (write-string (symbol->string obj)))
            ((char? obj) (write-char obj))
            (else (write obj))))
    ))
