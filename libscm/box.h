void box_gc_mark(sval_t);
sval_t s_box(sval_t);

static inline int
box_p(sval_t v)
{
	return (v & 0xf) == SVAL_TAG_BOX;
}
