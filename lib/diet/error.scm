(define-library (diet error)
  (export error? make-argument-error raise2 set-exception-handler!)
  (begin
    (define-cproc (error? v) "c_error_p")
    (define-cproc (make-argument-error line) "c_make_argument_error")
    (define-cproc (raise2 e) "c_raise2")
    (define-cproc (set-exception-handler! proc) "c_set_exception_handler")

    (define-cproc (error-abort) "c_error_abort")
    (set-exception-handler! error-abort)
  ))
