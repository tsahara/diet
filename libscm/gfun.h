void gfun_gc_mark(sval_t);

static inline int
gfun_p(sval_t v)
{
	return (v & 0xf) == SVAL_TAG_GPROC;
}
