(use file.util)
(use gauche.sequence)
(use gauche.uvector)
(use scheme.hash-table)
(use scheme.list)
(use scheme.list-queue)
(use scheme.set)
(use scheme.vector)
(use srfi-1)
(use srfi-13)
(use srfi-151)
(use util.toposort)


(define *testcode* #f)
(define *verbose* #f)

;; utility procedures

(define (for-each* f il)
  (cond ((pair? il)
         (f (car il))
         (for-each* f (cdr il)))
        ((not (null? il))
         (f il))))

(define (length* larg)
  (let loop ((l larg)
             (i 0))
    (if (pair? l)
        (loop (cdr l) (+ i 1))
        i)))

(define-class <position> ()
  ((filename :init-value #f :getter filename)
   (index    :init-value 0  :getter index)
   (line     :init-value 1  :getter line)
   (column   :init-value 1  :getter column)))

(define-method write-object ((self <position>) port)
  (format port "~a@~a[~a:~a]"
          (slot-ref self 'filename)
          (slot-ref self 'index)
          (slot-ref self 'line)
          (slot-ref self 'column)))

(define-method dup ((self <position>))
  (let1 new (make <position>)
    (slot-set! new 'filename  (slot-ref self 'filename))
    (slot-set! new 'index     (slot-ref self 'index))
    (slot-set! new 'line      (slot-ref self 'line))
    (slot-set! new 'column    (slot-ref self 'column))
    new))

(define-method next-line ((self <position>))
  (let1 new (dup self)
    (inc! (ref new 'index) 1)
    (inc! (ref new 'line))
    (set! (ref new 'column) 1)
    new))

(define-method advance ((self <position>) num)
  (let1 new (dup self)
    (inc! (ref new 'index) num)
    (inc! (ref new 'column) num)
    new))


;; Scheme variable
(define-class <scheme-variable> ()
  ((type   :init-keyword :type :getter type)
   (global :init-value #f :init-keyword :global :getter global?)
   (static :init-value #f :init-keyword :static :getter static?)
   (boxed  :init-value #f :init-keyword :boxed  :getter boxed?)
   (deferred-assignments :init-value '())
   ))

(define-method box! ((self <scheme-variable>))
  (slot-set! self 'boxed #t))

(define-method deferred-assignments ((self <scheme-variable>))
  (slot-ref self 'deferred-assignments))

(define-method clear-deferred-assignments ((self <scheme-variable>))
  (slot-set! self 'deferred-assignments '()))


;; ancestor of all type classes
(define-class <scheme-type> () ())
(define-class <boolean-type> (<scheme-type>) ())
(define-class <bytevector-type> (<scheme-type>) ())
(define-class <character-type> (<scheme-type>) ())
(define-class <number-type> (<scheme-type>) ())
(define-class <pair-type> (<scheme-type>) ())
(define-class <string-type> (<scheme-type>) ())
(define-class <vector-type> (<scheme-type>) ())

(define-class <unknown-type> (<scheme-type>) ())
(define-class <undefined-type> (<scheme-type>) ())

(define-class <procedure-type> (<scheme-type>)
  ((arity  :init-value #f :init-keyword :arity  :getter arity)
   (vararg :init-value #f :init-keyword :vararg :getter vararg?)
   (cfun   :init-value #f :init-keyword :cfun :getter cfun)))

(define-method write-object ((self <procedure-type>) port)
  (format port "(procedure-type :arity ~a :vararg ~a)"
            (slot-ref self 'arity)
            (slot-ref self 'vararg)))


(define-class <generic-function-type> (<procedure-type>)
  ((subprocs :init-value '())))

(define-method add-proc ((self <generic-function-type>) proc)
  (set-cdr! (last-pair (slot-ref self 'subprocs))
            (list proc)))

(define-class <named-let> (<procedure-type>)
  ((used   :init-value #f)
   (called :init-value #f)))


(define-class <annotated-expression> ()
  ((exp :init-value #f :getter aexp->exp :setter aexp-set-exp!)
   (pos :init-value #f :getter aexp->pos)
   (env :init-value #f :getter aexp->env :setter aexp-set-env!)
   (called :init-value #f)  ;; is this the first element in S-exp?
   (type :init-value #f :getter type-of)
   (cont-argspec :init-value #f)

   ;; environment of the first expression of the body (if any)
   (body-env :init-value #f :getter body-env :setter set-body-env!)))

(define-method copy-type! ((self <annotated-expression>) src)
  (slot-set! self 'type (slot-ref src 'type)))

(define-method write-object ((self <annotated-expression>) port)
  (let1 pos (slot-ref self 'pos)
    (format port "~a@~a[~a:~a]"
            (slot-ref self 'exp)
            (slot-ref pos 'index)
            (slot-ref pos 'line)
            (slot-ref pos 'column))))

(define (unannotate exp)
  (if (null? exp)
      '()
      (let1 exp (aexp->exp exp)
        (cond ((pair? exp)   (map* unannotate unannotate exp))
              ((vector? exp) (vector-map unannotate exp))
              (else exp)))))


;; (byte-index line-number column-number)
(define (peg syntax initial str filename)
  ;; (peg-expression pos) -> (value next-pos)
  (define (match exp pos)
    (define (input-available?)
      (< (index pos) (string-length str)))
    (cond ((symbol? exp)
           (let1 rule (assq exp syntax)
             (unless rule
               (errorf "undefined rule: ~a" exp))
             (let1 r (match (second rule) pos)
               (if (and r (> (length rule) 2))
                   (if-let1 r2 ((third rule) (car r) pos)
                     (list r2 (cadr r))
                     #f)
                   r))))

          ((string? exp)
           (let1 len (string-length exp)
             (if (and (input-available?)
                      (string-prefix? exp str 0 len (index pos)))
                 (list exp (advance pos len))
                 #f)))

          ((char? exp)
           (and-let* (( (input-available?))
                      (ch (string-ref str (index pos)))
                      ( (eq? exp ch)))
             (list ch (if (eq? ch #\newline)
                          (next-line pos)
                          (advance pos 1)))))

          ((char-set? exp)
           (if (input-available?)
               (let1 ch (string-ref str (index pos))
                 (if (char-set-contains? exp ch)
                     (list ch (if (eq? ch #\newline)
                                  (next-line pos)
                                  (advance pos 1)))
                     #f))
               #f))

          ((pair? exp)
           (case (car exp)
             ((/)  ;; ordered choice
              (let loop ((el (cdr exp)))
                (if (null? el)
                    #f
                    (or (match (car el) pos)
                        (loop (cdr el))))))

             ((*)  ;; zero or more
              (let loop ((vl '())
                         (pp pos))
                (if-let1 r (match (cdr exp) pp)
                  (loop (append vl (car r)) (cadr r))
                  (list vl pp))))

             ((+)  ;; one or more
              (let loop ((vl '())
                         (pp pos))
                (if-let1 r (match (cdr exp) pp)
                  (loop (append vl (car r)) (cadr r))
                  (and (pair? vl)
                       (list vl pp)))))

             ((?)  ;; zero or one
              (or (match (cdr exp) pos)
                  (list '() pos)))

             ((&)  ;; positive lookahead
              (if (match (cdr exp) pos)
                  (list '() pos)
                  #f))

             ((!)  ;; negative lookahead
              (if (match (cdr exp) pos)
                  #f
                  (list '() pos)))

             (else ;; sequence
              (let loop ((el exp)
                         (rr '())
                         (pp pos))
                (if (pair? el)
                    (if-let1 r (match (car el) pp)
                      (loop (cdr el) (cons (car r) rr) (cadr r))
                      #f)
                    (list (reverse rr) pp))))))
          (else (error "unknown peg syntax:" exp))))
  (let1 result (match initial (let1 pos (make <position>)
                                (slot-set! pos 'filename filename)
                                pos))
    (if result
        (car result)
        #f)))

(define (annotate exp pos :optional (type #f))
  (let1 aexp (make <annotated-expression>)
    (slot-set! aexp 'exp exp)
    (slot-set! aexp 'pos pos)
    (if type
        (slot-set! aexp 'type type))
    aexp))

(define (annotate-r exp pos)
  (if (is-a? exp <annotated-expression>)
      exp
      (let1 aexp (make <annotated-expression>)
        (slot-set! aexp 'exp
                   (cond ((pair? exp)
                          (if (dotted-list? exp)
                              (map* (^e (annotate-r e pos))
                                    (^e (annotate-r e pos))
                                    exp)
                              (map (^e (annotate-r e pos)) exp)))
                         ((vector? exp)
                          (vector-map (^e (annotate-r e pos)) exp))
                         (else exp)))
        (slot-set! aexp 'pos pos)
        aexp)))


(define (read-program filename)
  (define sp (string->uninterned-symbol "sp"))
  (define (remove-sp l)
    (delete sp l eq?))

  ;; The syntax below differs from the syntax defined in R7RS
  ;; for simplicity and performance.
  (define program-syntax
    `(;; 7.1.1. Lexical structure
      (identifier identifier-1
                  ,(lambda (sym pos)
                     (annotate sym pos)))
      ;; not compatible with R7RS
      (identifier-1 (initial (* subsequent))
                    ,(lambda (seq pos) ; -> symbol
                       (let1 s (apply string (cons (car seq) (cadr seq)))
                         (if (peg program-syntax 'a-number s filename)
                             #f
                             (string->symbol s)))))
      (initial #[-+0-9a-zA-Z!$%&*/:<=>?^/_~])  ;; including #[-+0-9]
      (subsequent (/ initial special-subsequent))
      (special-subsequent #[-+.@])
      (hex-digit #[0-9a-f])
      (inline-hex-escape ("\\x" hex-scalar-value ";"))
      (hex-scalar-value (+ hex-digit))
      (mnemonic-escape (#\\ #[abtnr])
                       ,(lambda (seq pos)
                          (case (second seq)
                            ((#\a) #\x0007)
                            ((#\b) #\x0008)
                            ((#\t) #\x0009)
                            ((#\n) #\x000A)
                            ((#\r) #\x000D))))

      (boolean (/ "#t" "#f")
               ,(lambda (str pos)
                  (annotate (string=? str "#t") pos (make <boolean-type>))))
      (character (/ character-name
                    character-hex
                    character-any)
                 ,(lambda (ch pos)
                    (annotate ch pos (make <character-type>))))
      (character-name ("#\\" (/ "alarm"
                                "backspace"
                                "delete"
                                "escape"
                                "newline"
                                "null"
                                "return"
                                "space"
                                "tab"))
                      ,(lambda (seq pos)
                         (cdr
                          (assoc (second seq)
                                 '(("alarm"     . #\x0007)
                                   ("backspace" . #\x0008)
                                   ("delete"    . #\x007f)
                                   ("escape"    . #\x001b)
                                   ("newline"   . #\x000a)
                                   ("null"      . #\x0000)
                                   ("return"    . #\x000d)
                                   ("space"     . #\x0020)
                                   ("tab"       . #\x0009))))))
      (character-hex ("#\\x" hex-scalar-value)
                     ,(lambda (seq pos)
                        (integer->char
                         (string->number
                          (apply string (second seq)) 16))))
      (character-any ("#\\" #[ -~])
                     ,(lambda (seq pos)
                        (second seq)))

      (string (#\" (* string-element) #\")
              ,(lambda (seq pos)
                 (annotate (apply string (second seq))
                           pos
                           (make <string-type>))))
      (string-element (/ mnemonic-escape
                         string-element-escape
                         ;; #\\ intraline-whitespace...
                         inline-hex-escape
                         #[^\"\\]))
      (string-element-escape (/ "\\\"" "\\\\")
                             ,(lambda (str pos)
                                (string-ref str 1)))

      (bytevector ("#u8(" sp (* byte sp) ")")
                  ,(lambda (seq pos)
                     (annotate (apply bytevector (remove-sp (third seq)))
                               pos
                               (make <bytevector-type>))))

      (byte (real-10)
            ,(lambda (str pos)
               (string->number (car str))))

      ;; 7.1.2. External representations
      (datum (/ simple-datum
                compound-datum
                ;; label #\= datum
                ;; label #\#
                ))
      (simple-datum (/ boolean
                       symbol        ;; try symbol first
                       number
                       character
                       string
                       bytevector))
      (symbol identifier)
      (compound-datum (/ list
                         vector
                         abbreviation))
      (list (/ list-proper
               list-dot))
      (list-proper ("(" sp (* datum sp) ")")
                   ,(lambda (seq pos)
                      (annotate (remove-sp (third seq))
                                pos
                                (make <pair-type>))))
      (list-dot ("(" sp (+ datum sp) "." sp datum ")")
                ,(lambda (seq pos)
                   (let1 l (remove-sp (third seq))
                     (set-cdr! (last-pair l) (sixth seq))
                     (annotate l pos (make <pair-type>)))))

      (abbreviation (abbrev-prefix datum)
                    ,(lambda (seq pos)
                       (case (car seq)
                         ((#\')  ; quote
                          (annotate (cons (annotate 'quote pos)
                                          (cdr seq))
                                    pos)))))
      (abbrev-prefix (/ #\'
                        #\`
                        ",@"
                        #\,))
      (vector (#\# #\( sp (* datum sp) #\))
              ,(lambda (seq pos)
                 (annotate (list->vector (remove-sp (fourth seq)))
                           pos
                           (make <vector-type>))))
      (label (#\# uinteger-10))

      (digit #[0-9])

      (number (/ num-10))

      (num-10 real-10
              ,(lambda (str pos)
                 (annotate (string->number str)
                           pos
                           (make <number-type>))))
      (real-10 (sign ureal-10)
               ,(lambda (l _)
                  (apply string (append (car l)
                                        (cadr l)))))
      (ureal-10 uinteger-10)
      (uinteger-10 (+ digit-10))
      (sign (? (/ #\+ #\-)))
      (digit-10 digit)

      ;; 7.1.3. Expressions
      (expression (/ identifier
                     literal
                     procedure-call
                     derived-expression
                     )) ;notyet
      (literal (/ quotation
                  self-evaluating))
      (self-evaluating (/ boolean
                          number
                          vector
                          ;;character
                          ;;string
                          bytevector
                          ))
      (quotation (/ (#\' datum)
                    (#\( "quote" datum  #\))))
      (procedure-call (#\( sp (+ expression sp) #\))
                      ,(lambda (seq pos)
                         (remove-sp (third seq))))
      (sequence ((* command) expression))
      (command expression)
      (derived-expression (/ derived-expression-begin))
      (derived-expression-begin (#\( sp "begin" sp
                                 sequence sp #\)))

      (intraline-whitespace #[ \t])
      (whitespace (/ intraline-whitespace
                     line-ending))
      (line-ending (/ newline (return newline) return))
      (comment (/ comment-eol
                  nested-comment
                  (#\# #\; (* atmosphere) datum)))
      (comment-eol (#\; (* #[^\u000a\u000d]) line-ending))
      (nested-comment (#\# #\| comment-text (* comment-cont) #\| #\#))
      (comment-text (* (/ (#\# (! #\|))
                          (#\| (! #\#))
                          #[^\u0023\u007c])))
      (comment-cont (nested-comment comment-text))
      (newline #\newline)
      (return #\return)

      (atmosphere (/ whitespace
                     comment
                     )) ;notyet


      ;; 7.1.6 Programs and definitions
      (program (+ command-or-definition))
      (command-or-definition (/ command
                                definition
                                command-or-definition-1))
      (command-or-definition-1 (#\( sp "begin" sp
                                (* definition) sp
                                #\)))
      (definition (/ definition-1))
      (definition-1 (#\( sp "define" sp identifier sp expression
                     sp #\)))

      ;; not defined in R7RS
      (program2 (sp (* datum sp))
                ,(lambda (seq pos)
                   (annotate (remove-sp (second seq))
                             pos)))
      (a-number (number (! #[ -~])))
      (sp (* atmosphere) ,(lambda (_ _) sp))))

  (peg program-syntax 'program2 (call-with-input-file filename
                                  port->string)
       filename))


;; C symbols with "_" prefix?
(define *leading-underscore* #f)

(define (csymbol str)
  (if *leading-underscore*
      (string-append "_" str)
      str))

(define (scmsymbol str)
  (string-append (if *leading-underscore* "_s_" "s_") str))


(define *label-counter* 1)

;;
;; Scheme Module = Scheme program or library
;;
(define-class <scheme-module> ()
  ((imported-libraries :init-value '())))

(define-method add-to-import-list ((self <scheme-module>) lib)
  (unless (assoc lib (slot-ref self 'imported-libraries))
    (push! (ref self 'imported-libraries) lib)))


;;
;; Library
;;

(define-class <scheme-library> ()
  ((name :getter get-name)
   (env)
   (prog)
   (exports :init-value '())
   (imported-libraries :init-value '())))

(define-method export ((self <scheme-library>) sym)
  (push! (slot-ref self 'exports) sym))

(define-method exported-symbols ((self <scheme-library>))
  (slot-ref self 'exports))

(define (load-libdef-file libname filename)
  (let ((cxt (make <scheme-to-icode-context>))
        (lib (make <scheme-library>))
        (env (make <scheme-environment>)))
    (slot-set! cxt 'module (make <scheme-module>))
    (slot-set! lib 'name libname)
    (slot-set! lib 'env env)
    (let1 prog (read-program filename)
      (transform-define prog cxt env))
    (update! (ref lib 'imported-libraries)
             (^l (append (slot-ref (slot-ref cxt 'module) 'imported-libraries)
                         l)))
    lib))

(define (make-safe-libname libname)
  (string-join (map (^w (symbol-s2c (x->string w))) libname) "__"))

(define (libname->main-proc l)
  (string-append "s_lib_" (make-safe-libname l)))

(define (libname->pathname l)
  (string-join (map x->string l) "."))

(define (import-library-environment env lib)
  (let1 el (slot-ref lib 'env)
    (for-each (lambda (pair)
                (let ((name (car pair))
                      (type (cdr pair)))
                  (put env name type)))
              (global-variables el))))


(define-class <scheme-to-icode-context> ()
  ((symtab   :init-form (make-hash-table 'eq?))
   (ico      :init-value #f)

   (nextreg  :init-value 0)
   (procs    :init-value '() :getter get-procs)

   (curproc  :init-value #f :getter curproc :setter set-curproc!)
   (disp     :init-value '())  ;; names of the display variables

   (lib      :init-value #f)
   (libname  :init-value #f)
   (module   :init-form #f)
   (library-set-initialized :init-value '())
   ))

(define-method symbol ((self <scheme-to-icode-context>) name)
  (hash-table-intern! (slot-ref self 'symtab) name
                      (^() #t)))

(define-method string ((self <scheme-to-icode-context>) str)
  (new-string (slot-ref self 'ico) str))

(define-method bytevector ((self <scheme-to-icode-context>) bv)
  (new-bytevector (slot-ref self 'ico) bv))

(define-method vector ((self <scheme-to-icode-context>) vec)
  (new-vector (slot-ref self 'ico) vec))

(define-method newreg ((self <scheme-to-icode-context>))
  (let1 r (string->symbol (format #f "r~a" (slot-ref self 'nextreg)))
    (inc! (ref self 'nextreg))
    r))

(define-method defproc ((self <scheme-to-icode-context>) name arity bodyq)
  (push! (ref self 'procs) (list name arity bodyq)))

(define-method copy-procs ((self <scheme-to-icode-context>) c2)
  (for-each (^p (push! (ref self 'procs) p))
            (get-procs c2)))

(define-method inherit ((self <scheme-to-icode-context>) parent)
  (slot-set! self 'symtab (slot-ref parent 'symtab))
  (slot-set! self 'module (slot-ref parent 'module)))

(define-method add-free-variable ((self <scheme-to-icode-context>) name)
  (or (list-index (pa$ eq? name) (slot-ref self 'disp))
      (let1 i (length (slot-ref self 'disp))
        (if (= i 0)
            (slot-set! self 'disp (list name))
            (set-cdr! (last-pair (slot-ref self 'disp))
                      (list name)))
        i)))

(define-method get-imported-lib ((self <scheme-to-icode-context>) libname)
  (find (^l (equal? libname (get-name l)))
        (slot-ref (slot-ref self 'module) 'imported-libraries)))


;;
;; <address-descriptor>
;;
(define-class <address-descriptor> ()
  ((alist :init-value '())))

(define-method get ((self <address-descriptor>) name)
  (if-let1 reg (assoc name (slot-ref self 'alist))
    (cdr reg)
    #f))

(define-method put! ((self <address-descriptor>) name value)
  (slot-set! self 'alist (acons name value (slot-ref self 'alist))))

(define-method clone ((self <address-descriptor>))
  (rlet1 child (make <address-descriptor>)
    (slot-set! child 'alist (slot-ref self 'alist))))

(define-method assigned? ((self <address-descriptor>) reg)
  (if-let1 p (rassoc reg (slot-ref self 'alist))
    (car p)
    #f))

(define-method print ((self <address-descriptor>))
  (format #t "(~a)\n"
          (string-join (map (lambda (p)
                              (format #f "~a:~a" (car p) (cdr p)))
                            (slot-ref self 'alist))
                       " ")
          ))


;;
;; environment in the compiled program
;;
(define-class <scheme-environment> ()
  ((bindings :init-form (make-hash-table 'eq?))
   (parent   :init-keyword :parent :init-value #f)
   ))

(define-method import-initial-bindings ((self <scheme-environment>))
  (put self 'call-with-values
       (make <scheme-variable> :type (make <procedure-type>
                                       :arity 2
                                       :cfun "g_call_with_values"
                                       :vararg #f)))
  (put self 'call/cc0
       (make <scheme-variable> :type (make <procedure-type>
                                       :arity 1
                                       :cfun "g_call_cc0"
                                       :vararg #f)))
  (put self 'make-generic-function
       (make <scheme-variable> :type (make <procedure-type>
                                       :arity 0
                                       :cfun "g_make_generic_function"
                                       :vararg #t)))
  (put self 'generic-function-add!
       (make <scheme-variable> :type (make <procedure-type>
                                       :arity 2
                                       :cfun "g_generic_function_add_b"
                                       :vararg #f)))
  )

(define-method subenv ((self <scheme-environment>))
  (rlet1 env (make <scheme-environment>)
    (slot-set! env 'parent self)))

(define-method get ((self <scheme-environment>) name)
  (let loop ((venv self))
    (if venv
        (or (hash-table-ref/default (slot-ref venv 'bindings) name #f)
            (loop (slot-ref venv 'parent)))
        #f)))

(define-method put ((self <scheme-environment>) name value)
  (unless (symbol? name)
    (errorf "name is not a symbol: (put ~a ~a ~a)~%" self name value))
  (unless (or (is-a? value <scheme-variable>)
              (is-a? value <scheme-syntax-rules>))
    (raise "put an object that is neither variable nor syntax!"))
  (hash-table-set! (slot-ref self 'bindings) name value))

(define-method print ((self <scheme-environment>))
  (let ((chain (let loop ((e self)
                          (l '()))
                 (let1 p (slot-ref e 'parent)
                   (if p
                       (loop p (cons e l))
                       (cons e l))))))
    (for-each-with-index
     (lambda (i e)
       (format #t "~a: ~a\n"
               (- (length chain) i 1)
               (sort (hash-table->alist (slot-ref e 'bindings))
                     (lambda (a b)
                       (string<? (symbol->string (car a))
                                 (symbol->string (car b)))))))
     (reverse chain))))

(define-method global-environment? ((self <scheme-environment>))
  (not (slot-ref self 'parent)))

(define-method global-variables ((self <scheme-environment>))
  (if (slot-ref self 'parent)
      (error "not global enviroment!"))
  (hash-table->alist (slot-ref self 'bindings)))

(define-method free-variable? ((self <scheme-environment>) name)
  (let loop ((venv self))
    (and venv
         (if (hash-table-ref/default (slot-ref venv 'bindings) name #f)
             (and (not (eq? venv self))
                  (not (global-environment? venv)))
             (loop (slot-ref venv 'parent))))))

(define-method bound-variable? ((self <scheme-environment>) name)
  (hash-table-ref/default (slot-ref self 'bindings) name #f))

(define-method find ((self <scheme-environment>) name)
  (let loop ((e self))
    (and e
         (if (hash-table-ref/default (slot-ref e 'bindings) name #f)
             e
             (loop (slot-ref e 'parent))))))

(define-method set-boxed-variable! ((self <scheme-environment>) name)
  (let1 e (find self name)
    (let1 value (hash-table-ref (slot-ref e 'bindings) name)
      (if (is-a? value <scheme-variable>)
          (box! value)
          (begin
            ;;(format #t "~a (not a type) in env!~%" value)
            (hash-table-set! (slot-ref e 'bindings) name
                             (make <scheme-variable>
                               :type (make <unknown-type>)
                               :boxed #t)))))))

(define-method box-all-variables ((self <scheme-environment>))
  (hash-table-for-each (lambda (name val)
                         (unless (or (and (is-a? val <scheme-variable>)
                                          (global? val))
                                     (is-a? val <scheme-syntax-rules>))
                           (if (is-a? val <scheme-variable>)
                               (box! val)
                               (hash-table-set! (slot-ref self 'bindings)
                                                name
                                                (make <scheme-variable>
                                                  :type (make <unknown-type>)
                                                  :boxed #t)))))
                       (slot-ref self 'bindings)))


;;
;; <intermediate-code-object>
;;
(define-class <intermediate-code-object> ()
  ((label-no :init-value 0)
   (data     :init-value '())
   ))

(define (make-intermediate-code-object)
  (make <intermediate-code-object>))

(define-method next-label ((self <intermediate-code-object>))
  (let1 n (slot-ref self 'label-no)
    (inc! (ref self 'label-no))
    n))

(define-method new-string ((self <intermediate-code-object>) str)
  (let1 label (format #f "string~4,'0d" (next-label self))
    (push! (ref self 'data) (list label str))
    label))

(define-method new-bytevector ((self <intermediate-code-object>) bv)
  (let1 label (format #f "bytevector~4,'0d" (next-label self))
    (push! (ref self 'data) (list label bv))
    label))

(define-method new-vector ((self <intermediate-code-object>) vec)
  (let1 label (format #f "vector~4,'0d" (next-label self))
    (push! (ref self 'data) (list label vec))
    label))


(define (symbol-s2c sym)
  (regexp-replace-all #/[^0-9A-Za-z]/
                      (if (symbol? sym)
                          (symbol->string sym)
                          sym)
                      (^m (format "_~2,'0x_"
                                  (char->integer
                                   (string-ref (rxmatch-substring m)
                                               0))))))

(define (symbol-s2i sym)
  (string-append "s_"
                 (regexp-replace-all #/[^0-9A-Za-z]/
                                     (if (or #t symbol? sym)
                                         (symbol->string sym)
                                         sym)
                                     (^m (format "_~2,'0x_"
                                                 (char->integer
                                                  (string-ref
                                                   (rxmatch-substring m)
                                                   0)))))))

(define (cproc-icode-symbol sym)
  (string-append "g_" (symbol-s2c sym)))

;; <formals> -> fixed-args, vararg
(define (parse-formals-aexp aexp)
  (let loop ((exp-list (unannotate aexp))
             (fargs    '()))
    (if (pair? exp-list)
        (loop (cdr exp-list) (cons (car exp-list) fargs))
        (values (reverse fargs) exp-list))))

;; annotated-expression -> (queue-of-insn register)
(define (s2i-compile-exp aexp cxt adesc tail)
  (let ((insn-q (make-list-queue '()))
        (rret   #f)
        (exp    (aexp->exp aexp))
        (newlabel (lambda ()
                    (let1 label (format #f "i_~5,'0d" *label-counter*)
                      (inc! *label-counter*)
                      label))))
    (define (emit . args)
      (if (list-queue? (car args))
          (list-queue-for-each (^l (apply emit l)) (car args))
          (if (pair? (car args))
              (list-queue-add-back! insn-q (car args))
              (list-queue-add-back! insn-q args))))
    (define (eval-args args)
      (let1 regs (make-list-queue'())
        (for-each (lambda (arg)
                    (let1 r (s2i-compile-exp arg cxt adesc #f)
                      (emit (car r))
                      (list-queue-add-back! regs (cadr r))))
                  args)
        (list-queue-list regs)))
    (define (compile-exp-list exp-list)
      (let loop ((exp-list exp-list)
                 (iq       (make-list-queue '())))
        (let* ((last? (null? (cdr exp-list)))
               (r (s2i-compile-exp (car exp-list) cxt adesc
                                   (if last? tail #f))))
          (set! iq (list-queue-append! iq (car r)))
          (if last?
              (cons iq (cdr r))
              (loop (cdr exp-list) iq)))))
    (define (compile-list-generator regs)
      (let1 q (make-list-queue'())
        (define i-cons (cproc-icode-symbol 'cons))
        (define i-set-cdr (cproc-icode-symbol 'set-cdr!))
        (define (igen . args) (list-queue-add-back! q args))
        (if (null? regs)
            (let1 nil (newreg cxt)
              (igen 'i.null nil)
              (list q nil))
            (let ((list-reg (newreg cxt))
                  (pair-reg (newreg cxt))
                  (prev-reg (newreg cxt)))
              (igen 'i.call 'c i-cons list-reg (car regs) (car regs))
              (igen 'i.move prev-reg list-reg)
              (for-each (lambda (reg)
                          (igen 'i.call 'c i-cons pair-reg reg reg)
                          (igen 'i.call 'c i-set-cdr #f prev-reg pair-reg)
                          (igen 'i.move prev-reg pair-reg))
                        (cdr regs))
              (igen 'i.null pair-reg)
              (igen 'i.call 'c i-set-cdr #f prev-reg pair-reg)
              (list q list-reg)))))
    (cond ((boolean? exp)
           (let1 r (newreg cxt)
             (emit (if exp 'i.true 'i.false) r)
             (set! rret r)))

          ((bytevector? exp)
           (let1 r (newreg cxt)
             (emit 'i.bytevector r (bytevector cxt exp))
             (set! rret r)))

          ((char? exp)
           (let1 r (newreg cxt)
             (emit 'i.char r (char->ucs exp))
             (set! rret r)))

          ((integer? exp)
           (let1 r (newreg cxt)
             (emit 'i.fixnum r exp)
             (set! rret r)))

          ((string? exp)
           (let1 r (newreg cxt)
             (emit 'i.string r (string cxt exp))
             (set! rret r)))

          ((symbol? exp)
           (if-let1 r (get adesc exp)
             ;; The symbol is a bound variable.
             (if (pair? r)
                 ;; It's boxed.
                 (let1 r2 (newreg cxt)
                   (emit 'i.unbox r2 (cdr r))
                   (set! rret r2))
                 ;; It is not boxed.
                 (set! rret r))
             (if-let1 v (get (aexp->env aexp) exp)
               (if (is-a? v <scheme-variable>)
                   (if (global? v)
                       ;; The symbol is a global variable.
                       (let1 r (newreg cxt)
                         (emit 'i.load r (symbol-s2i exp))
                         (set! rret r))
                       ;; The symbol is a free variable.
                       (let1 r (newreg cxt)
                         (emit 'i.display r (curproc cxt)
                               (add-free-variable cxt exp))
                         (if (boxed? v)
                             (let1 r2 (newreg cxt)
                               (emit 'i.unbox r2 r)
                               (set! rret r2))
                             (set! rret r))))
                   ;; The symbol is a syntax (it should not be!).
                   (errorf "it's a syntax not a variable: ~a" exp))
               ;; The symbol is not known.
               (errorf "undefined symbol: ~a" exp))))

          ((vector? exp)
           (let ((elements (unannotate aexp))
                 (r (newreg cxt)))
             (emit 'i.vector r (vector cxt elements))
             (set! rret r)))

          ((pair? exp)
           (case (aexp->exp (car exp))
             ((+)
              (let1 r (newreg cxt)
                (if (null? (cdr exp))
                    (emit 'i.fixnum r 0)
                    (let1 args (eval-args (cdr exp))
                      #;(slot-ref (cadr exp) 'type)
                      (if (null? (cdr args))
                          (emit 'i.move r (car args))
                          (begin
                            (emit 'i.add r (car args) (cadr args))
                            (let loop ((args (cddr args)))
                              (when (pair? args)
                                (emit 'i.add r r (car args))
                                (loop (cdr args))))))))
                (set! rret r)))

             ((-)
              (let ((args (eval-args (cdr exp)))
                    (r    (newreg cxt)))
                (if (null? (cddr exp))
                    ;; (- a)
                    (begin
                      (emit 'i.fixnum r 0)
                      (emit 'i.sub r r (car args)))
                    ;; (- a b c)
                    (begin
                      (emit 'i.sub r (car args) (cadr args))
                      (for-each (^n (emit 'i.sub r r n))
                                (cddr args))))
                (set! rret r)))

             ((and)
              (if (null? (cdr exp))
                  (let1 r (newreg cxt)
                    ;; (and) => #t
                    (emit 'i.true r)
                    (set! rret r))
                  (let* ((l   (newlabel))
                         (arg (s2i-compile-exp (cadr exp) cxt adesc #f))
                         (r   (cadr arg)))
                    ;; first argument
                    (emit (car arg))

                    ;; second to last argument
                    (let loop ((args (cddr exp)))
                      (unless (null? args)
                        (let1 arg (s2i-compile-exp (car args) cxt adesc
                                                   (if (null? (cdr args))
                                                       tail
                                                       #f))
                          (emit 'i.jf r l)
                          (emit (car arg))
                          (emit 'i.move r (cadr arg))
                          (loop (cdr args)))))

                    (emit 'i.label l)
                    (set! rret r))))

             ((assert-true)
              (let ((t (s2i-compile-exp (second exp) cxt adesc #f))
                    (r (newreg cxt)))
                (emit (car t))
                (emit 'i.fixnum r (line (aexp->pos aexp)))
                (emit 'i.call 'c (symbol-s2i 'assert0) #f (cadr t) r)))

             ((begin)
              (let1 r (compile-exp-list (cdr exp))
                (emit (car r))
                (set! rret (cdr r))))

             ((cond)
              (let ((lz (newlabel))
                    (r  (newreg cxt))
                    (has-else #f))
                (for-each (lambda (clause)
                            ;; clause = (<test> <exp1> <exp2>...)
                            ;; clause = (<test> => <exp>)
                            (unless (pair? (aexp->exp clause))
                              (error "malformed cond clause"))
                            (let* ((ln (newlabel))
                                   (test-aexp (car (aexp->exp clause)))
                                   (a  (if (not (eq? (aexp->exp test-aexp)
                                                     'else))
                                           (s2i-compile-exp test-aexp
                                                            cxt
                                                            adesc
                                                            #f)
                                           (begin
                                             (set! has-else #t)
                                             #f)))
                                   (b  (if (pair? (cdr (aexp->exp clause)))
                                           (compile-exp-list (cdr (aexp->exp
                                                                   clause)))
                                           #f)))
                              (when (and (not a) (not b))
                                (error "else clause must have an expression"))
                              (when a
                                (emit (car a))
                                (unless b
                                  (emit 'i.move r (cadr a)))
                                (emit 'i.jf (cadr a) ln))
                              (when b
                                (emit (car b))
                                (if-let1 rb (cadr b)
                                  (emit 'i.move r rb)))
                              (when a
                                (emit 'i.jmp lz)
                                (emit 'i.label ln))))
                          (cdr exp))
                (unless has-else
                  (emit 'i.unspecified r))
                (emit 'i.label lz)
                (set! rret r)))

             ;; (define name exp)
             ((define)
              (when (pair? (aexp->exp (second exp)))
                (errorf "internal error: define not translated! ~a" exp))
              (let ((name (aexp->exp (second exp))))
                (when *verbose*
                  (format #t "define: ~a~%" name))
                (let1 a (s2i-compile-exp (third exp) cxt adesc #f)
                  (emit (car a))
                  (if (global-environment? (aexp->env aexp))
                      (emit 'i.store (cadr a) (symbol-s2i name))
                      (put! adesc name (cadr a)))
                  (let1 v (get (aexp->env aexp) name)
                    (for-each (lambda (p)
                                (let ((rproc (car p))
                                      (idx   (cadr p)))
                                  (emit 'i.setdisplay rproc idx (cadr a))))
                              (deferred-assignments v))
                    (clear-deferred-assignments v))
                  )))

             ((define-library)
              (let ((lib          (slot-ref cxt 'lib))
                    (begin-blocks '()))
                (define (process-export-spec spec)
                  (if (pair? spec)
                      (if (eq? 'rename (car spec))
                          (export lib (cons (second spec) (third spec)))
                          (errorf "unknown export spec: ~a" spec))
                      (export lib spec)))
                (define (compile-begin-block aexp)
                  (let1 r (s2i-compile-exp aexp cxt
                                           (make <address-descriptor>) #f)
                    (push! begin-blocks (car r))))
                (for-each (lambda (decl-aexp)
                            (let1 decl (unannotate decl-aexp)
                              (case (car decl)
                                ((begin)
                                 (compile-begin-block decl-aexp))
                                ((export)
                                 (for-each process-export-spec (cdr decl)))
                                ((import)
                                 ;; nothing to do
                                 )
                                (else => (cut errorf
                                              "unsupported library decl: ~a"
                                              <>)))))
                          (cddr exp))
                (for-each emit (reverse begin-blocks))))

             ((define-values)
              (receive (fargs varg)
                  (parse-formals-aexp (second exp))
                (slot-set! (third exp) 'cont-argspec (cons fargs varg))
                (let1 a (s2i-compile-exp (third exp) cxt adesc #f)
                  (emit (car a))
                  (if (< (length (cdr a)) (length fargs))
                      (errorf "exp returns less values than expected: ~a"
                              (third exp)))
                  (unless (null? varg)
                    (let1 l (compile-list-generator (drop (cdr a)
                                                          (length fargs)))
                      (emit (car l))
                      (set! fargs (cons varg fargs))
                      (set! a (cons* #f (cadr l) (cdr a)))))
                  (for-each (lambda (arg reg)
                              (if (global-environment? (aexp->env aexp))
                                  (emit 'i.store reg (symbol-s2i arg))
                                  (put! adesc arg reg))
                              ;; deferred assignment
                              (let1 v (get (aexp->env aexp) arg)
                                (for-each (lambda (p)
                                            (let ((rproc (car p))
                                                  (idx   (cadr p)))
                                              (emit 'i.setdisplay rproc idx
                                                    reg)))
                                          (deferred-assignments v))
                                (clear-deferred-assignments v )))
                            fargs (cdr a)))))

             ((eq?)
              (let ((args (eval-args (cdr exp)))
                    (lt   (newlabel))
                    (lz   (newlabel))
                    (r    (newreg cxt)))
                (emit 'i.jeq (first args) (second args) lt)
                (emit 'i.false r)
                (emit 'i.jmp lz)
                (emit 'i.label lt)
                (emit 'i.true r)
                (emit 'i.label lz)
                (set! rret r)))

             ((if)
              ;; (if a b c)
              (let ((a (s2i-compile-exp (second exp) cxt adesc #f))
                    (b (s2i-compile-exp (third  exp) cxt (clone adesc) tail))
                    (c (if (= (length exp) 4)
                           (s2i-compile-exp (fourth exp) cxt (clone adesc)
                                            tail)
                           #f))
                    (rr #f)
                    (lf (newlabel))
                    (lz (newlabel)))
                (if (and c (not (= (length (cdr b))
                                   (length (cdr c)))))
                    (raise "then and else clauses must return the same numbers of values"))
                (set! rr (map (lambda (x) (newreg cxt)) (cdr b)))

                (emit (car a))
                (emit 'i.jf (cadr a) (if c lf lz))
                (emit (car b))
                (when (cadr b)
                  (for-each (lambda (r ra) (emit 'i.move r ra))
                            rr (cdr b)))
                (if c
                    (begin
                      (emit 'i.jmp lz)
                      (emit 'i.label lf)
                      (emit (car c))
                      (when (cadr c)
                        (for-each (lambda (r rc) (emit 'i.move r rc))
                                  rr (cdr c)))))
                (emit 'i.label lz)
                (if (= (length rr) 1)
                    (set! rret (car rr))
                    (set! rret rr))))

             ((import)
              ;; (import <import-spec> ...)
              (let1 imported-list '()
                (for-each (lambda (spec-aexp)
                            (define (run-lib-main libname)
                              (emit 'i.call 'c (libname->main-proc libname)
                                    #f))
                            (define (import-a-lib lib)
                              (unless (member (get-name lib) imported-list)
                                ;; XXX detect import loop and raise an error
                                (for-each import-a-lib
                                          (slot-ref lib 'imported-libraries))
                                (run-lib-main (get-name lib))
                                (push! imported-list (get-name lib))))
                            (let1 libname (unannotate spec-aexp)
                              (import-a-lib (get-imported-lib cxt libname))))
                          (cdr exp))))

             ((lambda)
              ;; (lambda formals body...)
              (let ((c2     (make <scheme-to-icode-context>))
                    (e2     (aexp->env (third exp)))
                    (insn-q (make-list-queue '()))
                    (name   (newlabel))
                    (a2     (make <address-descriptor>)))
                (define (igen2 . args)
                  (if (list-queue? (car args))
                      (list-queue-for-each (^l (apply igen2 l))
                                           (car args))
                      (list-queue-add-back! insn-q args)))

                (slot-set! c2 'ico (slot-ref cxt 'ico))
                (inherit c2 cxt)

                ;; callee: put current procedure into the first reg.
                (set-curproc! c2 (newreg c2))

                ;; callee: put names in formals to address descriptor.
                (for-each-formal-exps (lambda (name)
                                        (let ((r (newreg c2)))
                                          (if (boxed? (get e2 name))
                                              (begin
                                                (igen2 'i.box r r)
                                                (put! a2 name (cons 'box r)))
                                              (put! a2 name r))))
                                      (second exp))

                ;; callee: compile body
                (if (null? (cddr exp))
                    ;; if body is empty, return value is undefined(?)
                    (igen2 'i.ret)
                    ;; otherwise, eval body and return the last exp.
                    (let loop ((exp-list (cddr exp)))
                      (let ((exp  (car exp-list))
                            (rest (cdr exp-list)))
                        (let1 r (s2i-compile-exp exp c2 a2 (null? rest))
                          (igen2 (car r))
                          (if (pair? rest)
                              (loop rest)
                              (apply igen2 (cons 'i.ret (cdr r))))))))

                (copy-procs cxt c2)

                ;; caller
                (let1 rl (newreg cxt)
                  ;; make a procedure object
                  (receive (arity vararg?)
                      (let loop ((formals (aexp->exp (second exp)))
                                 (count   0))
                        (if (pair? formals)
                            (loop (cdr formals) (+ count 1))
                            (values count (not (null? formals)))))
                    (defproc cxt name (+ arity (if vararg? 1 0)) insn-q)
                    (emit 'i.lambda rl name arity
                          (length (slot-ref c2 'disp))
                          vararg?))

                  ;; set display variables of the procedure object
                  (for-each-with-index
                   (lambda (i name)
                     (define (refvar name)
                       (if-let1 r (get adesc name)
                         (if (pair? r)
                             (cdr r)
                             r)
                         (if-let1 v (get e2 name)
                           (if (bound-variable? (aexp->env aexp) name)
                               v
                               (let1 r (newreg cxt)
                                 (emit 'i.display r (curproc cxt)
                                       (add-free-variable cxt name))
                                 r))
                           (errorf "undefined symbol: ~a" name))))
                     (let* ((r         (refvar name))
                            (deferred? (is-a? r <scheme-variable>)))
                       (if deferred?
                           (push! (ref r 'deferred-assignments) (list rl i))
                           (emit 'i.setdisplay rl i r))
                       (when *verbose*
                         (format #t "set display#~a for ~a~a~%"
                                 i name
                                 (if deferred? " (deferred)" "")))))
                   (slot-ref c2 'disp))
                  (set! rret rl))))

             ((lambda-cproc)
              ;; (lambda formals cproc-name)
              (let1 rl (newreg cxt)
                (receive (a v)
                    (parse-arity (aexp->exp (second exp)))
                  ;; generate a bridge procedure
                  (let ((name  (newlabel))
                        (nargs (+ a (if v 1 0))))
                    (defproc cxt name nargs
                      (make-list-queue
                       (list
                        (cons* 'i.tailcall 'c (aexp->exp (third exp))
                               (map (lambda (i)
                                      (string->symbol
                                       (string-append "r" (number->string i))))
                                    (iota nargs 1))))))
                    (emit 'i.lambda rl name a 0 v)))
                (set! rret rl)))

             ((let let* letrec letrec* let-values let*-values)
              ;; (let <bindings> <body>)
              ;; (let <name> <bindings> <body>)
              (let* ((a2       (clone adesc))
                     (keyword  (aexp->exp (car exp)))
                     (seq?     (or (eq? keyword 'let*)
                                   (eq? keyword 'letrec*)
                                   (eq? keyword 'let*-values)))
                     (rec?     (or (eq? keyword 'letrec)
                                   (eq? keyword 'letrec*)))
                     (mv?      (or (eq? keyword 'let-values)
                                   (eq? keyword 'let*-values)))
                     (name     (let1 2nd (aexp->exp (cadr exp))
                                 (if (not (list? 2nd))
                                     2nd
                                     #f)))
                     (bindings (aexp->exp (if name
                                              (caddr exp)
                                              (cadr exp))))
                     (body     (if name
                                   (cdddr exp)
                                   (cddr exp)))
                     (bodyenv  (body-env aexp))
                     (args     '())
                     (llet     (newlabel)))
                ;; make bindings
                (for-each (lambda (binding)
                            (let* ((var    (aexp->exp (car binding)))
                                   (rvalue (newreg cxt))
                                   (rbox   (newreg cxt)))
                              (when (and (not mv?)
                                         (boxed? (get bodyenv var)))
                                (emit 'i.false rvalue)
                                (emit 'i.box rbox rvalue)
                                (put! a2 var (cons 'box rbox)))))
                          (map aexp->exp bindings))
                (for-each (lambda (binding)
                            (receive (fargs varg)
                                (parse-formals-aexp (car binding))
                              (when mv?
                                (slot-set! (cadr binding) 'cont-argspec
                                           (cons fargs varg)))
                              (let1 e (s2i-compile-exp (cadr binding)
                                                       cxt
                                                       (if (or seq? rec?)
                                                           a2
                                                           adesc)
                                                       #f)
                                (emit (car e))
                                (cond (mv?
                                       (for-each (lambda (arg reg)
                                                   (put! a2 arg reg))
                                                 fargs (cdr e))
                                       (unless (null? varg)
                                         (let1 l (compile-list-generator
                                                  (drop (cdr e)
                                                        (length fargs)))
                                           (emit (car l))
                                           (put! a2 varg (cadr l)))))
                                      (else
                                       (let1 r (cadr e)
                                         (if (and-let* ((eb (body-env aexp))
                                                        (v  (get eb varg)))
                                               (boxed? v))
                                             (let1 rbox (get a2 varg)
                                               (emit 'i.set-box! (cdr rbox)
                                                     (cadr e)))
                                             (begin
                                               (when (assigned? a2 r)
                                                 (set! r (newreg cxt))
                                                 (emit 'i.move r (cadr e)))
                                               (put! a2 varg r)
                                               (push! args r)))))))))
                          (map aexp->exp bindings))
                (when name
                  (emit 'i.label llet)
                  (put! a2 name (cons* 'named-let llet (reverse args))))

                ;; evaluate expressions in body
                (let loop ((body body))
                  (let1 e (s2i-compile-exp (car body) cxt a2
                                           (if (null? (cdr body))
                                               tail
                                               #f))
                    (emit (car e))
                    (if (pair? (cdr body))
                        (loop (cdr body))
                        (set! rret (cadr e)))))))

             ((library-file)  ;; compiler directive
              (slot-set! cxt 'libname (aexp->exp (second exp))))

             ((not)
              (let* ((arg (s2i-compile-exp (second exp) cxt adesc #f))
                     (i   (car arg))
                     (r   (cadr arg))
                     (lf  (newlabel))
                     (lz  (newlabel)))
                (emit i)
                (emit 'i.jf r lf)
                (emit 'i.false r)
                (emit 'i.jmp lz)
                (emit 'i.label lf)
                (emit 'i.true r)
                (emit 'i.label lz)
                (set! rret r)))

             ((or)
              (let ((r  (newreg cxt))
                    (lz (newlabel)))
                (let loop ((args (cdr exp)))
                  (let1 arg (s2i-compile-exp (car args) cxt adesc
                                             (if (null? (cdr args))
                                                 tail
                                                 #f))
                    (emit (car arg))
                    (emit 'i.move r (cadr arg))
                    (when (pair? (cdr args))
                      (emit 'i.jt r lz)
                      (loop (cdr args)))))
                (emit 'i.label lz)
                (set! rret r)))

             ((quote)
              (let ((sexp (aexp->exp (second exp)))
                    (r    (newreg cxt))
                    (pos  (aexp->pos aexp)))
                (cond ((boolean? sexp)
                       (emit (if sexp 'i.true 'i.false) r))
                      ((symbol? sexp)
                       (begin
                         (symbol cxt sexp)
                         (emit 'i.symbol r sexp)))
                      ((number? sexp)
                       (emit 'i.fixnum r sexp))
                      ((pair? sexp)
                       (let ((a (s2i-compile-exp (annotate
                                                  (list
                                                   (annotate 'quote pos)
                                                   (car sexp))
                                                  pos)
                                                 cxt adesc #f))
                             (b (s2i-compile-exp (annotate
                                                  (list
                                                   (annotate 'quote pos)
                                                   (let1 t (cdr sexp)
                                                     (if (or (null? t)
                                                             (pair? t))
                                                         (annotate t pos)
                                                         t)))
                                                  pos)
                                                 cxt adesc #f)))
                         (emit (car a))
                         (emit (car b))
                         ;; could be tailcall?
                         (emit 'i.call 'c (cproc-icode-symbol 'cons)
                               r (cadr a) (cadr b))
                         ))
                      ((null? sexp)
                       (emit 'i.null r))
                      ((or (char? sexp)
                           (string? sexp)
                           (vector? sexp)
                           (bytevector? sexp))
                       (let1 e (s2i-compile-exp (second exp) cxt adesc tail)
                         (emit (car e))
                         (set! r (cadr e))))
                      (else
                       (errorf "cannot quote: ~a" sexp)))
                (set! rret r)))

             ((set!)
              (let ((name (aexp->exp (second exp)))
                    (a    (s2i-compile-exp (third exp) cxt adesc #f)))
                (emit (car a))
                (set! rret (cadr a))

                (if-let1 v (get (aexp->env aexp) name)
                  (if (global? v)
                      (emit 'i.store (cadr a) (symbol-s2i name))
                      (if-let1 r (get adesc name)
                        (if (pair? r)
                            (emit 'i.set-box! (cdr r) (cadr a))
                            (emit 'i.move r (cadr a)))
                        (let1 rd (newreg cxt)
                          (emit 'i.display rd (curproc cxt)
                                (add-free-variable cxt name))
                          (unless (boxed? v)
                            (errorf "set free variable but not boxed: ~a ~a"
                                    name aexp))
                          (emit 'i.set-box! rd (cadr a)))))
                  (errorf "set unknown variable???: ~a" name))))

             ((values)
              (let1 args (map (^e (s2i-compile-exp e cxt adesc #f))
                              (cdr exp))
                (for-each emit (map car args))
                (set! rret (map cadr args))))

             (else
              ;; procedure call
              (let ((proc  (aexp->exp (car exp)))
                    (ucall 'r)
                    (subproc-index #f)
                    (proctype #f)
                    (is-global #f)
                    (is-boxed #f))

                (define (genproc-match gptype args) ;; -> (subproc index)
                  (find-with-index (lambda (subproc)
                                     (or (= (length args) (arity subproc))
                                         (and (> (length args) (arity subproc))
                                              (vararg? subproc))))
                                   (slot-ref gptype 'subprocs)))

                ;; evaluate arguments
                (let1 args (map (^e (s2i-compile-exp e cxt adesc #f))
                                (cdr exp))
                  (for-each emit (map car args))

                  (when *verbose*
                    (if-let1 t (type-of (car exp))
                      (format #t "typed car: ~a: ~a~%" proc t)
                      (if-let1 v (get (aexp->env aexp) proc)
                        (if (is-a? v <scheme-variable>)
                            (format #t "type in env: ~a: ~a~%"
                                    proc (type v))
                            (format #t "untyped: ~a~%" proc))
                        (format #t "not in env???: ~a~%" proc))))

                  (cond ((symbol? proc)
                         (let1 var (get (aexp->env aexp) proc)
                           (unless var
                             (errorf "unknown symbol: ~a" (car exp)))
                           (set! proctype (type var))
                           (set! is-global (global? var))
                           (set! is-boxed (boxed? var))))
                        ((pair? proc)
                         ;; nothing
                         )
                        (else
                         (error "cannot apply")))

                  (when (is-a? proctype <generic-function-type>)
                    (receive (i t)
                        (genproc-match proctype args)
                      (unless i
                        (errorf "no matching subproc found: proc=~a, arity=~a"
                                proc (length args)))
                      (set! subproc-index i)
                      (set! proctype t)))

                  (unless (is-a? proctype <procedure-type>)
                    (set! proctype (make <procedure-type>
                                     :arity 0 :vararg #t))
                    (set! ucall 'u))

                  ;; arity check
                  (let ((nargs (length args))
                        (a     (arity proctype)))
                    (if (vararg? proctype)
                        (when (< nargs a)
                          (errorf "\"~a\" requires ~a or more parameters~%" proc a))
                        (when (not (= nargs a))
                          (errorf "\"~a\" should have ~a parameters (actual:~a)~%" proc a nargs))))

                  ;; take arguments as a list?
                  (when (vararg? proctype)
                    (if (= (length args) (arity proctype))
                        ;; empty list
                        (let1 l (newreg cxt)
                          (emit 'i.null l)
                          (set! args (append (take args (arity proctype))
                                             (list `(#f ,l)))))

                        ;; convert args to a list
                        (let ((list-args (drop args (arity proctype)))
                              (l         (newreg cxt))  ;; first pair
                              (tmp       (newreg cxt))  ;; temporary
                              (prev      (newreg cxt))) ;; previous pair
                          ;; l = cons(1st, 1st);
                          (emit 'i.call 'c (cproc-icode-symbol 'cons) l
                                (cadr (car list-args)) (cadr (car list-args)))
                          ;; prev = l;
                          (emit 'i.move prev l)
                          (for-each (lambda (r)
                                      ;; tmp = cons(r,r);
                                      (emit 'i.call 'c
                                            (cproc-icode-symbol 'cons)
                                            tmp r r)
                                      ;; set-cdr!(prev, tmp);
                                      (emit 'i.call 'c
                                            (cproc-icode-symbol 'set-cdr!)
                                            #f prev tmp)
                                      ;; prev = tmp;
                                      (emit 'i.move prev tmp))
                                    (map cadr (cdr list-args)))
                          ;; tmp = NULL;
                          (emit 'i.null tmp)
                          ;; set-cdr!(prev, tmp);
                          (emit 'i.call 'c (cproc-icode-symbol 'set-cdr!)
                                #f prev tmp)

                          (set! args (append (take args (arity proctype))
                                             (list `(#f ,l)))))))

                  (let ((arg-regs (map cadr args)))
                    (define (let-jump label arg-regs value-regs)
                      (for-each (lambda (ra rv)
                                  (emit 'i.move ra rv))
                                arg-regs value-regs)
                      (emit 'i.jmp label))
                    (define (i-call itype iproc)
                      (when subproc-index
                        (let1 rsub (newreg cxt)
                          (emit 'i.subproc rsub iproc subproc-index)
                          (set! iproc rsub)))
                      (if tail
                          (emit (cons* 'i.tailcall itype iproc arg-regs))
                          (let1 ca (slot-ref aexp 'cont-argspec)
                            (if ca
                                (let1 ret-regs (map (^_ (newreg cxt))
                                                    (car fargs))
                                  (emit (append `(i.multicall ,itype ,iproc)
                                                ret-regs
                                                '(#f)
                                                arg-regs))
                                  (set! rret ret-regs))
                                (let1 rr (newreg cxt)
                                  (emit (cons* 'i.call itype iproc rr
                                               arg-regs))
                                  (set! rret rr))))))

                    (if (pair? proc)
                        ;; ((exp ...) ...)
                        (let1 r (s2i-compile-exp (car exp) cxt adesc #f)
                          (emit (car r))
                          (i-call ucall (cadr r)))

                        ;; (sym ...)
                        (if-let1 r (get adesc proc)
                          (cond ((and (pair? r)
                                      (eq? 'box (car r)))
                                 (let1 r2 (newreg cxt)
                                   (emit 'i.unbox r2 (cdr r))
                                   (i-call ucall r2)))
                                ((and (pair? r)
                                      (eq? 'named-let (car r)))
                                 (let-jump (cadr r) (cddr r) arg-regs))
                                (else
                                 (i-call ucall r)))
                          (let1 v proctype
                            (unless v
                              (errorf "proc is not in env: ~a" proc))
                            (unless (is-a? v <procedure-type>)
                              (errorf "proc is not a procedure: ~a" proc))
                            (if-let1 fname (cfun v)
                              (begin
                                (when subproc-index
                                  (set! subproc-index #f))
                                (i-call 'c fname))
                              (if is-global
                                  (if subproc-index
                                      (let1 r (newreg cxt)
                                        (emit 'i.load r (symbol-s2i proc))
                                        (i-call ucall r))
                                      (let1 sym (symbol-s2i proc)
                                        (if (eq? ucall 'u)
                                            (let1 r (newreg cxt)
                                              (emit 'i.load r sym)
                                              (i-call 'u r))
                                            (i-call 'g sym))))
                                  (let1 r (newreg cxt)
                                    (emit 'i.display r (curproc cxt)
                                          (add-free-variable cxt proc))
                                    (if is-boxed
                                        (emit 'i.unbox r r))
                                    (i-call ucall r)))))))))))))

          ;; illegal/unknown syntax
          (else (errorf "cannot compile yet: ~a" exp)))

    (unless rret  ;; XXX: need to return some value
      (let1 r (newreg cxt)
        (emit 'i.null r)
        (set! rret r)))

    (if (list? rret)
        (cons insn-q rret)
        (list insn-q rret))))


;; syntax-rules = a set of rules, has a name.
(define-class <scheme-syntax-rules> ()
  (name rules))

;; synatax-rule = a mapping from arguments to templated expressions.
(define-class <scheme-syntax-rule> ()
  (args template rest pattern2))

(define (make-syntax-rules name literals-aexp syntax-rule-list)
  ;; (syntax-rules (<literal> ...) <syntax-rule> ...)
  (define (parse-syntax-rule syntax-rule-aexp)
    ;; syntax-rule-aexp = (<pattern> <template>)
    (let* ((sr (make <scheme-syntax-rule>))
           (a-rule (aexp->exp syntax-rule-aexp))
           (args   (cdr (aexp->exp (car a-rule)))))
      (slot-set! sr 'args (map* unannotate (lambda (a) '()) args))
      (slot-set! sr 'template (cadr a-rule))
      (slot-set! sr 'rest (if (dotted-list? args)
                              (aexp->exp (cdr (last-pair args)))
                              #f))
      (slot-set! sr 'pattern2 (aexp->exp (car a-rule)))
      sr))
  (let1 srules (make <scheme-syntax-rules>)
    (slot-set! srules 'name name)
    (slot-set! srules 'rules (map parse-syntax-rule syntax-rule-list))
    srules))

(define-method expand ((self <scheme-syntax-rules>) arguments)
  (define (match? rule arguments)
    (define (unannotate2 aexp)
      (if (pair? aexp)
          (map* unannotate2 unannotate2 aexp)
          (unannotate aexp)))

    ;; matched     -> ((pvar . elements) ...)
    ;; not matched -> #f
    (define (matchsub pattern elements)
      (define (num-pairs dlist)
        (let loop ((l dlist)
                   (n 0))
          (if (pair? l)
              (loop (cdr l) (+ n 1))
              n)))

      (cond ((eq? pattern '_) '())
            ((symbol? pattern)  ;; non-literal identifier
             (list (cons pattern elements)))
            ;; - literal identifier
            ((proper-list? pattern)
             (and (= (length pattern) (length elements))
                  (let loop ((pl pattern)
                             (el elements)
                             (varlist '()))
                    (if (pair? pl)
                        (let1 vl (matchsub (car pl) (car el))
                          (if vl
                              (loop (cdr pl) (cdr el) (append vl varlist))
                              #f))
                        varlist))))
            ((dotted-list? pattern)
             (and-let* ((n (num-pairs pattern))
                        ( (>= (length elements) n)))
               (let loop ((pl pattern)
                          (el elements)
                          (varlist '()))
                 (if (pair? pl)
                     (let1 vl (matchsub (car pl) (car el))
                       (if vl
                           (loop (cdr pl) (cdr el) (append vl varlist))
                           #f))
                     (let1 vl (matchsub pl el)
                       (and vl (append vl varlist)))))))
            ;; - proper list with ellipsis
            ;; - improper list with ellipsis
            ;; - vector
            ;; - vector with ellipsis
            ;; constant?
            (else
             (raise "unsupported macro pattern"))))

    (matchsub (cdr (unannotate2 (slot-ref rule 'pattern2)))
              (unannotate2 arguments)))

  (define (replace template mappings)
    (let1 r
        (if (null? template)
            '()
            (let1 exp (aexp->exp template)
              (if (pair? exp)
                  (annotate-r (map* (lambda (aexp) (replace aexp mappings))
                                    (lambda (aexp) (replace aexp mappings))
                                    exp)
                              (aexp->pos template))
                  (let1 m (and (symbol? exp) (assq exp mappings))
                    (if m
                        (cdr m)  ;; can be #f
                        exp)))))
      r))
  (any (lambda (rule)
         (if-let1 m (match? rule arguments)
           (aexp->exp (replace (slot-ref rule 'template) m))
           #f))
       (slot-ref self 'rules)))


;; -> (values arity-at-least vararg?)
(define (parse-arity formals)
  (let loop ((l formals)
             (i 0))
    (if (pair? l)
        (loop (cdr l) (+ i 1))
        (values i (not (null? l))))))

;; expression              formals
;; *(lambda *(*a *b))   -> *(*a *b)   -> (*a *b)   -> *a, *b
;; *(lambda *(*a . *b)) -> *(*a . *b) -> (*a . *b) -> *a, *b
;; *(lambda *a)         -> *a
(define (for-each-formal-exps f formals)
  (let1 formals-exp (aexp->exp formals)
    (cond ((pair? formals-exp)
           (for-each* (^a (f (aexp->exp a))) formals-exp))
          ((not (null? formals-exp))
           (f formals-exp)))))

(define (aexp-macroexpand! aexp env)
  (and-let* ((exp-list (aexp->exp aexp))
             ( (pair? exp-list))
             (op (aexp->exp (car exp-list)))
             ( (symbol? op))
             (syntax (get env op))
             ( (is-a? syntax <scheme-syntax-rules>)))
    (aexp-set-exp! aexp (expand syntax (cdr exp-list)))
    (aexp-macroexpand! aexp env)))

(define (report-error aexp error-type msg)
  (when *testcode*
    (let ((error-maker 'make-argument-error)
          (pos         (aexp->pos aexp)))
      (aexp-set-exp! aexp
                     (map (^e (annotate-r e pos))
                          `(raise2 (,error-maker ,(line pos)))))))
  (format #t "~a: ~a: ~a~%"
          (line (aexp->pos aexp)) error-type msg))

(define (transform-define prog cxt env)
  ;; define transformation
  (define (trans aexp env)
    (define (ann template args)
      (cond ((list? template)
             (annotate (map (^t (ann t args)) template) (aexp->pos aexp)))
            ((eq? template 'arg1)
             (second args))
            ((eq? template 'arg2)
             (third args))
            ((eq? (current-class-of template) <annotated-expression>)
             template)
            (else
             (annotate template (aexp->pos aexp)))))
    (define (make-generic-function-type list-of-procs)
      (for-each (^t (unless (is-a? t <procedure-type>)
                      (erorr "not a proc")))
                list-of-procs)
      (let1 g (make <generic-function-type>
                :arity  0
                :vararg #t)
        (slot-set! g 'subprocs list-of-procs)
        g))

    (aexp-set-env! aexp env)

    (let1 exp (aexp->exp aexp)
      (if (not (pair? exp))
          ;; when exp is a value, ...
          (and-let* (( (symbol? exp))
                     (var (get env exp))
                     (t   (type var)))
            (slot-set! aexp 'type t)
            (when (is-a? t <named-let>)
              (unless (slot-ref aexp 'called)
                (slot-set! t 'used #t))
              (unless (slot-ref t 'called)
                (slot-set! t 'called (slot-ref aexp 'called)))))

          ;; when exp is a pair, ...
          (case (aexp->exp (car exp))
            ((call/cc0 call/cc)
             (box-all-variables env)
             (trans (cadr exp) env))

            ;; (case-lambda ((<formals> <body>) ...)
            ;; -> (make-generic-function (lambda <formals> <body>) ...)
            ((case-lambda)
             (aexp-set-exp! (car exp) 'make-generic-function)
             (for-each (lambda (clause)
                         (aexp-set-exp! clause
                                        (cons (annotate 'lambda
                                                        (aexp->pos aexp))
                                              (aexp->exp clause))))
                       (cdr exp))
             (trans aexp env))

            ((define)
             (let1 e2 (if (global-environment? env)
                          (subenv env)
                          env)
               (if (pair? (aexp->exp (second exp)))
                   ;; (define (name . formals) body...)
                   ;; -> (define name (lambda (formals) body...))
                   (let* ((2nd     (aexp->exp (cadr exp)))
                          (name    (aexp->exp (car 2nd)))
                          (formals (cdr 2nd))
                          (pos     (aexp->pos aexp)))
                     (put e2 name (make <scheme-variable>
                                    :type <unknown-type>))
                     (set-cdr! exp (list (car 2nd)
                                         (annotate (cons*
                                                    (annotate 'lambda pos)
                                                    (if (or (pair? formals)
                                                            (null? formals))
                                                        (annotate formals pos)
                                                        formals)
                                                    (cddr exp))
                                                   pos))))
                   ;; (define a ...)
                   (put e2 (aexp->exp (second exp))
                        (make <scheme-variable> :type <unknown-type>)))
               (trans (third exp) e2)
               (let1 t (type-of (third exp))
                 (put e2 (aexp->exp (second exp))
                      (make <scheme-variable>
                        :type t :global (global-environment? env))))
               (copy-type! aexp (third exp)))
             (put env (aexp->exp (second exp))
                  (let ((t (or (slot-ref aexp 'type)
                               (make <scheme-type>)))
                        (env-is-global (global-environment? env)))
                    (make <scheme-variable>
                      :type t
                      :global env-is-global
                      :static env-is-global))))

            ;; (define-cproc (name args) c-symbol)
            ;; -> (define name (lambda-cproc args c-symbol))
            ((define-cproc)
             (aexp-set-exp! aexp
                            (list (ann 'define '())
                                  (car (aexp->exp (second exp)))
                                  (ann (list 'lambda-cproc
                                             (cdr (aexp->exp (second exp)))
                                             (third exp))
                                       '())))
             (trans aexp env))

            ;; (define-generic (<variable> . <formals>) <body>...)
            ((define-generic)
             (let* ((e2        (aexp->exp (second exp)))
                    (a-name    (car e2))
                    (a-formals (cdr e2)))
               (let1 newproc (receive (arity vararg)
                                 (parse-arity a-formals)
                               (make <procedure-type>
                                 :arity arity
                                 :vararg vararg))
                 (if-let1 v (get env (aexp->exp a-name))
                   ;; If it is already defined, add a new sub-procedure to it.
                   (let ((gf (type v)))
                     (unless (is-a? gf <generic-function-type>)
                       (error "gf must be a generic function"))
                     (add-proc gf newproc)
                     ;; -> (generic-function-add! <variable>
                     ;;       (lambda <formals> <body>...))
                     (aexp-set-exp! aexp
                                    (aexp->exp
                                     (ann `(generic-function-add!
                                            ,a-name
                                            (lambda ,a-formals . ,(cddr exp)))
                                          '()))))
                   ;; Otherwise, define the new procedure.
                   (aexp-set-exp! aexp
                                  (aexp->exp
                                   (ann `(define ,a-name
                                           (make-generic-function
                                            (lambda ,a-formals . ,(cddr exp))))
                                        '()))))))
             (trans aexp env))

            ;; (define-generic-cproc (<variable> <formals>) cproc-name)
            ;;
            ;; XXX: should we merge this with define-generic?
            ((define-generic-cproc)
             (let* ((uexp    (unannotate aexp))
                    (name    (caadr uexp))
                    (formals (cdadr uexp))
                    (cproc   (caddr uexp)))
               (let1 newproc (receive (arity vararg)
                                 (parse-arity formals)
                               (make <procedure-type>
                                 :arity arity
                                 :cfun cproc
                                 :vararg vararg))
                 (if-let1 var (get env name)
                   (let ((gf (type var)))
                     (add-proc gf newproc)
                     (aexp-set-exp! aexp
                                    (aexp->exp
                                     (ann `(generic-function-add!
                                            ,name
                                            (lambda-cproc ,formals ,cproc))
                                          '())))
                     (trans aexp env))
                   (begin
                     (aexp-set-exp! aexp
                                    (aexp->exp
                                     (ann `(define ,name
                                             (make-generic-function
                                              (lambda-cproc ,formals ,cproc)))
                                          '())))
                     (trans aexp env))))))

            ;; (define-lib-proc (name args ...))
            ((define-lib-proc)
             (let1 argspec (unannotate (second exp))
               (receive (a v)
                   (parse-arity (cdr argspec))
                 (put env (car argspec)
                      (make <scheme-variable>
                        :type (make <procedure-type> :arity a :vararg v)
                        :global #t))))
             (slot-set! aexp 'exp #t))

            ;; (define-lib-variable name)
            ((define-lib-variable)
             (put env (aexp->exp (second exp))
                  (make <scheme-variable>
                    :type (make <unknown-type>) :global #t))
             (slot-set! aexp 'exp #t))

            ;; (define-library <name> <decl> ...)
            ((define-library)
             (let1 lib (make <scheme-library>)
               (slot-set! lib 'name (unannotate (second exp)))
               (slot-set! cxt 'lib lib)
               (slot-set! cxt 'libname (unannotate (second exp)))
               (let1 env (make <scheme-environment>)
                 (import-initial-bindings env)
                 (aexp-set-env! aexp env)
                 (slot-set! lib 'env env)
                 (for-each (lambda (decl-aexp)
                             (case (aexp->exp (car (aexp->exp decl-aexp)))
                               ((begin)
                                (trans decl-aexp env))
                               ((import)
                                (trans decl-aexp env))))
                           (cddr exp)))))

            ;; (define-syntax keyword (syntax-rules () ...))
            ((define-syntax)
             (let ((keyword          (aexp->exp (second exp)))
                   (transformer-spec (aexp->exp (third exp))))
               (put env
                    keyword
                    (make-syntax-rules keyword
                                       (cadr transformer-spec)
                                       (cddr transformer-spec))))
             (slot-set! aexp 'exp #t))

            ;; (define-values <formals> <expression>)
            ((define-values)
             (for-each* (lambda (var)
                          (let1 t (make <scheme-type>)
                            (put env var
                                 (make <scheme-variable> :type t
                                       :global (global-environment? env)
                                       :static (global-environment? env)))))
                        (unannotate (second exp)))
             (trans (third exp) env)
             ;; XXX: should set types of multiple values
             (slot-set! aexp 'type (make <undefined-type>)))

            ((import)
             (for-each (lambda (import-set-aexp)
                         (let* ((libname (unannotate import-set-aexp))
                                (filename (format #f "build/~a.d.scm"
                                                  (libname->pathname
                                                   libname))))
                           (let1 lib (load-libdef-file libname filename)
                             (import-library-environment env lib)
                             (add-to-import-list (slot-ref cxt 'module) lib))))
                       (cdr exp)))

            ;; transform (lambda <formals> <body>)
            ((lambda)
             (let1 e2 (subenv env)
               (receive (arity vararg)
                   (parse-arity (aexp->exp (second exp)))
                 (slot-set! aexp 'type
                            (make <procedure-type>
                              :arity arity :vararg vararg)))
               ;; bind variables in formals
               (for-each-formal-exps (^a (put e2 a
                                              (make <scheme-variable>
                                                :type (make <unknown-type>))))
                                     (second exp))
               ;; internal definitions or body expressions
               (let ((in-defs? #t)
                     (expand!-and-test
                      (lambda (aexp)
                        (aexp-macroexpand! aexp e2)
                        (and-let* ((exp (aexp->exp aexp))
                                   ( (pair? exp))
                                   (op (aexp->exp (car exp))))
                          (or (eq? op 'define)
                              (eq? op 'define-values)))))
                     (peek-internal-definition
                      (lambda (aexp)
                        ;; exp1 = (<op> <arg1> ...)
                        (and-let* ((exp1  (aexp->exp aexp))
                                   (op    (aexp->exp (car exp1)))
                                   (arg1  (aexp->exp (cadr exp1))))
                          (cond ((eq? op 'define)
                                 (cond ((symbol? arg1)
                                        (put e2 arg1
                                             (make <scheme-variable>
                                               :type (make <unknown-type>))))
                                       ((pair? arg1)
                                        (let1 name (aexp->exp (car arg1))
                                          (put e2 name
                                               (make <scheme-variable>
                                                 :type (make <procedure-type>))
                                               )))))
                                ((eq? op 'define-values)
                                 (for-each-formal-exps
                                  (lambda (arg)
                                    (put e2 arg
                                         (make <scheme-variable>
                                           :type (make <unknown-type>))))
                                  (cadr exp1)))
                                )))))
                 (for-each (lambda (aexp)
                             (if (expand!-and-test aexp)
                                 (if in-defs?
                                     (peek-internal-definition aexp)
                                     (errorf "internal definition after an expression"))

                                 (set! in-defs? #f))
                             (trans aexp e2))
                           (cddr exp)))))

            ;; (lambda-cproc <formals> <c-symbol>)
            ((lambda-cproc)
               (receive (arity vararg)
                   (parse-arity (aexp->exp (second exp)))
                 (slot-set! aexp 'type
                            (make <procedure-type>
                              :arity arity
                              :cfun (aexp->exp (third exp))
                              :vararg vararg))))

            ;; (let ((var1 init1) ...) body...)
            ;; (let name ((var1 init1) ...) body...)
            ((let)
             (let* ((e2       (subenv env))
                    (name     (let1 2nd (aexp->exp (cadr exp))
                                (if (not (list? 2nd))
                                    2nd
                                    #f)))
                    (bindings (aexp->exp (if name
                                             (caddr exp)
                                             (cadr exp))))
                    (body     (if name
                                  (cdddr exp)
                                  (cddr exp))))

               ;; put bindings on e2
               (for-each (lambda (binding)
                           (let ((var  (car (aexp->exp binding)))
                                 (init (cadr (aexp->exp binding))))
                             (trans init env)
                             (when *verbose*
                               (format #t "let: \"~a\" is ~a.~%"
                                       (aexp->exp var)
                                       (type-of init)))
                             (put e2 (aexp->exp var)
                                  (make <scheme-variable>
                                    :type (or (type-of init)
                                              (make <unknown-type>))))))
                         bindings)
               (if name
                   (put e2 name
                        (make <scheme-variable>
                          :type (make <named-let> :arity (length bindings)))))
               (set-body-env! aexp e2)

               ;; trans body part
               (for-each (^a (trans a e2)) body)

               (when name
                 (let* ((var  (get e2 name))
                        (attr (type var)))
                   (define (transform-named-let->letrec aexp)
                     ;; (let loop ((a 123)
                     ;;            (b 234))
                     ;;   (foo ...)
                     ;;   (loop b a))
                     ;; ->
                     ;; ((letrec ((loop (lambda (a b)
                     ;;                   (foo ...)
                     ;;                   (loop b a))))
                     ;;    loop) 123 234)
                     (define (lambda->letrec)
                       `(letrec ((,name (lambda ,(map (.$ car aexp->exp)
                                                      bindings) . ,body)))
                          ,name))
                     (let1 proc (ann (lambda->letrec) aexp)
                       ;; TODO: type inference
                       (slot-set! proc 'type
                                  (make <procedure-type>
                                    :arity (length bindings) :vararg #f))
                       (aexp-set-exp! aexp (cons proc (map (.$ cadr aexp->exp)
                                                           bindings))))
                     (trans aexp env))

                   ;; 1. If the named-let object is used,
                   ;;    transform it to a letrec expression.
                   ;; 2. If it is neither used nor called,
                   ;;    it can be unnamed.
                   ;; 3. Otherwise, it is to be compiled to a jump.
                   (cond ((slot-ref attr 'used)
                          (transform-named-let->letrec aexp))
                         ((not (slot-ref attr 'called))
                          (set-cdr! exp (cddr exp))))
                   ))))

            ;; (let* ((a v0) (b v1) ...) body...)
            ((let*)
             (let loop ((e1       env)
                        (bindings (aexp->exp (cadr exp))))
               (let1 e2 (subenv e1)
                 (if (pair? bindings)
                     ;; bindings
                     (let* ((binding (aexp->exp (car bindings)))
                            (name    (car binding))
                            (init    (cadr binding)))
                       (trans init e1)
                       (put e2 (aexp->exp name)
                            (make <scheme-variable> :type (type-of init)))
                       (loop e2 (cdr bindings)))
                     ;; body part
                     (begin
                       (set-body-env! aexp e2)
                       (for-each (^a (trans a e2)) (cddr exp)))))))

            ;; (let*-values (((a b) init0) ((c) init1) ...) body...)
            ((let*-values)
             (let loop ((e1       env)
                        (bindings (aexp->exp (cadr exp))))
               (let1 e2 (subenv e1)
                 (if (pair? bindings)
                     ;; bindings
                     (let1 binding (aexp->exp (car bindings))
                       (for-each* (lambda (var)
                                    (put e2 var
                                         (make <scheme-variable>
                                           :type (make <unknown-type>))))
                                  (unannotate (car binding)))
                       (trans (cadr binding) e1)
                       (loop e2 (cdr bindings)))
                     ;; body
                     (begin
                       (set-body-env! aexp e2)
                       (for-each (^a (trans a e2)) (cddr exp)))))))

            ;; (let-values (((a b) init0) ((c) init1) ...) body...)
            ((let-values)
             (let1 e2 (subenv env)
               (for-each (lambda (binding-aexp)
                           (let1 binding (aexp->exp binding-aexp)
                             (for-each* (lambda (var)
                                          (put e2 var
                                               (make <scheme-variable>
                                                 :type (make <unknown-type>))))
                                        (unannotate (car binding)))
                             (trans (cadr binding) env)))
                         (aexp->exp (cadr exp)))
               (set-body-env! aexp e2)
               (for-each (^a (trans a e2)) (cddr exp))))

            ;; (letrec ((var1 init1) ...) body...)
            ;; (letrec* ((var1 init1) ...) body...)
            ((letrec letrec*)
             (let* ((e2       (subenv env))
                    (bindings (aexp->exp (cadr exp)))
                    (body     (cddr exp)))

               (for-each (lambda (binding)
                           (let ((var (car (aexp->exp binding))))
                             (put e2 (aexp->exp var)
                                  (make <scheme-variable>
                                    :type (make <unknown-type>)
                                    :boxed #t))))
                         bindings)
               (for-each (lambda (binding)
                           (let ((var  (car  (aexp->exp binding)))
                                 (init (cadr (aexp->exp binding))))
                             (trans init e2)
                             (put e2 (aexp->exp var)
                                  (make <scheme-variable>
                                    :type (or (type-of init)
                                              (make <unknown-type>))
                                    :boxed #t))))
                         bindings)
               (set-body-env! aexp e2)

               ;; trans body part
               (for-each (^a (trans a e2)) body)))

            ((load)
             (let1 prog (read-program (aexp->exp (second exp)))
               (let ((pos (aexp->pos aexp)))
                 (aexp-set-exp! aexp
                                (cons (annotate 'begin pos)
                                      (aexp->exp prog)))
                 (trans aexp env))))

            ;; (set! lvar ...)
            ((set!)
             (let ((lvar (aexp->exp (second exp))))
               ;;(format #t "set! ~a~%" lvar)
               (if (free-variable? env lvar)
                   (set-boxed-variable! env lvar)))
             (trans (third exp) env))

            ;; procedure or syntax application
            (else
             (let1 val (get env (aexp->exp (car exp)))
               (if (and val (eq? (current-class-of val) <scheme-syntax-rules>))
                   (begin
                     ;; macro expansion
                     (aexp-set-exp! aexp (expand val (cdr exp)))
                     (trans aexp env))
                   (begin
                     ;; procedure application
                     (slot-set! (car (aexp->exp aexp)) 'called #t)

                     (let loop ((l (aexp->exp aexp)))
                       (trans (car l) env)
                       (if (pair? (cdr l))
                           (loop (cdr l))))

                     (and-let* ((t (type-of (car exp)))
                                ( (is-a? t <procedure-type> ))
                                (proc-sym (aexp->exp (car exp))))

                       ;; check the number of arguments
                       (when (and (not (vararg? t))
                                  (not (= (length exp) (+ (arity t) 1))))
                         (report-error aexp 'argument-error
                                       (format #f
                                               "\"~a\" ~a ~a ~a (actual:~a)"
                                               proc-sym "should have"
                                               (arity t) "parameters"
                                               (- (length exp) 1)))
                         (for-each (^e (trans e env)) (aexp->exp aexp))))

                     ;; Generic function type
                     (if (eq? (aexp->exp (car exp)) 'make-generic-function)
                         (slot-set! aexp 'type
                                    (make-generic-function-type
                                     (map type-of (cdr exp)))))))))))))

  (aexp-set-env! prog env)
  (for-each (^e (trans e env))
            (aexp->exp prog)))

(define (compile-scheme-program-to-icode prog outport mainp)
  (define (automatic-import prog env)
    (define (recursive-annotate exp)
      (if (pair? exp)
          (annotate (map (^e (recursive-annotate e)) exp) (aexp->pos prog))
          (annotate exp (aexp->pos prog))))
    (unless (and-let* ((exps (aexp->exp prog))
                       ( (pair? exps))
                       (exp1 (aexp->exp (car exps))))
              (and (pair? exp1)
                   (or (eq? (aexp->exp (car exp1)) 'import)
                       (eq? (aexp->exp (car exp1)) 'define-library))))
      ;;(print "auto-import")
      (let1 libs (filter-map (lambda (filename)
                               (let1 name (map string->symbol
                                               (drop-right
                                                (string-split
                                                 (sys-basename filename) #\.)
                                                2))
                                 (if (eq? (car name) 'test)
                                     #f
                                     name)))
                             (sys-glob "build/*.d.scm"))
        (let* ((1st-pair (aexp->exp prog))
               (copy (cons (car 1st-pair) (cdr 1st-pair))))
          (set-car! 1st-pair (recursive-annotate (cons 'import libs)))
          (set-cdr! 1st-pair copy)))))
  (define (insn-queue->list insn-q)
    (list-queue-list insn-q))
  (define (write-defproc proc)
    (let ((name   (car proc))
          (arity  (cadr proc))
          (insn-l (insn-queue->list (caddr proc))))
      (format outport "(defproc ~s ~a\n" name arity)
      (let loop ((l insn-l))
        (unless (null? l)
          (format outport "  ~a" (if (eq? l insn-l) "(" " "))
          (write (car l) outport)
          (format outport "~a\n" (if (pair? (cdr l)) "" "))"))
          (loop (cdr l))))))

  (let ((cxt    (make <scheme-to-icode-context>))
        (env    (make <scheme-environment>))
        (adesc  (make <address-descriptor>)))
    (slot-set! cxt 'module (make <scheme-module>))
    (slot-set! cxt 'ico (make-intermediate-code-object))

    ;; pass 1
    (import-initial-bindings env)
    (automatic-import prog env)
    (transform-define prog cxt env)

    ;; compile toplevel expressions
    (let1 toplevels (map (lambda (aexp)
                           (s2i-compile-exp aexp cxt adesc #f))
                         (aexp->exp prog))
      (let1 r (newreg cxt)
        (defproc cxt (if mainp
                         "s_main"
                         (libname->main-proc (slot-ref cxt 'libname)))
          0
          (list-queue-append!
           (list-queue `(i.pointer ,r "global_variable_pointers")
                       `(i.call c "s_mem_gvar" ,r ,r))
           (list-queue-concatenate (map car toplevels))
           (list-queue '(i.ret))))))

    ;; write symbols
    (hash-table-for-each (lambda (name _)
                           (format outport "(defsym ~a)\n" name))
                         (slot-ref cxt 'symtab))

    ;; write literal values
    (format outport "(defliteral\n")
    (for-each (lambda (l)
                (format outport "  (~s ~s)\n" (car l) (cadr l)))
              (reverse (slot-ref (slot-ref cxt 'ico) 'data)))
    (format outport "  )\n")

    ;; write procedures
    (for-each write-defproc (get-procs cxt))

    (when mainp
      (put (aexp->env prog) 'stack-bottom
           (make <scheme-variable> :type (make <unknown-type>) :static #t)))

    ;; write global variables (from env)
    (let1 gvars (append (global-variables (aexp->env prog))
                        (or (and-let* ((lib (slot-ref cxt 'lib))
                                       (env (slot-ref lib 'env)))
                              (global-variables env))
                            '()))
      (write (cons 'defvar
                   (map (^p (list (symbol-s2i (car p)) #f))
                        (filter (^t (and (is-a? (cdr t) <scheme-variable>)
                                         (static? (cdr t))))
                                gvars)))
             outport))

    cxt))

;;
;; Machine Code Generation
;;
(define *sval-tag-str*        #x04)
(define *sval-tag-procedure*  #x05)
(define *sval-tag-box*        #x06)
(define *sval-tag-char*       #x07)
(define *sval-tag-vector*     #x08)
(define *sval-tag-bytevector* #x09)
(define *sval-tag-gfun*       #x0d)
(define *sval-nil*            #x0e)
(define *sval-false*          #x0f)
(define *sval-true*           #x1f)
(define *sval-unspecified*    #x2f)

(define *bytevector-bytes-offset* 8)
(define *string-bytes-offset* 8)
(define *symbol-name-offset* 8)

(define *proc-offset-code* (+ (- *sval-tag-procedure*) 8))
(define *proc-offset-display* 40)
(define *gfun-offset-procs* 24)

(define (sval value)
  (cond ((integer? value)
         (* value 16))
        ((char? value)
         (+ *sval-tag-char* (arithmetic-shift (char->integer value) 32)))
        (else
         (errorf "cannot convert ~a to sval" value))))

(define (icode-register? sym)
  (and (symbol? sym)
       (if-let1 str (symbol->string sym)
         (and (>= (string-length str) 2)
              (eq? (string-ref str 0) #\r)
              (not (string-index (string-drop str 1) #[^0-9]))))))

;; insn -> (values set-of-read-regs set-of-write-regs)
(define (insn-regs insn)
  (define (ret rl wl)
    (define (to-set a)
      (cond ((pair? a)
             (list->set eq-comparator a))
            ((null? a)
             (set eq-comparator))
            (else
             (set eq-comparator a))))
    (values (to-set rl) (to-set wl)))
  (case (car insn)
    ((i.jmp i.label) ;; (_ ...)
     (ret '() '()))
    ((i.bytevector i.char i.false i.fixnum i.frame i.lambda i.load i.null
                   i.pointer i.string i.symbol i.true i.unspecified i.vector)
     (ret '() (second insn)))  ;; (_ w _...)
    ((i.add i.box i.sub i.unbox)  ;; (_ w r1...)
     (ret (drop insn 2) (second insn)))
    ((i.jf i.jt i.store)  ;; (_ r _...)
     (ret (second insn) '()))
    ((i.ret)  ;; (_ r1...)
     (ret (cdr insn) '()))
    ((i.display i.move i.subproc)  ;; (_ w r _...)
     (ret (third insn) (second insn)))
    ((i.set-box!)  ;; (_ r1 r2)
     (ret (cdr insn) '()))
    ((i.jeq)  ;; (_ r1 r2 _)
     (ret (list (second insn) (third insn)) '()))
    ((i.setdisplay)  ;; (_ r1 _ r2)
     (ret (list (second insn) (fourth insn)) '()))
    ((i.call)  ;; (_ _ ? w r1...)
     (case (second insn)
       ((c g)  ;; (_ _ _ w r1...)
        (ret (drop insn 4) (fourth insn)))
       ((r u)  ;; (_ _ r1 w r2...)
        (ret (cons (third insn) (drop insn 4)) (fourth insn)))
       (else
        (errorf "insn-regs: unknown i.call type: ~a" insn))))
    ((i.multicall) ;; (_ _ ? _ w1... r1...)
     (let* ((proc  (second insn))
            (type  (third insn))
            (nret  (fourth insn))
            (rrets (take (drop insn 4) nret))
            (rargs (drop insn (+ 4 nret))))
       (case type
         ((c g)  ;; (_ _ _ _ w1... r1...)
          (ret rargs rets))
         ((r u)  ;; (_ _ r1 _ w1... r2...)
          (ret (cons proc rargs) rets))
         (else
          (errorf "insn-regs: unknown i.multicall type: ~a" insn)))))
    ((i.tailcall)
     (case (second insn)
       ((c g)  ;; (_ _ _ r1...)
        (ret (drop insn 3) '()))
       ((r u)  ;; (_ _ r1 r2...)
        (ret (drop insn 2) '()))
       (else
        (errorf "insn-regs: unknown i.tailcall type: ~a" insn))))
    (else
     (errorf "insn-regs does not know ~a" (car insn)))))

;; -> #((set live-reg...) ...)
(define (live-register-analysis arity insn-list)

  ;; -> alist of (label . index)
  (define (make-label-index insn-list)
    (let1 alist '()
      (for-each-with-index (lambda (i insn)
                             (when (eq? (first insn) 'i.label)
                               (push! alist (cons (second insn) i))))
                           insn-list)
      alist))

  ;; -> alist of (label jump-index1 jump-index2 ...)
  (define (make-jump-map insn-list)
    (let1 alist '()
      (define (add-label label idx)
        (if-let1 pair (assoc label alist)
          (set-cdr! pair (cons idx (cdr pair)))
          (push! alist (list label idx))))
      (for-each-with-index (lambda (i insn)
                             (case (first insn)
                               ((i.jmp)     (add-label (second insn) i))
                               ((i.jf i.jt) (add-label (third insn) i))
                               ((i.jeq)     (add-label (fourth insn) i))))
                           insn-list)
      alist))

  (when *verbose*
    (format #t "  - analyze live registers~%"))
  (let* ((insn-vec      (list->vector insn-list))
         (live-regs-vec (make-vector (vector-length insn-vec) #f))
         (label-index   (make-label-index insn-list))
         (jump-map      (make-jump-map insn-list))
         (empty-regs    (set eq-comparator))
         (loop-counter  0))
    (define (set-live-regs! i regs)
      (vector-set! live-regs-vec i regs))
    (define (label-to-index label)
      (cdr (assoc label label-index)))
    (define (print-icode-with-regs)
      (for-each-with-index (lambda (i insn regs)
                             (format #t "    ~4d ~40a ~a~%"
                                     i insn
                                     (sort (set->list regs))))
                           insn-vec live-regs-vec))

    (let loop ((idx (- (vector-length insn-vec) 1))
               (last-live-regs empty-regs))
      (inc! loop-counter)
      (when (>= idx 0)
        (let ((insn     (vector-ref insn-vec idx))
              (old-regs (vector-ref live-regs-vec idx))
              (regs     #f))
          (receive (rl wl)
              (insn-regs insn)
            (case (car insn)
              ((i.ret i.tailcall)
               (set! regs rl))
              ((i.jmp)
               (set! regs (vector-ref live-regs-vec
                                      (label-to-index (second insn)))))
              ((i.jeq i.jf i.jt)
               (let1 jmp-regs (vector-ref live-regs-vec
                                          (label-to-index
                                           (if (eq? (car insn) 'i.jeq)
                                               (fourth insn)
                                               (third  insn))))
                 (set! regs (set-union rl
                                       (or jmp-regs empty-regs)
                                       last-live-regs
                                       (or old-regs empty-regs)))))
              ((i.label)
               (set! regs last-live-regs)
               (unless (and old-regs (set<=? regs old-regs))
                 (set-live-regs! idx regs)
                 (if-let1 p (assoc (second insn) jump-map)
                   (for-each (lambda (i)
                               (loop i regs)
                               (let1 regs2 (vector-ref live-regs-vec idx)
                                 (when (not (set=? regs regs2))
                                   (set! regs (set-union regs regs2))
                                   (set! old-regs regs))))
                             (cdr p)))))
              (else
               (set! regs (if last-live-regs
                              (set-union rl (set-difference last-live-regs wl))
                              rl)))))
          (unless (and old-regs (set<=? regs old-regs))
            (set-live-regs! idx regs)
            (loop (- idx 1) regs)))))
    (when *verbose*
      (print-icode-with-regs)
      (format #t "    loop count: ~a~%" loop-counter))
    live-regs-vec))

(define (build-register-interference-graph regs-vec)
  (let ((rig (make-tree-map)))  ;; treemap of r0 -> (set of r1, r2...)
    (define (add-to-rig r regs)
      (if-let1 s (tree-map-get rig r #f)
        (set-union! s regs)
        (tree-map-put! rig r regs)))
    (define (print-rig)
      (format #t "  - register inference graph:~%")
      (tree-map-for-each rig
                         (lambda (k v)
                           (format #t "    ~a -> ~a~%"
                                   k (sort (set->list v))))))
    (for-each (lambda (regs)
                (set-for-each (lambda (r0)
                                (add-to-rig r0 (set-delete regs r0)))
                              regs))
              regs-vec)
    (when *verbose*
      (print-rig))
    rig))

(define (color-registers rig arity)
  (define mregs-for-args #("%rdi" "%rsi" "%rdx" "%rcx" "%r8" "%r9"))
  (define mreg-vec #("%rdi" "%rsi" "%rdx" "%rcx" "%r8" "%r9" "%r10"
                     "%r11" "%r12" "%r13" "%r14" "%r15"))
  (let ((colormap (make-tree-map)))  ;; treemap of ir -> mr.
    (define (print-colormap)
      (format #t "  - register assignments:~%")
      (tree-map-for-each colormap
                         (lambda (ir mr)
                           (format #t "    ~4a -> ~a~%" ir mr))))

    ;; assign argument registers
    (when (> arity (vector-length mregs-for-args))
      (errorf "no regs for arguments: ~a args > ~a regs"
              arity (vector-length mregs-for-args)))
    (dotimes (i (+ arity 1))  ;; one for closure
      (let ((ir (string->symbol (format #f "r~a" i))))
        (tree-map-put! colormap ir (vector-ref mregs-for-args i))))

    ;; assign other registers
    (tree-map-for-each rig
                       (lambda (ir ireg-set)
                         (unless (tree-map-exists? colormap ir)
                           (let1 mreg-set
                               (set-map equal-comparator
                                        (lambda (ir2)
                                          (tree-map-get colormap ir2 #f))
                                        ireg-set)
                             (set->list mreg-set)
                             (let1 mr (vector-any
                                       (lambda (cand)
                                         (and (not (set-contains? mreg-set
                                                                  cand))
                                              cand))
                                       mreg-vec)
                               (tree-map-put! colormap ir mr)
                               )))))

    (when *verbose*
      (print-colormap))
    colormap))

(define (register-assignment insn-list regs-vec arity)
  (let1 rig (build-register-interference-graph regs-vec)
    (color-registers rig arity)
    ))

(define (icode-codegen infile p)
  (define mregs-for-args '("%rdi" "%rsi" "%rdx" "%rcx" "%r8" "%r9"))
  (define mregs-for-return-values
    '("%rax""%rdi" "%rsi" "%rdx" "%rcx" "%r8" "%r9" "%r10" "%r11"))

  (define (parallel-assign src-regs dst-regs)
    (define (delete-at! list idx)
      (if (= idx 0)
          (cdr list)
          (let ((head (list-tail list (- idx 1)))
                (tail (list-tail list (+ idx 1))))
            (set-cdr! head tail)
            list)))

    ;; 1. If a register in dst-regs is not in src-regs, copy it.
    ;; If every register in dst-regs is a member of src-regs, go to phase 2.
    (let copy-loop ((i 0)
                    (len (length dst-regs)))
      (when (< i len)
        (let ((dst (list-ref dst-regs i)))
          (if (member dst src-regs)
              (copy-loop (+ i 1) len)
              (begin
                (format p "    mov ~a, ~a\n" (list-ref src-regs i) dst)
                (set! src-regs (delete-at! src-regs i))
                (set! dst-regs (delete-at! dst-regs i))
                (copy-loop 0 (- len 1)))))))

    ;; 2. Exchange the first register in src-regs (src) and the first
    ;; register in dst-regs (dst) if it is not equal, and remove them.
    (while (pair? src-regs)
      (let ((src (pop! src-regs))
            (dst (pop! dst-regs)))
        (unless (equal? src dst)
          (format p "    xchg ~a, ~a\n" src dst)
          ;; 3. Replace dst in src-regs with src.
          (set-car! (member dst src-regs) src)))))

  (define (untag type)
    (case type
      ((proc) (- *sval-tag-procedure*))
      ((box)  (- *sval-tag-box*))
      (else (errorf "unknown tag type: ~a" type))))

  (define (epilogue p procname)
    (if (string=? procname "s_main")
        (begin
          ;; special epilogue
          (format p "    addq $8, %rsp\n")
          (for-each (^r (format p "    popq ~a\n" r))
                    (reverse '("%rbx" "%r12" "%r13" "%r14" "%r15")))))
    (format p "    popq %rbp\n"))

  (define (defproc icode)
    (when *verbose*
      (format #t "* codegen proc: ~a~%" (second icode)))

    (let* ((procname (second icode))
           (arity    (third icode))
           (body     (fourth icode))
           (idx      0)
           (live-regs-vec (live-register-analysis arity body))
           (regmap (register-assignment body live-regs-vec arity)))

      (define (ireg->mreg ir)
        (tree-map-get regmap ir "%rax"))

      (define (our-regs wr)
        (let1 regset (vector-ref live-regs-vec (+ idx 1))
          (if (pair? wr)  ;; wr is a reg (symbol) or a list of regs
              (apply set-delete! (cons regset wr))
              (set-delete! regset wr))
          (map ireg->mreg (set->list regset))))

      (define (save-our-regs is-cproc wr)
        (let1 regs (our-regs wr)
          (if (even? (length regs))
              (begin
                (format p "    pushq $~a\n"
                        (+ 1 (length regs) (if is-cproc 256 0)))
                (format p "    pushq $~a\n" *sval-false*))
              (format p "    pushq $~a\n"
                      (+ (length regs) (if is-cproc 256 0))))
          (for-each (^r (format p "    pushq ~a\n" r)) regs)))

      (define (restore-our-regs wr)
        (let1 regs (our-regs wr)
          (for-each (^r (format p "    popq ~a\n" r))
                    (reverse regs))
          (format p "    addq $~d, %rsp\n"
                  (if (even? (length regs)) 16 8))))

      (define (set-proc-args closure? regs)
        (parallel-assign (map ireg->mreg regs)
                         (take (if closure?
                                   (cdr mregs-for-args)
                                   mregs-for-args)
                               (length regs))))

      (format p ".text\n")
      (unless (string-prefix? "i_" procname)
        (format p ".globl ~a\n" (csymbol procname)))
      (format p "~a:\n" (csymbol procname))
      (format p "    pushq %rbp\n")
      (format p "    movq %rsp, %rbp\n")

      (when (string=? procname "s_main")
        ;; special prologue for "s_main"
        (for-each (^r (format p "    pushq ~a\n" r))
                  '("%rbx" "%r12" "%r13" "%r14" "%r15"))
        (for-each (^r (format p "    movq $0x~x, ~a\n" *sval-false* r))
                  '("%rax" "%rbx" "%rcx" "%rdx" "%rsi" "%rdi" "%r8" "%r9"
                    "%r10" "%r11" "%r12" "%r13" "%r14" "%r15"))
        (format p "    pushq %rax  /* align stack */\n")
        (format p "    movq %rsp, ~a(%rip)\n" (scmsymbol "stack_2d_bottom")))

      (for-each (lambda (insn)
                  (case (car insn)
                    ;; (i.add r0 r1 r2)
                    ;; -> r0 = r1 + r2
                    ((i.add)
                     (let ((mr0 (ireg->mreg (second insn)))
                           (mr1 (ireg->mreg (third insn)))
                           (mr2 (ireg->mreg (fourth insn))))
                       (cond ((equal? mr0 mr1)
                              (format p "    addq ~a, ~a\n" mr2 mr0))
                             ((equal? mr0 mr2)
                              (format p "    addq ~a, ~a\n" mr1 mr0))
                             (else
                              (format p "    movq ~a, ~a\n" mr1 mr0)
                              (format p "    addq ~a, ~a\n" mr2 mr0)))))

                    ;; (i.box R0 R1)
                    ;; -> R0 = box(R1)
                    ((i.box)
                     (save-our-regs #t (second insn))
                     (set-proc-args #f (list (third insn)))
                     (format p "    callq ~a\n"
                             (scmsymbol "box"))
                     (restore-our-regs (second insn))
                     (format p "    movq %rax, ~a\n"
                             (ireg->mreg (second insn))))

                    ;; (i.bytevector reg label)
                    ((i.bytevector)
                     (format p "    leaq ~a+~a(%rip), ~a\n"
                             (csymbol (third insn))
                             *sval-tag-bytevector*
                             (ireg->mreg (second insn))))

                    ;; (i.call ptype proc rret regs...)
                    ((i.call)
                     (begin
                       (save-our-regs (eq? (second insn) 'c)
                                      (fourth insn))
                       (case (second insn)
                         ((c)
                          (set-proc-args #f (list-tail insn 4))
                          (format p "    callq ~a\n"
                                  (csymbol (third insn))))
                         ((g)
                          (set-proc-args #t (list-tail insn 4))
                          (format p "    movq ~a(%rip), %rdi\n"
                                  (csymbol (third insn)))
                          (format p "    movq ~d(%rdi), %rax\n"
                                  *proc-offset-code*)
                          (format p "    callq *%rax\n"))
                         ((r)
                          (unless (icode-register? (third insn))
                            (errorf "illegal register in \"call r\": ~a\n"
                                    (third insn)))
                          (set-proc-args #f (cons (third insn)
                                                  (list-tail insn 4)))
                          (format p "    movq ~d(%rdi), %rax\n"
                                  *proc-offset-code*)
                          (format p "    callq *%rax\n"))
                         ((u)
                          (set-proc-args #f (cons (third insn)
                                                  (list-tail insn 4)))
                          (format p "    callq ~a\n" (csymbol "ucall")))
                         (else
                          => (^t (errorf "call does not support ptype=~a"
                                         t))))
                       (restore-our-regs (fourth insn))
                       (when (fourth insn)
                         (format p "    movq %rax, ~a\n"
                                 (ireg->mreg (fourth insn))))))

                    ;; (i.char reg num)
                    ((i.char)
                     (format p "    movq $~a, ~a\n"
                             (+ (ash (third insn) 32) #x07)
                             (ireg->mreg (second insn))))

                    ;; (i.display R0 R1 I)
                    ((i.display)
                     (unless (icode-register? (third insn))
                       (errorf "2nd operand must be a register: ~a\n" insn))
                     (format p "    movq ~d~d(~a), ~a\n"
                             (+ (* (fourth insn) 8) *proc-offset-display*)
                             (untag 'proc)
                             (ireg->mreg (third insn))
                             (ireg->mreg (second insn))))

                    ;; (i.false reg)
                    ((i.false)
                     (format p "    movq $0x0f, ~a\n"
                             (ireg->mreg (second insn))))

                    ;; (i.fixnum reg num)
                    ((i.fixnum)
                     (format p "    movq $~a, ~a\n"
                             (sval (third insn))
                             (ireg->mreg (second insn))))

                    ;; (i.frame reg)
                    ((i.frame)
                     (format p "    movq %rbp, ~a\n"
                             (ireg->mreg (second insn))))

                    ;; (i.jeq ra rb label)
                    ((i.jeq)
                     (format p "    cmpq ~a, ~a\n"
                             (ireg->mreg (second insn))
                             (ireg->mreg (third  insn)))
                     (format p "    je ~a\n" (fourth insn)))

                    ;; (i.jf reg label)
                    ((i.jf)
                     (format p "    cmpq $0x0f, ~a\n"
                             (ireg->mreg (second insn)))
                     (format p "    je ~a\n" (third insn)))

                    ;; (i.jmp label)
                    ((i.jmp)
                     (format p "    jmp ~a\n" (second insn)))

                    ;; (i.jt reg label)
                    ((i.jt)
                     (format p "    cmpq $0x0f, ~a\n"
                             (ireg->mreg (second insn)))
                     (format p "    jne ~a\n" (third insn)))

                    ;; (i.label l)
                    ((i.label)
                     (format p "~a:\n" (second insn)))

                    ;; (i.lambda reg proc arity ndisplay vararg?)
                    ((i.lambda)
                     (save-our-regs #t (second insn))
                     (format p "    leaq ~a(%rip), %rdi\n"
                             (csymbol (third insn)))
                     (format p "    movq $0x~16,'0x, %rsi\n"
                             (copy-bit 63 (fourth insn) (sixth insn)))
                     (format p "    movq $~d, %rdx\n" (fifth insn))
                     (format p "    callq ~a\n" (csymbol "make_proc"))
                     (restore-our-regs (second insn))
                     (format p "    movq %rax, ~a\n"
                             (ireg->mreg (second insn))))

                    ;; (i.load reg var)
                    ((i.load)
                     (format p "    movq ~a(%rip), ~a\n"
                             (csymbol (third insn))
                             (ireg->mreg (second insn))))

                    ;; (i.move r0 r1)
                    ((i.move)
                     (unless (and (icode-register? (second insn))
                                  (icode-register? (third insn)))
                       (errorf "not a register: ~a\n" insn))
                     (format p "    movq ~a, ~a\n"
                             (ireg->mreg (third insn))
                             (ireg->mreg (second insn))))

                    ;; (i.multicall ptype proc nret rret... rarg...)
                    ((i.multicall)
                     (let* ((nrets     (fourth insn))
                            (rret-list (take (drop insn 4) nrets))
                            (rarg-list (drop insn (+ 4 nrets))))
                       (save-our-regs (eq? (second insn) 'c) rret-list)
                       (case (second insn)
                         ((g)
                          (set-proc-args #t rarg-list)
                          (format p "    movq ~a(%rip), %rdi\n"
                                  (csymbol (third insn)))
                          (format p "    movq ~d(%rdi), %rax\n"
                                  *proc-offset-code*)
                          (format p "    callq *%rax\n"))
                         ((r)
                          (unless (icode-register? (third insn))
                            (errorf "illegal register in \"call r\": ~a\n"
                                    (third insn)))
                          (set-proc-args #f (cons (third insn) rarg-list))
                          (format p "    movq ~d(%rdi), %rax\n"
                                  *proc-offset-code*)
                          (format p "    callq *%rax\n"))
                         ((u)
                          (set-proc-args #f (cons (third insn) rarg-list))
                          (format p "    callq ~a\n" (csymbol "ucall")))
                         (else
                          => (^t (errorf
                                  "i.muticall call does not support ptype=~a"
                                  t))))
                       (parallel-assign (take nrets mregs-for-return-values)
                                        (map mreg->ireg rret-list))
                       (restore-our-regs rret-list)))

                    ;; (i.null reg)
                    ((i.null)
                     (format p "    movq $~a, ~a\n"
                             *sval-nil*
                             (ireg->mreg (second insn))))

                    ;; (i.pointer reg var)
                    ((i.pointer)
                     (format p "    leaq ~a(%rip), ~a\n"
                             (csymbol (third insn))
                             (ireg->mreg (second insn))))

                    ;; (i.ret R0...)
                    ((i.ret)
                     (let1 nrets (length (cdr insn))
                       (when (> nrets (length mregs-for-return-values))
                         (error "too many return values: ~a" insn))
                       (epilogue p procname)
                       (parallel-assign (map ireg->mreg (cdr insn))
                                        (take mregs-for-return-values nrets))
                       (format p "    retq\n")))

                    ;; (i.set-box! R0 R1)
                    ;; -> box(R0) = R1
                    ((i.set-box!)
                     (format p "    movq ~a, ~d(~a)\n"
                             (ireg->mreg (third insn))
                             (untag 'box)
                             (ireg->mreg (second insn))))

                    ;; (i.setdisplay R0 I R2)
                    ((i.setdisplay)
                     (format p "    movq ~a, ~d~d(~a)\n"
                             (ireg->mreg (fourth insn))
                             (+ (* (third insn) 8) *proc-offset-display*)
                             (untag 'proc)
                             (ireg->mreg (second insn))))

                    ;; (i.store reg var)
                    ((i.store)
                     (format p "    movq ~a, ~a(%rip)\n"
                             (ireg->mreg (second insn))
                             (csymbol (third insn))))

                    ;; (i.string reg label)
                    ((i.string)
                     (format p "    leaq ~a+~a(%rip), ~a\n"
                             (csymbol (third insn))
                             *sval-tag-str*
                             (ireg->mreg (second insn))))

                    ;; (i.sub r0 r1 r2)
                    ;; -> r0 = r1 - r2
                    ((i.sub)
                     (let ((mr0 (ireg->mreg (second insn)))
                           (mr1 (ireg->mreg (third insn)))
                           (mr2 (ireg->mreg (fourth insn))))
                       (cond ((equal? mr0 mr1)
                              (format p "    subq ~a, ~a\n" mr2 mr0))
                             ((equal? mr0 mr2)
                              ;; r0 = r1 - r0  =>  r0 = (-r0) + r1
                              (format p "    negq ~a\n" mr0)
                              (format p "    addq ~a, ~a\n" mr1 mr0))
                             (else
                              (format p "    movq ~a, ~a\n" mr1 mr0)
                              (format p "    subq ~a, ~a\n" mr2 mr0)))))

                    ;; (i.subproc R0 R1 I)
                    ((i.subproc)
                     (format p "    movq ~a-~a+~a*8(~a), ~a\n"
                             *gfun-offset-procs*
                             *sval-tag-gfun*
                             (fourth insn)
                             (ireg->mreg (third insn))
                             (ireg->mreg (second insn))))

                    ;; (i.symbol reg sym)
                    ((i.symbol)
                     (format p "    leaq sym_~a+0x3(%rip), ~a\n"
                             (symbol-s2c (third insn))
                             (ireg->mreg (second insn))))

                    ;; (i.tailcall ptype proc regs...)
                    ((i.tailcall)
                     (begin
                       (case (second insn)
                         ((c)
                          (set-proc-args #f (list-tail insn 3))
                          (epilogue p procname)
                          (format p "    jmp ~a\n"
                                  (csymbol (third insn))))
                         ((g)
                          (set-proc-args #t (list-tail insn 3))
                          (format p "    movq ~a(%rip), %rdi\n"
                                  (csymbol (third insn)))
                          (format p "    movq ~d(%rdi), %rax\n"
                                  *proc-offset-code*)
                          (epilogue p procname)
                          (format p "    jmp *%rax\n"))
                         ((r)
                          (unless (icode-register? (third insn))
                            (errorf "illegal register in \"tailcall r\": ~a\n"
                                    (third insn)))
                          (set-proc-args #f (list-tail insn 2))
                          (format p "    movq ~d(%rdi), %rax\n"
                                  *proc-offset-code*)
                          (epilogue p procname)
                          (format p "    jmp *%rax\n"))
                         ((u)
                          (set-proc-args #f (list-tail insn 2))
                          (epilogue p procname)
                          (format p "    jmp ~a\n" (csymbol "ucall")))
                         (else
                          => (^t (errorf "tailcall does not support ptype=~a"
                                         t))))))

                    ;; (i.true reg)
                    ((i.true)
                     (format p "    movq $0x~x, ~a\n"
                             *sval-true*
                             (ireg->mreg (second insn))))

                    ;; (i.unbox r0 r1)
                    ((i.unbox)
                     (format p "    movq ~d(~a), ~a\n"
                             (untag 'box)
                             (ireg->mreg (third insn))
                             (ireg->mreg (second insn))))

                    ;; (i.unspecified reg)
                    ((i.unspecified)
                     (format p "    movq $~a, ~a\n"
                             *sval-unspecified*
                             (ireg->mreg (second insn))))

                    ;; (i.vector reg label)
                    ((i.vector)
                     (format p "    leaq ~a+~a(%rip), ~a\n"
                             (csymbol (third insn))
                             *sval-tag-vector*
                             (ireg->mreg (second insn))))

                    (else (errorf "unknown icode instruction: ~a" insn)))
                  (inc! idx))
                body)
      ))

  (define (defvar def is-global)
    (define newlabel
      (let1 i 0
        (lambda ()
          (rlet1 s (format #f "a_~d" i)
            (inc! i)))))
    (define (write-vector-members! vec)
      (vector-for-each-with-index (lambda (i value)
                                    (if (or (bytevector? value)
                                            (string? value)
                                            (vector? value))
                                        (let1 label (newlabel)
                                          (write-literal-value label value)
                                          (vector-set! vec i label))))
                                  vec))
    (define (write-literal-value name value)
      ;; replace small objects with their label.
      (if (vector? value)
          (write-vector-members! value))
      (format p "~a:\n" name)
      (cond ((bytevector? value)
             (format p "  .quad ~a\n"
                     (+ *bytevector-bytes-offset*
                        (bytevector-length value)))
             (let1 len (bytevector-length value)
               (when (> len 0)
                 (format p "  .byte ")
                 (dotimes (i (- len 1))
                   (format p "~a, " (bytevector-u8-ref value i)))
                 (format p "~a\n" (bytevector-u8-ref value
                                                     (- len 1))))))

            ((integer? value)
             (format p "  .quad ~a\n" (sval value)))

            ((string? value)
             (format p "  .quad ~a\n"
                     (+ *string-bytes-offset*
                        (string-length value)))
             (format p "  .ascii \"~a\"\n" value))

            ((vector? value)
             (format p "  .quad ~a\n"
                     (* (+ 1 (vector-length value)) 8))
             (vector-for-each (lambda (e)
                                (format p "  .quad ~a\n"
                                        (if (string? e)
                                            e
                                            (sval e))))
                              value))

            ((eq? value #f)
             (format p "  .quad ~a\n" *sval-false*))

            (else
             (errorf "(defvar) not support ~a" (cadr def)))))
    (format p "\n")
    (format p ".data\n")
    (for-each (lambda (def)
                (let ((name  (car def))
                      (value (cadr def)))
                  (when is-global
                    (format p ".globl ~a\n" (csymbol name)))
                  (format p "  .p2align 4\n")
                  (write-literal-value (csymbol name) value)
                  ))
              (cdr def))
    (format p "\n"))

  (define (defsym name)
    (format p ".data\n")
    (format p "  .p2align 4\n")
    (format p "sym_~a:\n" (symbol-s2c name))
    (format p "  .quad ~a\n" (+ *symbol-name-offset*
                                (string-length (symbol->string name))))
    (format p "  .ascii \"~a\"\n" name))

  (define (write-global-variable-pointers vars)
    (format p "  .p2align 3\n")
    (format p "~a:\n" (csymbol "global_variable_pointers"))
    (for-each (lambda (var)
                (format p "  .quad ~a\n" (csymbol (car var))))
              vars)
    (format p "  .quad 0\n"))

  (call-with-input-file infile
    (lambda (inport)
      (define gvars '())
      (for-each (^c (cond ((eq? (car c) 'defsym)
                           (defsym (cadr c)))
                          ((eq? (car c) 'defproc)
                           (defproc c))
                          ((eq? (car c) 'defliteral)
                           (defvar c #f))
                          ((eq? (car c) 'defvar)
                           (begin
                             (set! gvars (append gvars (cdr c)))
                             (defvar c #t)))
                          (else
                           (errorf "unknown icode top: ~a" c))))
                (port->list read inport))
      (write-global-variable-pointers gvars))))

(define (write-libdef-file prog cxt filename)
  (call-with-output-file filename
    (lambda (port)
      (define (arg-list arity)
        (map (^i (format #f "a~a" i)) (iota arity)))
      (define (write-import lib-list)
        (write (cons 'import
                     (map (^l (slot-ref l 'name)) lib-list))
               port)
        (newline port))
      (define (write-a-exported-symbol sym)
        (let1 env (slot-ref (slot-ref cxt 'lib) 'env)
          (let1 v (get env sym)
            (cond ((and (is-a? v <scheme-variable>)
                        (static? v))
                   (write-a-proc (cons sym (type v))))
                  ((is-a? v <scheme-syntax-rules>)
                   (write-a-macro (cons sym v)))))))
      (define (write-a-proc pair)
        (let ((name (car pair))
              (type (cdr pair)))
          (if (is-a? type <procedure-type>)
              (format port "(define-lib-proc ~a)~%"
                      (if (vararg? type)
                          (cons name (append (arg-list (arity type)) 'tail))
                          (cons name (arg-list (arity type)))))
              (format port "(define-lib-variable ~a)~%" name))))
      (define (write-a-macro pair)
        (define (syntax-rule->exp srule)
          (if-let1 rest (slot-ref srule 'rest)
            (list (append '(_) (slot-ref srule 'args) rest)
                  (unannotate (slot-ref srule 'template)))
            (list (cons* '_ (slot-ref srule 'args))
                  (unannotate (slot-ref srule 'template)))))

        (define (syntax-rules->exp srules)
          (cons* 'syntax-rules '() (map syntax-rule->exp
                                        (slot-ref srules 'rules))))
        (write (list 'define-syntax
                     (car pair)
                     (syntax-rules->exp (cdr pair)))
               port)
        (format port "~%"))

      (write-import (slot-ref (slot-ref cxt 'module) 'imported-libraries))

      (for-each write-a-exported-symbol
                (exported-symbols (slot-ref cxt 'lib)))

      (for-each write-a-proc (filter (^t (and (is-a? (cdr t) <scheme-variable>)
                                              (static? (cdr t))))
                                     (global-variables (aexp->env prog))))
      (for-each write-a-macro (filter (^t (is-a? (cdr t)
                                                 <scheme-syntax-rules>))
                                      (global-variables (aexp->env prog))))
      )))


(define (detect-target)
  (when (string-contains (gauche-architecture) "apple")
    (set! *leading-underscore* #t)))


;;
;; Commands
;;
(define (do-build scheme-program-file)
  (define (lib-names-recursive libs)
    (define (rec libs lib-names)
      (if (null? libs)
          lib-names
          (let* ((lib     (car libs))
                 (libname (slot-ref (car libs) 'name)))
            (if (member libname lib-names)
                (rec (cdr libs) lib-names)
                (rec (cdr libs)
                     (rec (slot-ref lib 'imported-libraries)
                          (cons libname lib-names)))))))
    (rec libs '()))

  (let ((prog (read-program scheme-program-file)))

    (let1 cxt (call-with-output-file "a.icode"
                (^p (compile-scheme-program-to-icode prog p #t)))

      (call-with-output-file "a.s"
        (^p (icode-codegen "a.icode" p)))

      (sys-system "cc -c -g a.s")

      (let1 objs (string-join
                  (map (lambda (libname)
                         (string-append "build/"
                                        (libname->pathname libname)
                                        ".o"))
                       (lib-names-recursive (slot-ref (slot-ref cxt 'module)
                                                      'imported-libraries)))
                  " ")
        (sys-system #"cc -g -o a.out a.o ~|objs| -Llibscm -lscm")))))

(define-class <deplib> ()
  (name filename requires users))

(define (make-deplib name filename libs)
  (let1 dl (make <deplib>)
    (slot-set! dl 'name name)
    (slot-set! dl 'filename filename)
    (slot-set! dl 'requires libs)
    (slot-set! dl 'users '())
    dl))

(define (do-build3)
  (define (deps-sort files)
    (define (libfile->deplist filename)
      (and-let* ((prog     (read-program filename))
                 (prog-exp (aexp->exp prog))
                 (top      (and (pair? prog-exp)
                                (aexp->exp (car prog-exp))))
                 (libname  (and (pair? top)
                                (eq? 'define-library (aexp->exp (car top)))
                                (unannotate (cadr top)))))
        (let loop ((decl-aexps (cddr top)))
          (if (pair? decl-aexps)
              (or (and-let* ((decl (aexp->exp (car decl-aexps)))
                             ( (pair? decl))
                             ( (eq? (aexp->exp (car decl))
                                    'import))
                             (imported-libs (map unannotate (cdr decl))))
                    (when *verbose*
                      (format #t "~a depends on ~a~%"
                              filename imported-libs))
                    (make-deplib libname filename imported-libs))
                  (loop (cdr decl-aexps)))
              (make-deplib libname filename '())))))
    (define (requires->users deplist)
      (when #f
        (print "library imports:")
        (for-each (lambda (dl)
                    (format #t "  ~a <- ~a~%"
                            (slot-ref dl 'name)
                            (slot-ref dl 'requires)))
                  deplist))
      (let1 alist (map (lambda (dl)
                         (cons (slot-ref dl 'name) dl))
                       deplist)
        (for-each (lambda (dl)
                    (for-each (lambda (name)
                                (let1 p (assoc name alist)
                                  (push! (ref (cdr p) 'users) dl)))
                              (slot-ref dl 'requires)))
                  deplist)
        (when #f
          (print "library required by:")
          (for-each (lambda (dl)
                      (format #t "  ~a -> ~a~%"
                              (slot-ref dl 'name)
                              (map (^d (slot-ref d 'name))
                                   (slot-ref dl 'users))))
                    deplist))
        deplist))
    (define (deplist->graph deplist)
      (map (lambda (dl)
             (cons* dl (slot-ref dl 'users)))
           deplist))
    (when *verbose*
      (format #t "source files for libraries:~%")
      (for-each (^f (format #t "  ~a~%" f)) files))
    (map (lambda (dl)
           (slot-ref dl 'filename))
         (topological-sort
          (deplist->graph
           (requires->users
            (map libfile->deplist files))))))

  (make-directory* "build")
  (for-each do-lib (deps-sort (sys-glob "lib/**/*.scm")))
  0)

(define (do-compile scheme-file)
  (let ((name (if (string-suffix? ".scm" scheme-file)
                  (string-drop-right scheme-file 4)
                  scheme-file)))

    (make-directory* "build")

    (let1 prog (read-program scheme-file)
      (let1 cxt (call-with-output-file #"~|name|.icode"
                  (^p (compile-scheme-program-to-icode prog p #f)))
        (let1 prefix (libname->pathname (slot-ref cxt 'libname))
          (write-libdef-file prog cxt #"build/~|prefix|.d.scm")

          (call-with-output-file #"~|name|.s"
            (^p (icode-codegen #"~|name|.icode" p)))

          (sys-system #"cc -c -g -o build/~|prefix|.o ~|name|.s"))))))

(define (do-lib scmfile)
  (define (newer? a b)
    (define (mtime filename)
      (slot-ref (sys-stat filename) 'mtime))
    (guard (e (else #f))
      (> (mtime a) (mtime b))))
  (let1 tmp (build-path "build" "tmp.icode")
    (let1 prog (read-program scmfile)
      (make-directory* "build")
      (let1 cxt (call-with-output-file tmp
                  (^p (compile-scheme-program-to-icode prog p #f)))
        (let* ((basename   (libname->pathname (slot-ref cxt 'libname)))
               (headerfile #"build/~|basename|.d.scm")
               (icodefile  #"build/~|basename|.icode")
               (asmfile    #"build/~|basename|.s")
               (objfile    #"build/~|basename|.o")
               (libfile    #"build/liball.a"))
          (unless (newer? objfile scmfile)
            (sys-rename tmp icodefile)
            (format #t "COMPILE ~a -> ~a~%" scmfile icodefile)

            (format #t "HEADER  ~a -> ~a~%" scmfile headerfile)
            (write-libdef-file prog cxt headerfile)

            (format #t "CODEGEN ~a -> ~a~%" icodefile asmfile)
            (call-with-output-file asmfile
              (^p (icode-codegen icodefile p)))

            (format #t "ASM     ~a -> ~a~%" asmfile objfile)
            (sys-system #"cc -c -g -o ~|objfile| ~|asmfile|")

            (format #t "AR      ~a -> liball.a~%" objfile)
            (sys-system #"ar -rcu ~|libfile| ~|objfile|"))))))
  0)

(define (do-run program-file)
  (do-build program-file)
  (sys-exec "./a.out" '("a.out")))

(define (usage)
  (display "usage: gosh scheme.scm <command> [<args>...]\n")
  (display "    build    Build a binary program\n")
  (display "    build3   Build runtime libraries\n")
  (display "    compile  Generate an object file\n")
  (display "    lib      Compile a library module\n")
  (display "    run      Compile and run a Scheme program\n"))

(define (main args)
  (when (= (length args) 1)
    (usage)
    (exit 1))
  (detect-target)
  (let1 cmd (second args)
    (set! args (drop args 2))
    (while (and (pair? args)
                (string-prefix? "-" (car args)))
      (let1 arg (pop! args)
        (cond ((string=? arg "-t")
               (set! *testcode* #t))
              ((string=? arg "-v")
               (set! *verbose* #t))
              (else (format #t "unknown option: ~a~%" arg)
                    (exit 1)))
        ))
    (cond ((equal? cmd "build")
           (do-build (car args)))

          ((equal? cmd "build3")
           (do-build3))

          ((equal? cmd "compile")
           (do-compile (car args)))

          ((equal? cmd "lib")
           (do-lib (car args)))

          ((equal? cmd "run")
           (do-run (car args)))

          (else (usage)
                (exit 1)))))
