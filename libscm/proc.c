#include <stdlib.h>
#include <string.h>

#include "scm.h"

struct sobj_proc {
	struct sobj_header h;
	void *code;
	sval_t flags;
	void *stack;        /* @24 */
	size_t stacklen;    /* @32 */
	sval_t display[];   /* @40 */
};

#define SOBJ_PROC_FLAG_VARARG        (0x8000ULL << 48)
#define SOBJ_PROC_FLAG_CONTINUATION  (0x4000ULL << 48)
#define SOBJ_PROC_ARITY_MASK         (0xffffULL)


static struct sobj_proc *
sobj_proc(sval_t v)
{
        return (struct sobj_proc *)(v - SVAL_TAG_PROC);
}

sval_t
sobj_proc_arity(const struct sobj_proc *proc)
{
	return proc->flags & SOBJ_PROC_ARITY_MASK;
}

static sval_t
sobj_proc_num_of_display(const struct sobj_proc *proc)
{
	sval_t disp_len = sobj_h_len(&proc->h) - sizeof(struct sobj_proc);
	return disp_len / sizeof(sval_t);
}

sval_t
make_proc(void *code, sval_t flags, sval_t ndisp)
{
	struct sobj_proc *p;
	size_t len;
	int i;

	len = sizeof(struct sobj_proc) + ndisp * sizeof(sval_t);
	p = mem_alloc(len, NULL, 0);
	p->h.word = len;
	p->flags = flags;
	p->code = code;
	p->stack = NULL;
	p->stacklen = 0;
	for (i = 0; i < ndisp; i++)
		p->display[i] = SVAL_UNSPECIFIED;
	return (sval_t)p | SVAL_TAG_PROC;
}

void
proc_copy_stack(sval_t v, void *ptr, sval_t size)
{
	struct sobj_proc *proc = sobj_proc(v);
	sval_t vb;
	uint8_t *bytes;

	vb = make_bytevector(size, &v, 1);
	bytes = bytevector_bytes(vb);
	memcpy(bytes, ptr, size);

	proc->stack = bytes;
	proc->stacklen = size;
}

void
proc_gc_mark(sval_t v)
{
	struct sobj_proc *proc = sval_to_ptr(v);
	sval_t ndisps;
	int i;

	if (sobj_h_mark(&proc->h))
		return;
	sobj_h_set_mark(&proc->h, 1);

	ndisps = sobj_proc_num_of_display(proc);
	for (i = 0; i < ndisps; i++)
		gc_mark(proc->display[i]);

	if (proc->stack != NULL)
		gc_mark_stack(proc->stack, proc->stacklen);
}

sval_t
g_arity(sval_t vp)
{
	return sval_fixnum(sobj_proc_arity(sobj_proc(vp)));
}

sval_t
g_procedure_p(sval_t v)
{
	return (proc_p(v) || gfun_p(v)) ? SVAL_TRUE : SVAL_FALSE;
}
