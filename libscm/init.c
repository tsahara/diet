#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "scm.h"


static unsigned int test_total  = 0;
static unsigned int test_failed = 0;


int
main(int argc, char **argv)
{
	extern sval_t s_main(void);
	int status;

	mem_init();
	status = fixnum(s_main());

	mem_last();
	return status;
}

void
s_assert0(sval_t v_exp, sval_t v_line)
{
	test_total++;
	if (v_exp == SVAL_FALSE) {
		printf("assertion failed: line %d\n", fixnum(v_line));
		test_failed++;
	}
}

sval_t
g_eq_p(sval_t a, sval_t b)
{
	// XXX no test
	if (a == b)
		return SVAL_TRUE;
	return SVAL_FALSE;
}

sval_t
g_equal(sval_t a, sval_t b)
{
	extern sval_t g_eqv(sval_t, sval_t);

	// if (eqv? a b) is #t, (equal? a b) must be #t.
	if (g_eqv(a, b) == SVAL_TRUE)
		return SVAL_TRUE;
	if (pair_p(a) && pair_p(b))
		return pair_equal(a, b);
	if (string_p(a) && string_p(b))
		return g_string_3d__3f_(a, b);
	if (vector_p(a) && vector_p(b))
		return vector_equal(a, b);
	if (bytevector_p(a) && bytevector_p(b))
		return bytevector_equal(a, b);
	return SVAL_FALSE;
}

sval_t
g_eqv(sval_t a, sval_t b)
{
	if (a == b)
		return SVAL_TRUE;
	return SVAL_FALSE;
}

sval_t
c_exit(void)
{
	extern sval_t c_exit_n(sval_t);
	return c_exit_n(SVAL_TRUE);
}

sval_t
c_exit_n(sval_t obj)
{
	int status;

	if (obj == SVAL_TRUE)
		status = EXIT_SUCCESS;
	else if (obj == SVAL_FALSE)
		status = EXIT_FAILURE;
	else if (fixnum_p(obj))
		status = cval_fixnum(obj);
	else
		status = 2;
	exit(status);

	/* NOTREACHED */
	return SVAL_FALSE;
}

sval_t
g_exit_2d_test(void)
{
	printf("%s: %u tests, %u failed.\n",
		(test_failed == 0 ? "PASS" : "FAILED"),
		test_total, test_failed);
	return sval_fixnum(test_failed > 0);
}

sval_t
s_get_2d_environment_2d_variable(sval_t v)
{
	const uint8_t *cp;
	uint8_t *bufp, *valp;
	sval_t slen, vret;

	sval_string_ref(v, &cp, &slen);
	bufp = malloc(slen + 1);
	memcpy(bufp, cp, slen);
	bufp[slen] = '\0';
	valp = (uint8_t *)getenv((char *)bufp);
	vret = make_string(strlen((char *)valp), valp);
	free(bufp);
	return vret;
}

sval_t
g_sleep(sval_t v)
{
	sleep(cval_fixnum(v));
	return SVAL_NIL;
}

void
g_sys_abort(void)
{
	abort();
}
