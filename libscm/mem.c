#include <sys/mman.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "scm.h"

struct free_pair {
	sval_t unused;
	struct free_pair *next;
};

struct free_block {
	struct sobj_header h;
	struct free_block *next;
};

#define CELL_COUNT 253
struct cell_pool {  /* 4096 bytes */
	struct cell_pool *next;
	sval_t a;
	sval_t bitmap[4];
	struct sobj_pair cells[CELL_COUNT];
};


uint8_t *memory_block = NULL;
size_t allocated;

struct free_block *heap_free_list = NULL;

struct cell_pool *cell_pool_list = NULL;
struct free_pair *cell_free_list = NULL;
sval_t *cell_unused;

static FILE *gc_fp = NULL;
static unsigned long gc_counter = 0;

static sval_t *module_gvars[10];

static sval_t tmproot_objects[20];
static size_t tmproot_index = 0;

static void gc_logging_allocate(const void *, size_t, size_t);
static void gc_mark_main_stack(void *);
static void mark_global_variables(void);
static void mark_live_objects(sval_t *, size_t);
static void heap_mark_free_blocks(void);
static void mem_gc_run(sval_t *, size_t, const char *);
static void mem_walk(void);
static void sweep_cell_pool(void);
static void sweep_heap(void);
static void unmark_heap(void);

static void new_cell_pool(void);
static void cell_gc_mark(sval_t);
static void tmproot_mark(void);


void
mem_init(void)
{
	if (sizeof(struct cell_pool) != 4096) {
		fprintf(stderr, "cell_pool size bad %zd\n",
				sizeof(struct cell_pool));
		abort();
	}
	gc_fp = fopen("gc-log.txt", "w");
	new_cell_pool();
}

void *
mem_alloc(size_t rsize, sval_t *liveobjects, sval_t nobjs)
{
	struct free_block *blk, *newblk, **prevp;
	size_t asize;
	sval_t blocksize;
	void *ptr;
	bool retry = false;

	if (memory_block == NULL) {
		ptr = mmap(0, 1024*1024, PROT_READ|PROT_WRITE,
			   MAP_ANON|MAP_SHARED, -1, 0);
		if (ptr == MAP_FAILED)
			abort();
		memory_block = ptr;
		allocated = 1024*1024;

		blk = ptr;
		blk->h.word = 1024*1024;
		blk->next = NULL;
		heap_free_list = blk;
	}

	asize = (rsize + 0x0f) & ~0x0f;
search:
	for (blk = heap_free_list, prevp = &heap_free_list;
	     blk != NULL;
	     prevp = &blk->next, blk = blk->next) {
		blocksize = sobj_h_len(&blk->h);
		if (asize < blocksize) {
			newblk = (struct free_block *)((uint8_t *)blk + asize);
			newblk->h.word = blocksize - asize;
			newblk->next = blk->next;
			*prevp = newblk;
			break;
		} else if (asize == blocksize) {
			*prevp = blk->next;
			break;
		} else if (blocksize == 0) {
			fprintf(stderr, "!!! blocksize == 0\n");
			abort();
		}
	}
	if (blk == NULL) {
		if (retry) {
			fprintf(stderr, "heap full\n");
			abort();
		}
		mem_gc_run(liveobjects, nobjs, "no heap block");
		retry = true;
		goto search;
	}

	gc_logging_allocate(blk, rsize, asize);
	return blk;
}

void *
cell_alloc(sval_t *objects, sval_t nobjs)
{
	struct sobj_pair *p;

	if (cell_free_list == NULL) {
		mem_gc_run(objects, nobjs, "no cell");
		if (cell_free_list == NULL) {
			fprintf(stderr, "cannot allocate cell\n");
			abort();
		}
	}

	p = (struct sobj_pair *)cell_free_list;
	cell_free_list = cell_free_list->next;
	p->car = 0xc1c1c1c1c1c1c1c1ULL;
	p->cdr = 0xc2c2c2c2c2c2c2c2ULL;

	fprintf(gc_fp, "0x%012lx alloc cell\n", (sval_t)p);
	return p;
}

static void
new_cell_pool(void)
{
	struct cell_pool *pool;
	struct free_pair *pair, *prev;
	void *ptr;
	int i;

	ptr = mmap(0, sizeof(struct cell_pool), PROT_READ|PROT_WRITE,
		   MAP_ANON|MAP_SHARED, -1, 0);
	if (ptr == MAP_FAILED)
		abort();
	if (((uintptr_t)ptr & 0xfff) != 0) {
		fprintf(stderr, "cell pool must be aligned to page boundary\n");
		exit(1);
	}

	pool = ptr;
	pool->next = cell_pool_list;
	cell_pool_list = pool;

	memset(pool->bitmap, 0, sizeof(pool->bitmap));

	prev = NULL;
	for (i = 0; i < CELL_COUNT; i++) {
		pair = (struct free_pair *)&pool->cells[i];
		pair->unused = SVAL_UNSPECIFIED;
		pair->next = prev;
		prev = pair;
	}
	cell_free_list = prev;
}

static void
unmark_cell_pool(void)
{
	struct cell_pool *pool;

	fprintf(gc_fp, "# unmark cells\n");
	for (pool = cell_pool_list; pool != NULL; pool = pool->next) {
		fprintf(gc_fp, "0x%012lx unmark cell pool\n", (sval_t)pool);
		memset(pool->bitmap, 0, sizeof(pool->bitmap));
	}
}

static void
cell_gc_mark(sval_t v)
{
	struct sobj_pair *p;

	gc_mark_cell_bitmap(v);

	p = sval_to_ptr(v);
	gc_mark(p->car);
	gc_mark(p->cdr);
}

void
gc_mark_cell_bitmap(sval_t v)
{
	struct cell_pool *pool;
	sval_t idx, mask;

	// 0x4320 -> 0x320 -> iimm mmmm 0000
	pool = (struct cell_pool *)(v & ~0xfff);
	idx = (v >> 10) & 0x3;
	mask = 1 << ((v >> 4) & 0x3f);

	if ((pool->bitmap[idx] & mask) == 0)
		pool->bitmap[idx] |= mask;
}

static void
gc_mark_free_cells(void)
{
	struct cell_pool *pool;
	struct free_pair *pair;
	sval_t v;
	sval_t idx, mask;

	for (pair = cell_free_list; pair != NULL; pair = pair->next) {
		v = (sval_t)pair;
		pool = (struct cell_pool *)(v & ~0xfff);
		idx = (v >> 10) & 0x3;
		mask = 1 << ((v >> 4) & 0x3f);
		pool->bitmap[idx] |= mask;
	}
}

static void
sweep_cell_pool(void)
{
	struct cell_pool *pool;
	struct free_pair *pair;
	sval_t idx, mask, v;
	int i;

	fprintf(gc_fp, "# sweep cell pool\n");

	for (pool = cell_pool_list; pool != NULL; pool = pool->next) {
		for (i = 0; i < CELL_COUNT; i++) {
			v = (sval_t)&pool->cells[i];
			idx = (v >> 10) & 0x3;
			mask = 1 << ((v >> 4) & 0x3f);

			if ((pool->bitmap[idx] & mask) == 0) {
				fprintf(gc_fp, "0x%012lx sweep cell\n", v);
				pair = sval_to_ptr(v);
				pair->unused = SVAL_UNSPECIFIED;
				pair->next = cell_free_list;
				cell_free_list = pair;
			}
		}
	}
}

static void
gc_logging_allocate(const void *ptr, size_t rsize, size_t asize)
{
	fprintf(gc_fp, "0x%012lx requested=%zd allocated=%zd\n",
			(sval_t)ptr, rsize, asize);
}

sval_t
s_mem_gvar(sval_t ptr)
{
	int i;

	for (i = 0; i < 10; i++) {
		if (module_gvars[i] == NULL)
			break;
	}
	if (i == 10)
		printf("module_gvars overflow!\n");

	module_gvars[i] = (void *)ptr;
	return SVAL_TRUE;
}

void
mem_last(void)
{
	sval_t **sp;
	int i;

	fprintf(gc_fp, "# memory last cleanup\n");

	for (i = 0; i < 10; i++) {
		sp = (sval_t **)module_gvars[i];
		if (sp == NULL)
			break;

		while (*sp != NULL) {
			fprintf(gc_fp, "0x%012lx gvar@%p module[%d] %s\n",
					**sp, *sp, i, sval_tag_name(**sp));
			sp++;
		}
	}

	mem_walk();
}

static void
mem_gc_run(sval_t *objects, size_t nobjs, const char *reason)
{
	void *rbp;

	fprintf(gc_fp, "# GC starts #%lu (%s)\n", ++gc_counter, reason);

	unmark_cell_pool();
	unmark_heap();

	asm volatile("movq %%rbp, %0" : "=r"(rbp));
	gc_mark_main_stack(rbp);
	mark_global_variables();
	mark_exception_handler();
	tmproot_mark();
	mark_live_objects(objects, nobjs);
	gc_mark_free_cells();
	heap_mark_free_blocks();

	sweep_cell_pool();
	sweep_heap();

	fprintf(gc_fp, "# GC ends #%lu\n", gc_counter);
}

static void
mem_walk(void)
{
	sval_t asize;
	uint8_t *ptr;

	ptr = memory_block;
	while (ptr < memory_block + allocated) {
		struct sobj_header *h = (struct sobj_header *)ptr;
		asize = sobj_h_allocated_size(h);

		if (sobj_h_large_p(h)) {
			struct sobj_large_header *lh =
				*(struct sobj_large_header **)(h + 1);

			fprintf(gc_fp, "0x%012lx asize=%lu large object\n",
					(sval_t)ptr, asize);
			(*lh->free_object)(h);
		} else
			fprintf(gc_fp, "0x%012lx asize=%lu small object\n",
					(sval_t)ptr, asize);
		ptr += asize;
	}
}

void
g_gc(void)
{
	mem_gc_run(NULL, 0, "user request");
}

static void
gc_mark_main_stack(void *rbp)
{
	extern sval_t *s_stack_2d_bottom;
	size_t len;

	len = (sval_t)s_stack_2d_bottom - (sval_t)rbp;
	gc_mark_stack(rbp, len);
}

void
gc_mark_stack(void *base, size_t len)
{
	extern sval_t *s_stack_2d_bottom;
	sval_t *bottom, *top, *p, v;
	int nregs;
	bool cproc = false;
	enum {
		STACK_BOTTOM,
		STACK_REGNUM,
		STACK_REGISTER,
		STACK_RETURN,
		STACK_RBP,
		STACK_C
	} nextp;

	fprintf(gc_fp, "# mark stack variables\n");

	top = (sval_t *)base;
	bottom = (sval_t *)((const uint8_t *)base + len);
	for (p = bottom, nextp = STACK_BOTTOM; p >= top; p--) {
		fprintf(gc_fp, "%p => %016lx  ", p, *p);
		switch (nextp) {
		case STACK_BOTTOM:
			fprintf(gc_fp, "<- stack-bottom\n");
			nextp = STACK_REGNUM;
			break;
		case STACK_REGNUM:
			v = *p;
			if (*p >= 256) {
				cproc = true;
				v -= 256;
				fprintf(gc_fp, "cproc, ");
			}
			fprintf(gc_fp, "nregs = %d\n", (int)v);
			nextp = STACK_REGISTER;
			nregs = v;
			break;
		case STACK_REGISTER:
			fprintf(gc_fp, "saved register\n");
			gc_mark(*p);

			nregs--;
			if (nregs == 0) {
				if (cproc)
					nextp = STACK_C;
				else
					nextp = STACK_RETURN;
			}
			break;
		case STACK_RETURN:
			fprintf(gc_fp, "return address\n");
			nextp = STACK_RBP;
			break;
		case STACK_RBP:
			fprintf(gc_fp, "saved %%rbp\n");
			nextp = STACK_REGNUM;
			break;
		case STACK_C:
			fprintf(gc_fp, "(c)\n");
			break;
		}
	}
}

static void
mark_global_variables(void)
{
	sval_t **sp;
	int i;

	fprintf(gc_fp, "# mark global variables\n");

	for (i = 0; i < 10; i++) {
		sp = (sval_t **)module_gvars[i];
		if (sp == NULL)
			break;

		while (*sp != NULL) {
			fprintf(gc_fp, "0x%012lx gvar@%p module[%d] mark %s\n",
					**sp, *sp, i, sval_tag_name(**sp));
			gc_mark(**sp);
			sp++;
		}
	}
}

static void
mark_live_objects(sval_t *objects, size_t nobjs)
{
	size_t i;

	fprintf(gc_fp, "# mark temporaly objects\n");
	for (i = 0; i < nobjs; i++)
		gc_mark(objects[i]);
}

static void
heap_mark_free_blocks(void)
{
	struct free_block *blk;

	for (blk = heap_free_list; blk != NULL; blk = blk->next)
		sobj_h_set_mark(&blk->h, 1);
}

void
gc_mark(sval_t v)
{
	struct sobj_header *h;

	switch (v & 0xf) {
	case SVAL_TAG_PAIR:
		cell_gc_mark(v);
		break;

	case SVAL_TAG_SYMBOL:
	case SVAL_TAG_STR:
	case SVAL_TAG_BYTEVECTOR:
	case SVAL_TAG_ERROR:
		fprintf(gc_fp, "0x%012lx mark sobj\n", v);
		h = sval_to_ptr(v);
		sobj_h_set_mark(h, 1);
		break;

	case SVAL_TAG_LARGE:
		large_gc_mark(v);
		break;

	case SVAL_TAG_GPROC:
		gfun_gc_mark(v);
		break;

	case SVAL_TAG_PROC:
		proc_gc_mark(v);
		break;

	case SVAL_TAG_VECTOR:
		vector_gc_mark(v);
		break;

	case SVAL_TAG_BOX:
		box_gc_mark(v);
		break;

	default:
		break;
	}
}

static void
unmark_heap(void)
{
	struct sobj_header *h;
	uint8_t *ptr;

	fprintf(gc_fp, "# unmark heap\n");
	ptr = memory_block;
	while (ptr < memory_block + allocated) {
		fprintf(gc_fp, "0x%012lx unmark block\n", (sval_t)ptr);
		h = (struct sobj_header *)ptr;
		sobj_h_set_mark(h, 0);
		ptr += sobj_h_allocated_size(h);
	}
}

static void
sweep_heap(void)
{
	struct free_block *blk;
	uint8_t *ptr;

	fprintf(gc_fp, "# sweep objects in heap\n");

	ptr = memory_block;
	while (ptr < memory_block + allocated) {
		struct sobj_header *h = (struct sobj_header *)ptr;

		fprintf(gc_fp, "0x%012lx sweep object: marked=%d\n",
				(sval_t)ptr, sobj_h_mark(h) ? 1 : 0);
		if (! sobj_h_mark(h)) {
			fprintf(gc_fp, "0x%012lx free heap sobj "
				       "(size=%lu/0x%lx)\n",
					(sval_t)ptr,
					sobj_h_len(h), sobj_h_len(h));

			if (sobj_h_large_p(h))
				free_large_object(ptr);
			blk = (struct free_block *)h;
			blk->h.word = sobj_h_allocated_size(h);
			blk->next = heap_free_list;
			heap_free_list = blk;
		}
		ptr += sobj_h_allocated_size(h);
	}
}

void
tmproot_add(sval_t v)
{
	if (tmproot_index >= 20) {
		fprintf(stderr, "tmproot_add: object overflow! (index=%zd)\n",
				tmproot_index);
		abort();
	}
	tmproot_objects[tmproot_index++] = v;
}

static void
tmproot_mark(void)
{
	fprintf(gc_fp, "# mark tmproot: %zd objects\n", tmproot_index);
	for (size_t i = 0; i < tmproot_index; i++) {
		sval_t v = tmproot_objects[i];
		fprintf(gc_fp, "0x%012lx tmproot[%zd] %s\n",
			v, i, sval_tag_name(v));
		gc_mark(v);
	}
}

size_t
tmproot_prepare(void)
{
	return tmproot_index;
}

void
tmproot_reset(size_t ti)
{
	tmproot_index = ti;
}
